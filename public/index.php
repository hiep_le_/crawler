<?php

use App\Application;

return call_user_func(function () {
    require_once __DIR__ . '/../vendor/autoload.php';
    if (!function_exists('split')) {
        function split($delimiter, $string, $limit = null) {
            return explode($delimiter, $string, $limit);
        }
    }

    $env = getenv('ENV') ?: 'dev';
    $cnf = is_file(__DIR__."/../config.{$env}.php")
        ? __DIR__."/../config.{$env}.php"
        : __DIR__.'/../config.default.php';

    $app = new Application(require $cnf);

    return ('cli' === php_sapi_name()) ? $app : $app->run();
});
