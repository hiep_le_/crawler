## Installation

````
composer install
./bin/console schema install
````

## Usage

### Scraping

````
./bin/console scraping {PROVIDER}
Eg:
=> scraping lo
 ./bin/console scraping udemy --env=dev 
=> scraping note
 ./bin/console scraping udemy --env=dev --note=1
=> Override data
 ./bin/console scraping udemy --env=dev --override=1
````


### Prepare Import into #account

````
./bin/console prepare_import udemy --env=dev
````

### Import into #account

````
./bin/console import udemy --env=dev
````

### Export data to csv

````
./bin/console export2csv {PROVIDER}
````