<?php

namespace go1\Scraping\domain\scraping;

use Doctrine\DBAL\Connection;
use Exception;
use PDO;

class ScrapingMappingRepository
{

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function insert($data) {
        $this->db->beginTransaction();
        try {
            $this->db->insert('scraping_mapping', $data);
            $id = $this->db->lastInsertId();
            $this->db->commit();
            return $id;
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function get($provider, $sourceId, $type) {
        $this->db->beginTransaction();
        try {
            $record = $this->db
                ->executeQuery('SELECT * 
                    FROM scraping_mapping 
                    WHERE provider = ? AND source_id = ? AND type = ?',
                    [$provider, $sourceId, $type]
                )
                ->fetch(PDO::FETCH_OBJ);

            if ($record) {
                return $record;
            }
        }
        catch (Exception $exception) {
            throw new Exception('Can not get scraping_mapping table');
        }
    }

}
