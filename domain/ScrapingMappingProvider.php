<?php

namespace go1\Scraping\domain;

use go1\Scraping\controller\ScrapingMappingCreateController;
use go1\Scraping\controller\ScrapingMappingGetController;
use go1\Scraping\controller\ScrapingMappingSchemaController;
use go1\Scraping\domain\scraping\ScrapingMappingRepository;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class ScrapingMappingProvider implements ServiceProviderInterface, BootableProviderInterface
{
    public function register(Container $c)
    {
        $c['scraping_mapping.ctrl.create'] = function (Container $c) {
            return new ScrapingMappingCreateController(
                $c['scraping_mapping.repository'],
                $c['access_checker']
            );
        };

        $c['scraping_mapping.ctrl.get'] = function (Container $c) {
            return new ScrapingMappingGetController(
                $c['scraping_mapping.repository']
            );
        };

        $c['scraping_mapping.repository'] = function (Container $c) {
            return new ScrapingMappingRepository($c['dbs']['default']);
        };
        $c['scraping_mapping.schema'] = function (Container $c) {
            return new ScrapingMappingSchemaController($c['dbs']['default']);
        };
    }

    public function boot(Application $app)
    {
        $app->post('/', 'scraping_mapping.ctrl.create:create');
        $app->get('/mapping', 'scraping_mapping.ctrl.get:get');
        $app->get('/install', 'scraping_mapping.schema:install');
    }
}
