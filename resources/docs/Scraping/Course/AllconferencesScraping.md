
# Allconferences Scraping

## production
 - portal: allconferences.mygo1.com
 - mail: allconferences@go1.com
 - password: dvs-dv88
 
## sub portal
 - portal: sub-portal-name.mygo1.com
 - mail: sub-portal-name@mygo1.com
 - password: dvs-dv88
 

### Running
Step 1: Scrap portal
- ./bin/console scraping allconferences --type=portal -o true --env=prod
- ./bin/console prepare_import allconferences --type=portal -o true --env=prod
- ./bin/console import allconferences --type=portal -o true --env=prod

Step 2: Scrap conferences (course)
- ./bin/console scraping allconferences --type=course -o true --env=prod
- ./bin/console prepare_import allconferences --type=lo --env=prod
- ./bin/console import allconferences --type=lo_portal --env=prod
 
 