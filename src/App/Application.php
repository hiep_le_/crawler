<?php

namespace App;

use App\Browser\BrowserServiceProvider;
use App\Go1\Go1ServiceProvider;
use App\Import\ImportServiceProvider;
use App\PrepareImport\PrepareImportServiceProvider;
use App\Scraping\ScrapingServiceProvider;
use go1\app\App as GO1;
use go1\Scraping\domain\ScrapingMappingProvider;
use go1\util\UtilServiceProvider;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class Application extends GO1
{
    const NAME    = 'scraping';
    const VERSION = '1.0';
    const SCRAPING_STATUS_CONTINUE = 'scraping_continue';

    private $environment;
    private $loggers;

    public function __construct(array $values = [])
    {
        $this->environment = $values['env'];

        parent::__construct($values);
        $this
            ->register(new CommonServiceProvider())
            ->register(new BrowserServiceProvider())
            ->register(new ScrapingServiceProvider())
            ->register(new ImportServiceProvider())
            ->register(new ScrapingMappingProvider())
            ->register(new Go1ServiceProvider())
            ->register(new UtilServiceProvider())
            ->register(new PrepareImportServiceProvider())
        ;

        $this->get('/', function () {
            return new JsonResponse(['service' => static::NAME, 'version' => static::VERSION, 'time' => time(), 'env' => $this->environment, 'db_user_name' => getenv("_DOCKER_GO1_DB_USERNAME")]);
        });
    }

    /**
     * Print help for an command
     */
    public function printHelp(Command $command, InputInterface $input, OutputInterface $output)
    {
        $helperCommand = $this->get('help');
        $definition = $command->getDefinition();
        if ($definition->hasArgument('command')) {
            $arguments = $definition->getArguments();
            unset($arguments['command']);
            $definition->setArguments($arguments);
        }
        $helperCommand->setCommand($command);
        $exitCode = $this->doRunCommand($helperCommand, $input, $output);

        return $exitCode;
    }

    /**
     * Get current environment
     */
    public function getEnvironment()
    {
        return $this->environment ?: 'dev';
    }

    /**
     * Get the logger instance
     *
     * @param string $name
     * @return Logger
     */
    public function logger($name = 'scraping')
    {
        if (empty($this->loggers[$name])) {
            $this->loggers[$name] = new Logger($name);
            $this->loggers[$name]->pushHandler(new StreamHandler($this->getRootDir() . "/var/logs/$name.log"));
        }

        return $this->loggers[$name];
    }
}
