<?php

namespace App;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

class LoggerController
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function setPathLogger($path)
    {
        $handler = new StreamHandler($path, Logger::DEBUG);
        $this->app['logger']->setHandlers([$handler]);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->app['logger'];
    }
}
