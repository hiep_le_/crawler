<?php

namespace App\Browser\PhantomJs;

use JonnyW\PhantomJs\Http\Request as PhantomJsRequest;

class Request extends PhantomJsRequest
{
    protected $expectedSelector;

    /**
     * Get expected element
     */
    public function getSelector()
    {
        return $this->expectedSelector ?: null;
    }

    /**
     * Set expected element
     * @param string $selector a valid css selector
     * @return $this
     */
    public function setSelector($selector)
    {
        $this->expectedSelector = $selector;

        return $this;
    }
}
