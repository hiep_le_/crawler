<?php

namespace App\Browser;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class BrowserServiceProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['browser.guzzle'] = function () {
            return new Guzzle();
        };

//        $c['browser.phantomjs'] = function (Container $c) {
//            $phantomJs = new PhantomJs();
//            $phantomJs->getEngine()->addOption('--disk-cache=true');
//            $phantomJs->getEngine()->addOption('--load-images=false');
//            $phantomJs->getEngine()->addOption('--max-disk-cache-size=102400');
//            $phantomJs->setApp($c['application']);
//            $phantomJs->customProcedures('global');
//            return $phantomJs;
//        };
    }
}