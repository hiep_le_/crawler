<?php

namespace App\Browser;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Psr\Http\Message\RequestInterface;

class Guzzle extends Client
{
    /**
     * @param string $id
     * @param array $config
     * @return self
     */
    static function getInstance($id, $config = [])
    {
        static $instances;
        if (empty($instances[$id])) {
            if ($id === 'normal') {
                $config = [];
            }
            if (!isset($config['timeout'])) {
                $config['timeout'] = 30;
            }
            $instances[$id] = new self($config);
        }

        return $instances[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function __construct(array $config = [])
    {
        if (!isset($config['cookies']) || !$config['cookies']) {
            $config['cookies'] = new CookieJar();
        }

        parent::__construct($config);
    }

    /**
     * Get the current cookieJar instance
     */
    public function getCookieJar()
    {
        return $this->getConfig('cookies');
    }

    /**
     * Attach a header into the request
     *
     * @param $header
     * @param $value
     * @example $this->getConfig('handler')->push(self::addHeader('Authorization', 'Basic xxx')), 'prepare_body');
     * @return \Closure
     */
    public static function addHeader($header, $value)
    {
        return function (callable $handler) use ($header, $value) {
            return function (RequestInterface $request, array $options) use ($handler, $header, $value) {
                $request = $request->withHeader($header, $value);

                return $handler($request, $options);
            };
        };
    }
}
