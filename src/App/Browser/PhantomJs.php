<?php

namespace App\Browser;

use App\Application;
use JonnyW\PhantomJs\Client;
use JonnyW\PhantomJs\DependencyInjection\ServiceContainer;
use JonnyW\PhantomJs\Http\Response;
use Exception;
use App\Browser\PhantomJs\Request;

class PhantomJs extends Client
{
    /** @var  \App\Application */
    private $app;

    /** @var ServiceContainer */
    private $service;

    /** @var  Request */
    private $request;

    /** @var  Response */
    private $response;

    public function __construct()
    {
        $this->service = new ServiceContainer();
        $this->service->load();
        parent::__construct(
            $this->service->get('engine'),
            $this->service->get('procedure_loader'),
            $this->service->get('procedure_compiler'),
            $this->service->get('message_factory')
        );
    }

    /**
     * @param Application $app
     * @return $this
     */
    public function setApp(Application $app)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * @return ServiceContainer
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Use an custom procedures
     * @param $path
     * @return $this
     */
    public function customProcedures($path)
    {
        $path = $this->app->getRootDir() . 'resources/Procedures/' . $path;
        $procedureLoader = $this->service->get('procedure_loader_factory');
        $this->getProcedureLoader()->addLoader($procedureLoader->createProcedureLoader($path));

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Make a http request
     * @param $url Target url
     * @param $method Http method
     * @param int $timeout second timeout
     * @param array $options see the set* method on JonnyW\PhantomJs\Http\Request
     * @return \JonnyW\PhantomJs\Http\ResponseInterface
     * @throws Exception
     */
    public function request($url, $method, $timeout = 30, $options = [])
    {
        // Create request
        $this->request = new Request($url, $method, $timeout * 1000);
        if ($options) {
            foreach ($options as $key => $option) {
                $method = 'set' . ucfirst($key);
                if (!method_exists($this->request, $method)) {
                    throw new Exception(sprintf('Unknown option with key %s', $key));
                }
                $this->request->$method($option);
            }
        }

        // Create response
        $this->response = $this->getMessageFactory()->createResponse();

        // Send request
        $this->send($this->request, $this->response);

        return $this;
    }
}
