<?php

namespace App;

use App\Command\ManualCreateLiCommand;
use App\Command\ExportToCsvCommand;
use App\Command\ImportCommand;
use App\Command\PrepareImportCommand;
use App\Command\SchemaCommand;
use App\Command\ScrapingCommand;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputOption;

class Console extends ConsoleApplication
{
    /**
     * @var Application
     */
    private $app;

    public function __construct(Application $app)
    {
        parent::__construct('Go1 Scraping Tool', '1.0');
        $this->initConsole($app);
        $this->add(new ManualCreateLiCommand($app));
        $this->add(new SchemaCommand($app));
        $this->add(new ImportCommand($app));
        $this->add(new PrepareImportCommand($app));
        $this->add(new ScrapingCommand($app));
        $this->add(new ExportToCsvCommand($app));
    }

    public function initConsole(Application $app)
    {
        $this->app = $app;
        $this->app->boot();

        $this->getDefinition()->addOption(new InputOption(
            '--env',
            '-e',
            InputOption::VALUE_REQUIRED,
            'The Environment name.',
            $this->app->getEnvironment()
        ));
    }

    public function getGo1App()
    {
        return $this->app;
    }
}
