<?php

namespace App;

use Doctrine\DBAL\Schema\Schema;

class SchemaDefinition
{
    /**
     * @var Schema
     */
    private $schemaManager;

    public function __construct($schemaManager = null)
    {
        $this->schemaManager = $schemaManager ?: new Schema();
    }

    public function definition()
    {
        // Add table scraping data
        $this->tableScrapingData();

        // Add table scraping state
        $this->tableScrapingState();

        // Import map is global and save it on go1vn database
        // Add table scraping_mapping
//        $this->tableScrapingMapping();

        // Add table course_import_map
        $this->tablePrepareImportMap();

        // Add table file_upload
        $this->tableFileUpload();

        return $this->schemaManager;
    }

    private function tableScrapingData()
    {
        $scrapingData = $this->schemaManager->createTable('scraping_data');
        $scrapingData->addColumn('scraping_data_id', 'integer', [
            'unsigned'      => true,
            'autoincrement' => true,
            'comment'       => 'A unique id',
        ]);
        $scrapingData->addColumn('provider', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The data provider name',
        ]);
        $scrapingData->addColumn('type', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The type of entity',
        ]);
        $scrapingData->addColumn('source_id', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The id on the target site',
        ]);
        $scrapingData->addColumn('title', 'string', [
            'length'  => 255,
            'notnull' => true,
            'comment' => 'The course title',
        ]);
        $scrapingData->addColumn('source_url', 'string', [
            'length'  => 255,
            'notnull' => true,
            'comment' => 'The course url on the target site',
        ]);
        $scrapingData->addColumn('data', 'blob', [
            'notnull' => true,
            'comment' => 'The extra data',
        ]);
        $scrapingData->addColumn('queued_import', 'boolean', [
            'default' => true,
            'comment' => 'The course is queued to import',
        ]);
        $scrapingData->setPrimaryKey(['scraping_data_id']);
        $scrapingData->addUniqueIndex(['provider', 'source_id', 'type']);
        $scrapingData->addIndex(['source_id', 'title', 'source_url', 'type']);
        $scrapingData->addIndex(['provider']);
        $scrapingData->addIndex(['queued_import']);
    }

    private function tableScrapingState()
    {
        $scrapingState = $this->schemaManager->createTable('scraping_state');
        $scrapingState->addColumn('state_id', 'integer', [
            'unsigned'      => true,
            'autoincrement' => true,
            'comment'       => 'A unique id',
        ]);
        $scrapingState->addColumn('provider', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The data provider name',
        ]);
        $scrapingState->addColumn('timestamp', 'integer');
        $scrapingState->addColumn('data', 'blob', ['comment' => 'Store the extra data used for the compare expression']);
        $scrapingState->setPrimaryKey(['state_id']);
        $scrapingState->addIndex(['provider']);
    }

    public function tableScrapingMapping()
    {
        $map = $this->schemaManager->createTable('scraping_mapping');
        $map->addColumn('id', 'integer', [
            'unsigned'      => true,
            'autoincrement' => true,
            'comment'       => 'A unique id',
        ]);
        $map->addColumn('provider', 'string', [
            'comment' => 'The data provider name',
            'length'  => 255,
            'notnull' => true,
        ]);
        $map->addColumn('source_id', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'Linking to scraping_data.source_id',
        ]);
        $map->addColumn('remote_id', 'integer', [
            'unsigned' => true,
            'comment'  => 'The ID of entity created',
        ]);
        $map->addColumn('type', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The type of entity imported',
        ]);
        $map->setPrimaryKey(['id']);
        $map->addIndex(['source_id', 'provider', 'type']);
        return $this->schemaManager;
    }

    private function tablePrepareImportMap()
    {
        $map = $this->schemaManager->createTable('prepare_import_data');
        $map->addColumn('id', 'integer', [
            'unsigned'      => true,
            'autoincrement' => true,
            'comment'       => 'A unique id',
        ]);
        $map->addColumn('provider', 'string', [
            'comment' => 'The data provider name',
            'length'  => 255,
            'notnull' => true,
        ]);
        $map->addColumn('source_id', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'Linking to scraping_data.source_id',
        ]);
        $map->addColumn('source_url', 'string', [
            'length'  => 255,
            'notnull' => true,
            'comment' => 'The course url on the target site',
        ]);
        $map->addColumn('remote_id', 'integer', [
            'notnull'  => false,
            'unsigned' => true,
            'comment'  => 'The ID of entity created',
        ]);
        $map->addColumn('action', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'Insert, update, delete',
        ]);
        $map->addColumn('type', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The type of entity imported',
        ]);
        $map->addColumn('course_id', 'integer', [
            'notnull'  => false,
            'unsigned' => true,
            'comment'  => 'Course id of provider',
        ]);
        $map->addColumn('lo_id', 'integer', [
            'notnull'  => false,
            'unsigned' => true,
            'comment'  => 'Lo id on #lo',
        ]);
        $map->setPrimaryKey(['id']);
        $map->addIndex(['source_id', 'provider', 'type']);
        $map->addIndex(['course_id']);
    }

    private function tableFileUpload()
    {
        $map = $this->schemaManager->createTable('file_upload');
        $map->addColumn('id', 'integer', [
            'unsigned'      => true,
            'autoincrement' => true,
            'comment'       => 'A unique id',
        ]);
        $map->addColumn('source_url', 'string', [
            'notnull' => true,
            'length'  => 255,
            'comment' => 'The file url on remote site',
        ]);
        $map->addColumn('remote_url', 'string', [
            'length'  => 255,
            'notnull' => true,
            'comment' => 'The file url on cloudinary',
        ]);
        $map->addColumn('provider', 'string', [
            'comment' => 'The data provider name',
            'length'  => 255,
            'notnull' => true,
        ]);
        $map->setPrimaryKey(['id']);
        $map->addIndex(['source_url']);
    }
}
