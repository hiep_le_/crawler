<?php

namespace App;

use Madcoda\Youtube;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CommonServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $c)
    {
        $c['config.ctrl'] = function (Container $c) {
            return new ConfigController($c['application']);
        };

        $c['youtube'] = function (Container $c) {
            return new Youtube($c['youtube.options']);
        };

        $c['application'] = $c;

        $c['logger.ctrl'] = function (Container $c) {
            return new LoggerController($c['application']);
        };
    }
}
