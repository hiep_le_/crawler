<?php
/**
 * @author Tien Nguyen
 * @author Tien Nguyen <tien.nguyen@go1.com>
 * Date: 13/01/2017
 * Time: 11:06
 */

namespace App\Libraries;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use GuzzleHttp\Exception\RequestException;

class UploadHandle
{
    private $s3;
    private $jwt;
    private $params;

    public function __construct($config = [])
    {

        $this->params = $config;
        $this->client = Guzzle::getInstance('normal');
        $this->configCtrl = new ConfigController(new Application());
        $this->s3 = $this->configCtrl->getImportOptions('services/s3');
        $this->jwt = $this->configCtrl->getImportOptions("providers/learningheroes/jwt");
    }

    public function getLink()
    {
        try {
            $response = $this->client->post(
                $this->s3,
                [
                    'json'  => $this->params,
                    'query' => [
                        'jwt' => $this->jwt,
                    ],
                ]
            );
            if ($result = json_decode($response->getBody()->getContents())) {
                return $this->gets3DownloadLink($result);
            }

            return false;
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    public function gets3DownloadLink($params)
    {
        try {
            $resultUrl = $params->scheme . "://" . $params->host . $params->path;
            $requestUrl = $resultUrl .
                "?" . $params->query;

            // s3 response doesn't return anymore, just empty
            $this->client->put($requestUrl, [
                'timeout' => -1,
                'body'    => fopen($this->params['filePath'], 'r'),
            ]);

            return $resultUrl;
        }
        catch (RequestException $e) {
            echo 'error at ' . $requestUrl;

            return '';
        }
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function getContentType($filename)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        finfo_close($finfo);

        return $type;
    }

}