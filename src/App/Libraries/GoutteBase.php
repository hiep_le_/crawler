<?php
/**
 * @author Tien Nguyen
 * @author Tien Nguyen <tien.nguyen@go1.com>
 * Date: 12/01/2017
 * Time: 09:39
 */

namespace App\Libraries;

use Goutte\Client;
use GuzzleHttp\Exception\RequestException;

class GoutteBase
{
    public $client;

    /**
     * @var Dom element current selecting
     */
    public $cursor;

    public function __construct()
    {
        $this->client = new Client();
    }

    private static $blackList = [
        'www.',
    ];

    /**
     * @param $pdf
     * @return string
     */
    public function readPdf($pdf)
    {
        try {
            $parser = new \Smalot\PdfParser\Parser();
            $pdf = $parser->parseFile($pdf);

            return self::formatHtml($pdf->getText());
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * @param $input
     * @return string
     */
    public static function formatHtml($input)
    {
        $arr = explode(PHP_EOL, $input);
        // remove first line if this line inside blacklist
        $firstLine = substr($arr[0], 0, 4);

        $result = '';
        if (!empty($arr[0]) && in_array($firstLine, self::$blackList)) {
            unset($arr[0]);
        }
        if (!empty($arr)) {
            foreach ($arr as $value) {
                $result .= "<p>" . $value . "</p>";
            }
        }

        return $result;
    }

    /**
     * @param $url
     * @return string
     */
    public function getRawLink($url)
    {
        try {
            // get raw url
            return $this->gotoUrl($url)->getBaseHref();
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * Function download any file
     *
     * @param $url    | Url input
     * @param $output | Direct file output
     * @return bool
     *
     */
    public function downloadFile($url, $output)
    {
        try {
            // random delay request
            sleep(mt_rand(1, 3));
            $rawLink = $this->getRawLink($url);
            $ext = "." . pathinfo($rawLink, PATHINFO_EXTENSION);

            if (trim($ext) != ".") {
                $tempFile = basename($url, $ext) . "_" . time() . $ext;
                // Download file
                $this->client->getClient()->get($rawLink, [
                    'save_to'         => $output . $tempFile,
                    'allow_redirects' => false,
                ]);

                return $output . $tempFile;
            }

            return '';
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * @param $url
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function gotoUrl($url)
    {
        try {
            $this->cursor = $this->client->request('GET', $url, [
                'allow_redirects' => false,
            ]);

            return $this->cursor;
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * Proccess login
     *
     * @return bool
     */
    public function login($textLogin, $formsData)
    {
        try {
            $form = $this->cursor->selectButton($textLogin)->form();
            $this->cursor = $this->client->submit($form, $formsData);

            return true;
        }
        catch (RequestException $e) {
            echo 'error at ' . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * @param $path
     * @return bool
     */
    public function makeDir($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return true;
    }

    /**
     * @param $path
     * @return bool
     */
    public static function deleteAllFileInDir($path)
    {
        return is_file($path) ?
            @unlink($path) :
            array_map('unlink', glob($path . '/*'));
    }

    /**
     * @param $path
     * @return bool
     */
    public static function deleteDir($path)
    {
        $class_func = [__CLASS__, __FUNCTION__];

        return is_file($path) ?
            @unlink($path) :
            array_map($class_func, glob($path . '/*')) == @rmdir($path);
    }

}