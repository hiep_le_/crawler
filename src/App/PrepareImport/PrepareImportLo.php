<?php

namespace App\PrepareImport;

use App\ConfigController;
use App\Go1\Go1LoController;
use App\LoggerController;
use App\Scraping\ScrapingInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class PrepareImportLo extends PrepareImport implements PrepareImportInterface
{

    /**
     * @var \App\PrepareImport\PrepareImportData
     */
    private $prepareImportData;

    /**
     * @var \App\Go1\Go1LoController
     */
    private $go1LoCtrl;

    public function __construct(
        PrepareImportData $prepareImportData,
        Go1LoController $go1LoCtrl,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    )
    {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->prepareImportData = $prepareImportData;
        $this->go1LoCtrl = $go1LoCtrl;
    }

    public function getName()
    {
        return 'lo';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['learning_pathway', 'course', 'module'];
    }

    /**
     * @param                                 $providerName
     * @param \App\Scraping\ScrapingInterface $provider
     */
    public function push(ScrapingInterface $provider, $item = null)
    {
        $providerName = $provider->getName();
        $data = $item->data;
        if (!in_array($data['type'], $this->supportedTypes())) {
            throw new Exception('Wrong lo type');
        }

        $action = 'insert';
        if ($loId = $this->go1LoCtrl->searchLo($data, $providerName)) {
            $this->setEntityId($loId);
            $action = 'update';
        }

        $row = $this->prepareImportData->getPrepareImport($data['data']['course_id'], $providerName, $data['type']);

        $prepareImport = [
            'source_id'  => $data['data']['course_id'],
            'source_url' => $data['data']['course_url'],
            'remote_id'  => $loId,
            'action'     => $action,
            'provider'   => $providerName,
            'type'       => $data['type'],
        ];
        if ($row) {
            $this->prepareImportData->updatePrepareImport($prepareImport, $row->id);
            $this->isCreated = true;
        }
        else {
            $this->prepareImportData->insertPrepareImport($prepareImport);
            $this->isCreated = false;
        }
    }

}
