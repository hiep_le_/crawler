<?php

namespace App\PrepareImport;

use App\ConfigController;
use App\Go1\Go1LiController;
use App\Go1\Go1LoController;
use App\LoggerController;
use App\Scraping\ScrapingInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class PrepareImportLi extends PrepareImport implements PrepareImportInterface
{

    /**
     * @var \App\PrepareImport\PrepareImportData
     */
    private $prepareImportData;

    /**
     * @var \App\Go1\Go1LiController
     */
    private $go1LiCtrl;

    public function __construct(
        PrepareImportData $prepareImportData,
        Go1LiController $go1LiCtrl,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    )
    {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->prepareImportData = $prepareImportData;
        $this->go1LiCtrl = $go1LiCtrl;
    }

    public function getName()
    {
        return 'li';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['learning_pathway', 'course', 'module'];
    }

    /**
     * @param ScrapingInterface $provider
     * @param null              $item
     */
    public function push(ScrapingInterface $provider, $item = null)
    {
        $providerName = $provider->getName();
        $data = $this->getData();
        if (!in_array($data['type'], $this->supportedTypes())) {
            throw new Exception('Wrong li type');
        }

        $action = 'insert';
        if ($loId = $this->go1LiCtrl->searchLi($data, $providerName)) {
            $this->setEntityId($loId);
            //            $action = 'update';
        }

        $row = $this->prepareImportData->getPrepareImport($data['data']['course_id'], $providerName, $data['type']);
        $prepareImport = [
            'source_id'  => $data['data']['course_id'],
            'source_url' => $data['data']['course_url'],
            'remote_id'  => $loId,
            'action'     => $action,
            'provider'   => $providerName,
            'type'       => 'tincan',
        ];

        if ($row) {
            $this->prepareImportData->updatePrepareImport($prepareImport, $row->id);
            $this->isCreated = true;
        }
        else {
            $this->prepareImportData->insertPrepareImport($prepareImport);
            $this->isCreated = false;
        }
    }

}
