<?php

namespace App\PrepareImport;

use App\ConfigController;
use App\Exception\RuntimeException;
use App\Go1\Go1PortalController;
use App\Go1\Go1ScrapingController;
use App\Import\ImportMapData;
use App\LoggerController;
use App\Scraping\ScrapingInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DomCrawler\Crawler;
use App\Browser\Guzzle;
use go1\util\PortalChecker;

class PrepareImportPortal extends PrepareImport implements PrepareImportInterface
{
    private $prepareImportData;
    private $importMapData;
    private $go1PortalController;
    private $portalChecker;
    private $go1ScrapingController;

    public function __construct(
        PrepareImportData $prepareImportData,
        Go1ScrapingController $go1ScrapingController,
        Go1PortalController $go1PortalController,
        PortalChecker $portalChecker,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1PortalController = $go1PortalController;
        $this->portalChecker = $portalChecker;
        $this->prepareImportData = $prepareImportData;
        $this->go1ScrapingController = $go1ScrapingController;
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['portal'];
    }

    /**
     * @param $providerName
     * @param \App\Scraping\ScrapingInterface $provider
     * @param null $item
     */
    public function push(ScrapingInterface $provider, $item = null)
    {
        $isInsert = true;
        $providerName = $provider->getName();
        $data = $item->data;
        $sourceId = $item->source_id;


        if ($provider->hasPortalId()) {
            $mappingCourse = $this->go1ScrapingController->getMapping($providerName, $sourceId, 'portal');
            if ($mappingCourse) {
                $portal = $mappingCourse->remote_id;
            }
            else {
                $portal = $data['title'] . '.mygo1.com';
            }
            $portal = $this->go1PortalController->loadPortal($portal);
            if (!$portal) {

                $this->insertPrepareImport($sourceId, $item->source_url, $providerName);
            }
        }
    }

    public function insertPrepareImport($sourceId, $sourceUrl, $providerName)
    {
        $prepareImport = [
            'source_id' => $sourceId,
            'source_url' => $sourceUrl,
            'remote_id' => null,
            'action' => 'insert',
            'provider' => $providerName,
            'type' => 'portal'
        ];
        $this->prepareImportData->insertPrepareImport($prepareImport);
        $this->isCreated = false;
    }
}
