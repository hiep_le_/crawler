<?php

namespace App\PrepareImport;

use App\Scraping\ScrapingInterface;

interface  PrepareImportInterface
{
    /**
     * @return array
     */
    public function supportedTypes();

    /**
     * The data provider ID
     */
    public function getName();

    /**
     * Set the data to import
     * @param $data
     * @return mixed
     */
    public function setData($data);

    /**
     * Push the data to remote service
     * @param $providerName
     * @param \App\Scraping\ScrapingInterface $provider
     * @param null $item
     * @return mixed
     */
    public function push(ScrapingInterface $provider, $item = null);

    /**
     * Determine a entity is created or updated
     * @return mixed
     */
    public function isCreated();

    /**
     * Get id of entity imported
     * @return mixed
     */
    public function getEntityId();

    /**
     * This method calling every item processed
     * @return mixed
     */
    public function destroy();

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'prepare_import');
}