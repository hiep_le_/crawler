<?php

namespace App\PrepareImport;

use App\ConfigController;
use App\Exception\RuntimeException;
use App\Go1\Go1ScrapingController;
use App\Import\ImportMapData;
use App\LoggerController;
use App\Scraping\ScrapingInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DomCrawler\Crawler;
use App\Browser\Guzzle;

class PrepareImportNoteVote extends PrepareImport implements PrepareImportInterface
{
    private $prepareImportData;
    private $importMapData;
    private $go1ScrapingController;

    public function __construct(
        PrepareImportData $prepareImportData,
        Go1ScrapingController $go1ScrapingController,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->prepareImportData = $prepareImportData;
        $this->go1ScrapingController = $go1ScrapingController;
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['note'];
    }

    /**
     * @param $providerName
     * @param \App\Scraping\ScrapingInterface $provider
     */
    public function push(ScrapingInterface $provider, $item = null)
    {
        $providerName = $provider->getName();
        $data = $item->data;
        $course_id = $data['data']['course_id'];
        $mappingCourse = $this->go1ScrapingController->getMapping($providerName, $course_id, 'course');
        $loId = $mappingCourse->remote_id;

        if (!$provider->hasNoteId()) {
            $this->pushNoteNoId($data, $providerName, $course_id, $loId);
        }
        else {
            $this->pushNoteId($data, $providerName, $course_id, $loId);
        }
    }

    public function pushNoteNoId($providerName, $course_id, $loId)
    {
        $this->prepareImportData->deletePrepareImportByCourseId($course_id);
        $record = [
            'source_id' => '',
            'remote_id' => '',
            'course_id' => $course_id,
            'lo_id' => $loId,
            'action' => 'insert',
            'provider' => $providerName,
            'type' => 'note',
        ];

        $this->prepareImportData->insertPrepareImport($record);
    }

    public function pushNoteId($data, $providerName, $course_id, $loId)
    {
        foreach($data['data']['reviews'] as $review)
        {
            $record = [
                'source_id' => '',
                'remote_id' => '',
                'course_id' => $course_id,
                'lo_id' => $loId,
                'action' => 'insert',
                'provider' => $providerName,
                'type' => 'note'
            ];

            $record['source_id'] = $review['id'];
            // check note or vote exist on mapping table
            foreach(['note, vote'] as $type) {
                $mappingNote = $this->go1ScrapingController->getMapping($providerName, $review['id'], $type);
                if ($mappingNote)
                {
                    $record['remote_id'] = ($mappingNote) ? $mappingNote->remote_id : '';
                    $record['action'] = 'update';
                }
                // get prepare_mapping of vote and note
                $row = $this->prepareImportData->getPrepareImport($providerName, $record['source_id'], $type);
                ($row)
                    ? $this->prepareImportData->updatePrepareImport($record, $row->id)
                    : $this->prepareImportData->insertPrepareImport($record);
            }
        }
    }
}
