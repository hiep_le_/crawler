<?php

namespace App\PrepareImport;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PrepareImportServiceProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     */
    public function register(Container $c)
    {
        $c['prepare_import.data'] = function (Container $c) {
            return new PrepareImportData($c['db']);
        };
        $c['prepare_import.lo'] = function (Container $c) {
            return new PrepareImportLo($c['prepare_import.data'], $c['go1.lo.ctrl'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['prepare_import.li'] = function (Container $c) {
            return new PrepareImportLi($c['prepare_import.data'], $c['go1.li.ctrl'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['prepare_import.notevote'] = function (Container $c) {
            return new PrepareImportNoteVote(
                $c['prepare_import.data'],
                $c['go1.scraping.ctrl'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['prepare_import.portal'] = function (Container $c) {
            return new PrepareImportPortal(
                $c['prepare_import.data'],
                $c['go1.scraping.ctrl'],
                $c['go1.portal.ctrl'],
                $c['portal_checker'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['prepare_import.ctrl'] = function (Container $c) {
            return new PrepareImportController($c['application']);
        };
    }
}
