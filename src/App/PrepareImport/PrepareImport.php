<?php

namespace App\PrepareImport;

use App\ConfigController;
use App\LoggerController;

class PrepareImport
{
    private $data;
    private $entityId = null;
    protected $isCreated;
    private $configCtrl;
    private $loggerCtrl;

    public function __construct(ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        $this->configCtrl = $configCtrl;
        $this->loggerCtrl = $loggerCtrl;
    }

    public function isCreated() {
        return $this->isCreated;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }

    public function setEntityId($entityId) {
        $this->entityId = $entityId;
    }

    public function getEntityId() {
        return $this->entityId;
    }

    public function destroy() {
        $this->data = null;
        $this->entityId = null;
    }

    public function getName() {
    }

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'scraping')
    {
        $path = $this->configCtrl->getRootDir() . "/var/logs/prepare_import_{$name}.log";
        $this->loggerCtrl->setPathLogger($path);
        return $this->loggerCtrl->getLogger();
    }


}
