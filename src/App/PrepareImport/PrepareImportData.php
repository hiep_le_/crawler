<?php
namespace App\PrepareImport;
use App\Exception\RuntimeException;
use Doctrine\DBAL\Connection;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DomCrawler\Crawler;
use App\Browser\Guzzle;
use PDO;
class PrepareImportData
{
    private $db;
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }
    // data session
    public function getPrepareImport($source_id, $providerName, $type)
    {
        return $this->db->createQueryBuilder()
            ->select('*')
            ->from('prepare_import_data')
            ->where('provider = :provider')
            ->andWhere('source_id = :source_id')
            ->andWhere('type = :type')
            ->setParameters([
                'provider' => $providerName,
                'source_id' => $source_id,
                'type' => $type
            ])
            ->execute()
            ->fetch(PDO::FETCH_OBJ);
    }
    public function updatePrepareImport($prepareImport, $id)
    {
        $this->db->update('prepare_import_data', $prepareImport, ['id' => $id]);
    }
    public function insertPrepareImport($prepareImport)
    {
        $this->db->insert('prepare_import_data', $prepareImport);
    }
    public function deletePrepareImportByCourseId($loId)
    {
        $this->db->delete('prepare_import_data', ['course_id' => $loId]);
    }
}