<?php

namespace App\Scraping;

use Doctrine\DBAL\Connection;
use Exception;
use go1\util\DB;
use PDO;
use stdClass;

class ScrapingFileUpload
{

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function insert($data) {
        $this->db->beginTransaction();
        try {
            $this->db->insert('file_upload', $data);
            $this->db->commit();
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getBySourceUrl($sourceUrl) {
        $record = $this->db->createQueryBuilder()->select('*')
            ->from('file_upload')
            ->andWhere('source_url = :source_url')
            ->setParameter('source_url', $sourceUrl);
        return $record->execute()->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @param $fileUploadId
     * @param $remoteUrl
     */
    public function update($fileUploadId, $remoteUrl) {
        $this->db->update(
            'file_upload',
            ['remote_url' => $remoteUrl],
            ['id' => $fileUploadId]
        );
    }
}
