<?php

namespace App\Scraping\LearningItem\TED;

use App\Scraping\LearningItem\YoutubeAbstract;
use App\Scraping\ScrapingInterface;

class TEDScraping extends YoutubeAbstract implements ScrapingInterface
{
    public function getDataDetail($id)
    {
        $data = parent::getDataDetail($id);
        $data['author'] = [
            'full_name' => 'TED',
            'avatar'    => 'https://yt3.ggpht.com/-bonZt347bMc/AAAAAAAAAAI/AAAAAAAAAAA/lR8QEKnqqHk/s100-c-k-no-mo-rj-c0xffffff/photo.jpg',
        ];

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'ted';
    }

    function getChannelVideosUrl()
    {
        return 'https://www.youtube.com/user/TEDtalksDirector/videos';
    }
}
