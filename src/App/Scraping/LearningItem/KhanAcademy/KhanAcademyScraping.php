<?php

namespace App\Scraping\LearningItem\KhanAcademy;

use App\Scraping\LearningItem\YoutubeAbstract;
use App\Scraping\ScrapingInterface;

class KhanAcademyScraping extends YoutubeAbstract implements ScrapingInterface
{
    public function getDataDetail($id)
    {
        $data = parent::getDataDetail($id);
        $data['author'] = [
            'full_name' => 'Khan Academy',
            'avatar'    => 'https://cdn.kastatic.org/images/khan-logo-vertical-transparent.png',
        ];

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'khanacademy';
    }

    function getChannelVideosUrl()
    {
        return 'https://www.youtube.com/user/khanacademy/videos';
    }
}
