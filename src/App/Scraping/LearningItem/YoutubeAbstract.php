<?php

namespace App\Scraping\LearningItem;

use App\Application;
use App\Browser\Guzzle;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use Madcoda\Youtube;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

abstract class YoutubeAbstract extends ScrapingBase implements ScrapingInterface
{
    private $client;
    private $scrapingData;
    private $youtube;
    private $logger;
    private $nextPageUrl;
    const BASE_URL = 'https://www.youtube.com/';

    public function __construct(Guzzle $client, ScrapingData $scrapingData, Youtube $youtube, LoggerInterface $logger)
    {
        $this->client = $client::getInstance('normal');
        $this->scrapingData = $scrapingData;
        $this->youtube = $youtube;
        $this->logger = $logger;
    }

    public function getList($limit = null)
    {
        // Get scrape state
        if (!$this->nextPageUrl) {
            $state = $this->scrapingData->getScrapingState($this->getName());
            if (!empty($state->data['next_page_url'])) {
                $this->nextPageUrl = $state->data['next_page_url'];
            }
            else {
                $this->nextPageUrl = $state->data['next_page_url'] = null;
            }
        }

        if ($this->nextPageUrl) {
            $content = json_decode($this->client->get($this->nextPageUrl)->getBody()->getContents(), true);
            $button = $content['load_more_widget_html'];
            $content = $content['content_html'];
        }
        else {
            $content = $this->client->get($this->getChannelVideosUrl())->getBody()->getContents();
        }

        $crawl = new Crawler();
        $crawl->addContent($content);
        $ids = [];
        $crawl->filter('.channels-content-item')->each(function (Crawler $node) use (&$ids) {
            $link = $node->filter('.channels-content-item .yt-lockup-thumbnail a');
            if ($link->count() > 0) {
                $params = $this->youtube->_parse_url_query($link->attr('href'));
                if (isset($params['v'])) {
                    $ids[$params['v']] = $params['v'];
                }
            }
        });

        $this->nextPageUrl = $this->extractNextPageUrl(!empty($button) ? $button : $content);
        if ($this->nextPageUrl === false) {
            return null;
        }

        return $ids;
    }

    public function getDataDetail($id)
    {
        $videoInfo = $this->youtube->getVideoInfo($id);

        if (!$videoInfo) {
            $this->logger->logger($this->getName())->addDebug('Video was ignored because could not load the meta data information', ['data' => $item]);

            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $data['title'] = $videoInfo->snippet->title;
        $data['source_url'] = 'https://www.youtube.com/watch?v=' . $videoInfo->id;
        $data['source_id'] = $videoInfo->id;
        $data['type'] = 'video';
        $data['language'] = 'en';
        $data['private'] = 0;
        $data['published'] = 1;
        $data['marketplace'] = 1;
        $data['image'] = $videoInfo->snippet->thumbnails->high->url;
        $data['data'] = [
            'source_id'    => $data['source_id'],
            'source_url'   => $data['source_url'],
            'downloadable' => true,
            'online'       => '//www.youtube.com/watch?v=' . $videoInfo->id,
            'videoType'    => 'online',
            'provider'     => 'youtube',
        ];
        if ($descriptionParts = explode("\n", $videoInfo->snippet->description)) {
            foreach ($descriptionParts as $key => $description) {
                $data['description'][$key] = trim($description);
                if (empty($data['description'][$key])) {
                    unset($data['description'][$key]);
                }
                else {
                    $data['description'][$key] = "<p>{$data['description'][$key]}</p>";
                }
            }
            $data['description'] = implode('', $data['description']);
        }

        return $data;
    }

    public function getStateData()
    {
        return [
            'next_page_url' => $this->nextPageUrl,
        ];
    }

    public function extractNextPageUrl($html)
    {
        $crawl = new Crawler();
        $crawl->addContent($html);
        $button = $crawl->filter('.browse-items-load-more-button');
        if ($button->count() > 0) {
            return self::BASE_URL . trim($button->attr('data-uix-load-more-href'), '/');
        }

        return false;
    }

    abstract function getChannelVideosUrl();
}
