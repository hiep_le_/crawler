<?php

namespace App\Scraping\LearningItem\Wikihow;

use App\Application;
use App\Browser\Guzzle;
use App\LocaleMap;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Stichoza\GoogleTranslate\TranslateClient;
use Symfony\Component\DomCrawler\Crawler;

class WikihowScraping extends ScrapingBase implements ScrapingInterface
{
    private $client;
    private $wikihowCategories;
    private $remainingWikihowCategories;
    private $start;
    private $scrapingData;
    private $logger;

    const BASE_URL = 'http://www.wikihow.com';

    public function __construct(Guzzle $client, ScrapingData $scrapingData, LoggerInterface $logger)
    {
        $self = $this;
        $this->client = $client::getInstance('wikihow', [
            'base_uri' => self::BASE_URL,
            'timeout'  => 60,
            'on_stats' => function (TransferStats $stats) use (&$self) {
                $self->setMessage($stats->getEffectiveUri(), 'currentUrl');
            },
        ]);
        $this->scrapingData = $scrapingData;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function getList($limit = null)
    {
        // Init scrape
        if (!$this->start) {
            $state = $this->scrapingData->getScrapingState($this->getName());
            $this->start = isset($state->data['start']) && $state->data['start'] > 0 ? $state->data['start'] : 1;
            $this->wikihowCategories = !empty($state->data['categories']) ? $state->data['categories'] : $this->getWikihowCategories();
            $this->remainingWikihowCategories = !empty($state->data['remaining_categories']) ? $state->data['remaining_categories'] : $this->wikihowCategories;
        }

        if (empty($this->remainingWikihowCategories)) {
            return null;
        }

        $this->setMessage('Getting list of courses');
        $workingCategory = reset(array_slice($this->remainingWikihowCategories, 0, 1));
        try {
            $response = $this->client->get($workingCategory['path'], [
                'query' => [
                    'restaction' => 'pull-chunk',
                    'start'      => $this->start,
                ],
            ]);
        }
        catch (RequestException $ex) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        if ((!$content = json_decode($response->getBody()->getContents(), true)) || !$content['html']) {
            // Current category is scrape completed, continue for another
            unset($this->remainingWikihowCategories[$workingCategory['path']]);
            // Reset start
            $this->start = 1;

            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $crawler = new Crawler();
        $crawler->addContent($content['html']);
        $articlesInfo = [];
        $self = $this;
        $crawler->filter('table .thumbnail a')->each(function (Crawler $node) use (&$self, &$articlesInfo, $workingCategory) {
            $pathInfo = parse_url(trim($node->attr('href')));
            $id = trim($pathInfo['path'], '/ ');
            $articlesInfo[$id] = [
                'category'   => $workingCategory['name'],
                'source_url' => $self::BASE_URL . '/' . $id,
                'source_id'  => $id,
            ];
            $self->start++;
        });

        return $articlesInfo;
    }

    /**
     * @inheritdoc
     */
    public function getDataDetail($item)
    {
        $data = [];
        try {
            $content = $this->client->get($item['source_url'])->getBody()->getContents();
        }
        catch (RequestException $ex) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        try {
            $crawler = new Crawler();
            $crawler->addContent($content);
            $sectionMethodToc = $crawler->filter('#intro #method_toc');
            $data['title'] = trim($crawler->filter('#intro h1.firstHeading a')->text());
            $data['source_url'] = $item['source_url'];
            $data['source_id'] = $item['source_id'];
            $data['description'] = $this->getDescription($sectionMethodToc, true);
            $data['type'] = 'resource';
            $data['private'] = 0;
            $data['published'] = 1;
            $data['marketplace'] = 1;
            $image = $crawler->filter('.section.steps')->last()->filter('li img')->last();
            if ($image->count() > 0) {
                $data['image'] = $image->attr('src');
            }
            $data['data'] = [
                'source_id'         => $item['source_id'],
                'source_url'        => $item['source_url'],
                // Currently the LO service do not support tagging for LI, Temporary store on this
                'original_category' => $item['category'],
            ];
            $data['data']['reviewed'] = false;
            if (($reviewedSection = $crawler->filter('#intro .sp_intro_user')) && $reviewedSection->count() > 0) {
                $data['data']['reviewed'] = true;
                $data['data']['reviewed_by'] = 'user';
            }
            elseif (($reviewedSection = $crawler->filter('#intro .sp_intro_expert')) && $reviewedSection->count() > 0) {
                $data['data']['reviewed'] = true;
                $data['data']['reviewed_by'] = 'expert';
            }
            $data['description'] .= sprintf('<p><strong>Review status:</strong> %s</p>', $data['data']['reviewed'] ? $data['data']['reviewed_by'] : 'No');
            if ($sectionMethodToc) {
                $methods['label'] = $sectionMethodToc->filter('span');
                if ($methods['label']->count() > 0 && ($methods['label'] = $methods['label']->text())) {
                    $sectionMethodToc->filter('a')->each(function (Crawler $node) use (&$methods) {
                        if ($node->attr('id') !== 'qa_toc') {
                            $methods['method'][] = $node->text();
                        }
                    });
                    if ($methods['method'] && $methods['label']) {
                        $methods['method_text'] = '';
                        foreach ($methods['method'] as $text) {
                            $methods['method_text'] .= '<li>' . $text . '</li>';
                        }
                        $methods['method_text'] = '<ul>' . $methods['method_text'] . '</ul>';
                        $data['description'] .= sprintf('<strong>%s</strong> %s', $methods['label'], $methods['method_text']);
                    }
                }
            }
            $data['author'] = [
                'full_name' => 'wikihow',
                'avatar'    => 'http://pad2.whstatic.com/skins/owl/images/wikihow_logo.png',
            ];

            // Detect language
            try {
                $translate = new TranslateClient(null);
                $translate->translate($data['description']);
                $data['language'] = $translate->getLastDetectedSource();
                if (strlen($data['language']) > 2) {
                    // Convert locale to language code
                    $data['language'] = (new LocaleMap())->getLanguageCode(strtolower($data['language']));
                }
            }
            catch (Exception $ex) {
                $data['language'] = 'en';
            }
        }
        catch (InvalidArgumentException $ex) {
            $this->logger->logger($this->getName())->addAlert('Record ignored', ['exception' => $ex, 'item' => $item]);

            return Application::SCRAPING_STATUS_CONTINUE;
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getStateData()
    {
        return [
            'start'                => $this->start,
            'categories'           => $this->wikihowCategories,
            'remaining_categories' => $this->remainingWikihowCategories,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'wikihow';
    }

    private function getWikihowCategories()
    {
        $this->setMessage("Fetching wikihow's categories.");
        $response = $this->client->get(urlencode('Special:Categorylisting'));
        $crawler = new Crawler();
        $crawler->addContent($response->getBody()->getContents());
        $sectionText = $crawler->filter('#bodycontents .section_text');
        $categories = [];
        $sectionText->filter('a')->each(function (Crawler $node) use (&$categories) {
            $href = str_replace(':', '%3A', trim($node->attr('href'), '/'));
            $categories[$href] = ['path' => $href, 'name' => trim($node->filter('.text')->text())];
        });

        if (!$categories) {
            throw new Exception('Can not get the category from wikihow.');
        }

        return $categories;
    }

    private function getDescription($node, $reset = false)
    {
        static $description;
        $reset && $description = '';
        $nextNode = $node->nextAll();
        if (strpos($nextNode->attr('class'), 'clearall') === false) {
            $description .= '<p>' . $nextNode->html() . '</p>';

            return $this->getDescription($nextNode);
        }
        else {
            return $description;
        }
    }
}
