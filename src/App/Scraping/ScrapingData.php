<?php

namespace App\Scraping;

use Doctrine\DBAL\Connection;
use Exception;
use PDO;
use stdClass;

class ScrapingData
{

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Save the scraping data to DB
     *
     * @param      $data
     * @param      $dataProviderName
     * @param null $type
     */
    public function saveScrapingData($data, $dataProviderName)
    {
        $this->db->beginTransaction();
        try {
            $data1 = $data;
            unset($data1['source_id'], $data1['source_url']);
            $scrapingData = [
                'provider'      => $dataProviderName,
                'source_id'     => $data['source_id'],
                'title'         => $data['title'],
                'type'          => $data['type'],
                'source_url'    => $data['source_url'],
                'data'          => json_encode($data1),
                'queued_import' => true,
            ];

            // Does not add duplicate record
            $data = $this->getScrapingDataBySourceId($data['source_id'], $dataProviderName, $data['type']);
            // Save to db

            if (!empty($scrapingData['source_id'])) {
                if ($data) {
                    $this->db->update(
                        'scraping_data',
                        $scrapingData,
                        ['scraping_data_id' => $data->scraping_data_id]
                    );
                } else {
                    $this->db->insert('scraping_data', $scrapingData);
                }

            }

            $this->db->commit();
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Update queue status of a scraping record
     *
     * @param integer $scrapingDataId
     * @param boolean $queueStatus
     */
    public function updateScrapingRecordQueueStatus(
        $scrapingDataId,
        $queueStatus
    )
    {
        $this->db->update(
            'scraping_data',
            ['queued_import' => $queueStatus],
            ['scraping_data_id' => $scrapingDataId]
        );
    }

    /**
     * Get the scraping data by source id and provider ID
     *
     * @param string $sourceId     The id on the target site
     * @param string $providerName The provider ID
     * @return stdClass
     */
    public function getScrapingDataBySourceId($sourceId, $providerName, $type = null)
    {
        $record = $this->db->createQueryBuilder()->select('*')
                           ->from('scraping_data')
                           ->andWhere('source_id = :source_id')
                           ->andWhere('provider = :provider')
                           ->setParameter('source_id', $sourceId)
                           ->setParameter('provider', $providerName);

        if ($type) {
            $record
                ->andWhere('type = :type')
                ->setParameter('type', $type);
        }

        $record = $record->execute()->fetch(PDO::FETCH_OBJ);

        if ($record) {
            if (isset($record->data)) {
                $record->data = json_decode($record->data, true);
            }

            return $record;
        }
    }

    /**
     * Add the state data
     *
     * @param ScrapingInterface $provider The data provider controller instance
     * @throws Exception
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function addState(ScrapingInterface $provider)
    {
        $this->db->beginTransaction();
        try {
            // Delete exist state
            $this->deleteScrapingState($provider->getName());

            // Insert new state
            $this->db->insert(
                'scraping_state',
                [
                    'provider'  => $provider->getName(),
                    'timestamp' => time(),
                    'data'      => json_encode($provider->getStateData() ?: []),
                ]
            );

            $this->db->commit();
        }
        catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Delete the state of an special data provider
     *
     * @param $dataProviderName The data provider ID
     */
    public function deleteScrapingState($dataProviderName)
    {
        $this->db->delete('scraping_state', ['provider' => $dataProviderName]);
    }

    /**
     * Get the state of an special data provider
     *
     * @param $dataProviderName The data provider ID
     * @return stdClass
     */
    public function getScrapingState($dataProviderName)
    {
        $record = $this->db->createQueryBuilder()->select('*')
                           ->from('scraping_state')
                           ->where('provider = :provider')
                           ->setParameter('provider', $dataProviderName)
                           ->orderBy('timestamp', 'DESC')
                           ->orderBy('state_id', 'DESC')
                           ->setMaxResults(1)->execute()->fetch(PDO::FETCH_OBJ);

        if ($record) {
            $record->data = json_decode($record->data, true);

            return $record;
        }
    }

    /**
     * Get the records are queued to import
     *
     * @param     $types
     * @param     $provider
     * @param int $limit
     * @return array
     */
    public function getScrapingRecords(
        $types = [],
        $provider = null,
        $limit = 100
    )
    {
        $query = $this->db
            ->createQueryBuilder()
            ->select('*')
            ->from('scraping_data');
        $limit && $query->setMaxResults($limit);
        if ($types && is_array($types)) {
            $query->setParameter('types', $types, Connection::PARAM_STR_ARRAY)
                  ->andWhere('type IN (:types)');
        }
        $provider && $query->setParameter('provider', $provider)->andWhere(
            'provider = :provider'
        );
        $records = $query->execute()->fetchAll(PDO::FETCH_OBJ);
        $items = [];
        foreach ($records as $record) {
            $record->data = json_decode($record->data, true);
            $items[$record->scraping_data_id] = $record;
        }

        return $items;
    }

    /**
     * Get the records are queued to import
     *
     * @param     $types
     * @param     $provider
     * @param int $limit
     * @return array
     */
    public function getPrepareImportRecords(
        $types = [],
        $provider = null,
        $limit = 100
    )
    {
        $query = $this->db
            ->createQueryBuilder()
            ->select('*')
            ->from('prepare_import_data');

        $limit && $query->setMaxResults($limit);
        if ($types && is_array($types)) {
            $query->setParameter('types', $types, Connection::PARAM_STR_ARRAY)
                  ->andWhere('type IN (:types)');
        }

        $provider && $query->setParameter('provider', $provider)->andWhere(
            'provider = :provider'
        );
        $records = $query->execute()->fetchAll(PDO::FETCH_OBJ);

        return $records;
    }

    public function deletePrepareImportRecord($item)
    {
        $this->db->delete('prepare_import_data', ['id' => $item->id]);
    }
}
