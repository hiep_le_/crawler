<?php

namespace App\Scraping;

use App\Scraping\Course\Allconferences\AllconferencesScraping;
use App\Scraping\Course\Course360Training\Course360TrainingScraping;
use App\Scraping\Course\Coursera\CourseraScraping;
use App\Scraping\Course\E3Learning\E3LearningScraping;
use App\Scraping\Course\LearningHeroes\LearningHeroesScraping;
use App\Scraping\Course\Lynda\LyndaScraping;
use App\Scraping\Course\Open2Study\Open2StudyScraping;
use App\Scraping\Course\Opensesame\OpensesameScraping;
use App\Scraping\Course\Udacity\UdacityScraping;
use App\Scraping\Course\Udemy\UdemyScraping;
use App\Scraping\LearningItem\KhanAcademy\KhanAcademyScraping;
use App\Scraping\LearningItem\TED\TEDScraping;
use App\Scraping\LearningItem\Wikihow\WikihowScraping;
use App\Test\Scraping\Course360Training\Course360TrainingScrapingTest;
use App\Test\Scraping\Lynda\LyndaScrapingTest;
use App\Test\Scraping\Udacity\UdacityScrapingTest;
use App\Test\Scraping\Udemy\UdemyScrapingTest;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Process\ProcessBuilder;

class ScrapingServiceProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        // root
        $c['scraping.ctrl'] = function (Container $c) {
            return new ScrapingController($c['application']);
        };
        $c['scraping.data'] = function (Container $c) {
            return new ScrapingData($c['db']);
        };
        $c['scraping.file_upload'] = function (Container $c) {
            return new ScrapingFileUpload($c['db']);
        };
        // scraping
        $c['scraping.udemy'] = function ($c) {
            return new UdemyScraping($c['browser.guzzle'], $c['scraping.data'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.lynda'] = function ($c) {
            return new LyndaScraping($c['browser.guzzle'], $c['scraping.data'], $c['config.ctrl'], $c['logger'], $c['logger.ctrl']);
        };
        $c['scraping.udacity'] = function ($c) {
            return new UdacityScraping($c['browser.guzzle'], $c['scraping.data'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.coursera'] = function ($c) {
            return new CourseraScraping($c['browser.guzzle'], $c['scraping.data'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.open2study'] = function ($c) {
            return new Open2StudyScraping($c['browser.guzzle'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.opensesame'] = function ($c) {
            return new OpensesameScraping($c['browser.guzzle'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.wikihow'] = function ($c) {
            return new WikihowScraping($c['browser.guzzle'], $c['scraping.data'], $c['logger.ctrl']);
        };
        $c['scraping.khanacademy'] = function ($c) {
            return new KhanAcademyScraping($c['browser.guzzle'], $c['scraping.data'], $c['youtube'], $c['logger.ctrl']);
        };
        $c['scraping.ted'] = function ($c) {
            return new TEDScraping($c['browser.guzzle'], $c['scraping.data'], $c['youtube'], $c['logger.ctrl']);
        };
        $c['scraping.e3learning'] = function ($c) {
            return new E3LearningScraping($c['browser.guzzle'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.course360training'] = function ($c) {
            return new Course360TrainingScraping($c['browser.guzzle'], $c['config.ctrl'], $c['logger.ctrl']);
        };
        $c['scraping.allconferences'] = function ($c) {
            return new AllconferencesScraping(
                $c['browser.guzzle'],
                $c['go1.cloudinary.ctrl'],
                $c['scraping.file_upload'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        //Scraping Learning Hero
        $c['scraping.learningheroes'] = function ($c) {
            return new LearningHeroesScraping($c['config.ctrl'], $c['logger.ctrl'], $c['db']);
        };
    }
}
