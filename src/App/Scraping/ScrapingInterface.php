<?php

namespace App\Scraping;

use Symfony\Component\Console\Helper\ProgressBar;

interface ScrapingInterface
{
    /**
     * This is a responsible method get the entry list from the target site
     * @param null $limit
     * @return mixed
     */
    public function getList($limit = null);

    /**
     * Perform parse the data was returned from getList() into a valid object
     *
     * @param $item
     * @return array
     */
    public function getDataDetail($item);

    /**
     * @param $item
     * @return mixed
     */
    public function getNote($item);

    /**
     * @param $item
     * @return mixed
     */
    public function getPortal($item);

    /**
     * The extra data stored in scraping_state, useful for resuming
     */
    public function getStateData();

    /**
     * The data provider ID
     */
    public function getName();

    /**
     * @param \Symfony\Component\Console\Helper\ProgressBar $progress
     * @return mixed
     */
    public function setProgress(ProgressBar $progress);

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'scraping');

    /**
     * @return boolean
     */
    public function hasNoteId();

    /**
     * @return boolean
     */
    public function hasPortalId();
}
