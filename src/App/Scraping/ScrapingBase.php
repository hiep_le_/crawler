<?php

namespace App\Scraping;

use App\ConfigController;
use App\LoggerController;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DomCrawler\Crawler;

abstract class ScrapingBase
{
    private $progress;
    private $configCtrl;
    private $loggerCtrl;

    const LO_EVENT_DATE_FORMAT = 'Y-m-d H:i:s';

    public function __construct(ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        $this->configCtrl = $configCtrl;
        $this->loggerCtrl = $loggerCtrl;
    }

    /**
     * Set the progress bar instance
     *
     * @param ProgressBar $progress
     */
    public function setProgress(ProgressBar $progress)
    {
        $this->progress = $progress;
    }

    /**
     * Get and validate Go1 categories
     *
     * @param string|array $categoryNames Category name to get
     * @return array Categories' name
     * @throws Exception
     */
    public final function getCategories($categoryNames)
    {
        $categoryNames = (array) $categoryNames;
        $returnCategory = [];
        $go1Categories = [
            "Business"             => [
                "General Business Skills",
                "Industry Specific",
                "Operations",
                "Development",
                "Safety and Health",
                "Sales & Marketing",
            ],
            "Technology"           => [
                "Digital Literary & IT Skills",
                "Development",
                "Design",
                "Understanding/Analysing Data",
            ],
            "Compliance"           => [
                "Certifications",
                "Industry Specific Compliance",
            ],
            "Personal Development" => [
                "Language Learning",
                "Soft Skills",
            ],
            "Academics"            => [
                "Arts & Humanities",
                "Maths & Science",
                "Social Science",
                "Computer Science",
            ],
        ];

        $categoryCount = 0;
        foreach ($categoryNames as $categoryName) {
            // find all categories for current category name
            foreach ($go1Categories as $parentCategory => $childrenCategories) {
                if ($parentCategory == $categoryName) {
                    $returnCategory[] = $parentCategory;
                    break;
                }

                if (in_array($categoryName, $childrenCategories)) {
                    $returnCategory[] = $parentCategory;
                    $returnCategory[] = $categoryName;
                    break;
                }
            }

            // insert tag if not exists
            if ((count($returnCategory) - $categoryCount) === 0) {
                $returnCategory[] = $categoryName;
            }
            $categoryCount = count($returnCategory);
        }

        return array_values(array_unique($returnCategory));
    }

    /**
     * HTML formatter and beautifier
     */
    protected function formatHtml($html)
    {
        $domCrawler = new Crawler();
        $domCrawler->addHtmlContent($html);

        $domCrawler->filter('*')->each(function (Crawler $crawler) {
            // Remove blank html element
            if ($crawler->count() > 0 && empty(trim($crawler->text()))) {
                foreach ($crawler as $node) {
                    $node->parentNode->removeChild($node);
                }
            }

            // Remove the attributes
            if ($crawler->count() > 0 && $crawler->getNode(0)->hasAttributes()) {
                $attributes = $crawler->getNode(0)->attributes;
                for ($i = $attributes->length; $i >= 0; $i--) {
                    $attr = $attributes->item($i);
                    if ($attr && !in_array($attr->nodeName, ['href', 'src'])) {
                        $crawler->getNode(0)->removeAttributeNode($attr);
                    }
                }
            }
        });

        // Remove <script> tags
        $domCrawler->filter('script')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        if ($domCrawler->count() > 0 && $domCrawler->children()->count() > 0) {
            return $domCrawler->children()->html();
        }

        return '';
    }

    /**
     * Display a message to progress bar
     *
     * @param string $message
     * @param string $name
     */
    protected function setMessage($message, $name = 'message')
    {
        if ($this->progress) {
            $this->progress->setMessage($message, $name);
            $this->progress->display();
        }
    }

    protected function getDateTime($time) {
        return $time;
    }

    public function getNote($item) {
    }

    public function getPortal($row) {
    }

    public function hasNoteId()
    {
        return true;
    }

    public function hasPortalId() {
        return true;
    }

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'scraping')
    {
        $path = $this->configCtrl->getRootDir() . "/var/logs/scraping_{$name}.log";
        $this->loggerCtrl->setPathLogger($path);

        return $this->loggerCtrl->getLogger();
    }
}
