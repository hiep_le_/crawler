<?php

namespace App\Scraping;

use App\Application;
use App\DataProvider\DataProviderInterface;

class ScrapingController
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get the list of data provider name
     *
     * @return array
     */
    public function getProviderNames()
    {
        return array_filter(array_map(function ($name) {
            $nameInfo = explode('.', $name);
            if (count($nameInfo) === 2 && $nameInfo[0] === 'scraping') {
                if (!in_array($nameInfo[1], ['ctrl', 'data', 'file_upload'])) {
                    return $nameInfo[1];
                }
            }
        }, $this->app->keys()));
    }

    /**
     * @param $name
     * @return ScrapingInterface
     */
    public function getProvider($name)
    {
        return $this->app["scraping.$name"];
    }
}
