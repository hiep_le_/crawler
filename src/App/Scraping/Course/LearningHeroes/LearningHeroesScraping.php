<?php

namespace App\Scraping\Course\LearningHeroes;

use App\Application;
use App\ArrayHelper;
use App\ConfigController;
use App\Libraries\GoutteBase;
use App\Libraries\UploadHandle;
use App\LoggerController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use Doctrine\DBAL\Connection;

class LearningHeroesScraping extends ScrapingBase implements ScrapingInterface
{
    /** @var  Application */
    const  BASE_URL   = 'https://vi237.customerhub.net/';
    const  EMAIL      = 'mlubega@go1.com';
    const  PASSWORD   = 'saveusfromboring';
    const  TINCAN_URL = 'https://vi237.customerhub.net/tin-can-packages';

    public  $tempDir;
    public  $packageDir;
    public  $liDir;
    private $configCtrl;
    private $db;

    // Global value keep all data at one
    public $courseData;
    public $_id;

    // Global value to skip scraping if course empty
    private $tempFilter;
    public  $client;

    private $uploadParams = [];

    /**
     * LearningHeroesScraping constructor.
     *
     * @param ConfigController $configCtrl
     * @param LoggerController $loggerCtrl
     */
    public function __construct(ConfigController $configCtrl, LoggerController $loggerCtrl, Connection $db)
    {

        $this->db = $db;
        $this->client = new GoutteBase();

        $this->configCtrl = $configCtrl;
        $this->tempDir = $this->configCtrl->getRootDir() . 'files/temp/';
        $this->packageDir = $this->configCtrl->getRootDir() . 'files/package/';

        // define upload params
        $this->uploadParams = [
            "portal" => "mygo1.com",
            "app"    => "course-review",
        ];
        parent::__construct($configCtrl, $loggerCtrl);
    }

    /**
     * @return bool
     */
    public function behavior()
    {
        try {
            $this->setMessage('Login...');
            // Go to login
            $this->client->gotoUrl(self::BASE_URL);
            if ($this->client->login('Log in', ['email' => self::EMAIL, 'password' => self::PASSWORD])) {
                // Go to scraping parent course
                $this->setMessage('Scraping course...');
                $this->client->gotoUrl(self::TINCAN_URL);

                // Update course data
                $this->setCourseData($this->scrapingCourse('.scorm-table table'));

                // go to scraping child file download of this course
                $this->setMessage('Scraping file prepare for download...');
                $this->gotoGetDownload('.resources-table tr');

                // Re - Update course data
                $this->setCourseData($this->tempFilter);

                // 1. Download file
                // 2. get link from s3
                // 3. assign link to global value
                $this->downloadFiles();

                return true;
            }

            return false;
        }
        catch (RequestException $e) {
            $this->logger($this->getName())->addError('Course url is error: ' . $e->getRequest()->getUri());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        if ($this->behavior()) {
            return $this->courseData;
        }
    }

    /**
     * Set course Data
     *
     * @param $data
     */
    public function setCourseData($data)
    {
        $this->courseData = $data;
    }

    /**
     * Get link file prepare for download
     *
     * @param $element
     */
    public function gotoGetDownload($element)
    {
        if (!empty($this->courseData)) {
            foreach ($this->courseData as $key => $value) {
                if (!empty($value['source_url'])) {
                    //check course is exist
                    if ($this->checkCourseExist('learningheroes', md5(trim($value['source_url'])))) {
                        continue;
                    };
                    // Go to child
                    $this->client->gotoUrl($value['source_url']);
                    // scraping to get file
                    $this->scrapingFileForDownload($key, $value, $element);
                    //break for fast debug download
                    //                    if ($key > 2) {
                    //                        break;
                    //                    }

                }
            }
        }
    }

    public function checkCourseExist($providerName, $source_id, $type = 'course')
    {
        try {
            $result = $this->db->createQueryBuilder()
                               ->select('source_id')
                               ->from('scraping_data')
                               ->where('provider = :provider')
                               ->andWhere('source_id = :source_id')
                               ->andWhere('type = :type')
                               ->setParameters([
                                   'provider'  => $providerName,
                                   'source_id' => $source_id,
                                   'type'      => $type,
                               ])
                               ->execute()
                               ->fetch(\PDO::FETCH_OBJ);
            if (!empty($result)) {
                return true;
            }

            return false;
        }
        catch (RequestException $e) {
            $this->logger($this->getName())->addError('Course url is error: ' . $e->getRequest()->getUri());

            return '';
        }
    }

    /**
     * scraping to get url of thumb, description, package prepare for download
     *
     * @param $key
     * @param $value
     * @param $element
     */
    public function scrapingFileForDownload($key, $value, $element)
    {
        $tempResource = [];
        $this->client->cursor->filter($element)->each(function ($node) use ($key, $value, $element, $tempResource) {
            $p = $node->children()->filter('p');
            // get resource files
            $th = $node->parents()->filter($element)->children()->filter('th');
            //Go to resources and get all link download
            if ($th->count() > 0 && trim($th->text()) === "Resources") {
                $scrom = $th->parents()->filter('table')->children()->filter('.download-resource-btn');

                if ($scrom->count() > 0) {
                    $resource = $scrom->each(function ($node1) {
                        $title = $node1->parents()->filter('tr')->children()->filter('p');

                        return [
                            'title' => $title->text(),
                            'url'   => $node1->link()->getUri(),
                        ];
                    });

                    //reassign S3 file
                    foreach ($resource as $v) {
                        $tempResource[$v['title']] = $v['url'];
                    }
                    if (!empty($value['source_id'])) {
                        $this->tempFilter[$key]['resource'] = $tempResource;
                    }
                }
            }
            //end get resource files

            // Check if Url description exist will change cursor go to next and previous dom to get url
            if ($p->count() > 0 && $p->text() == "Course Description") {

                // Find dom node to get link download
                $packageUrl = $node->previousAll()->children()->filter('.download-resource-btn');
                $descriptionUrl = $node->children()->filter('.download-resource-btn');
                $thumbUrl = $node->nextAll()->children()->filter('.download-resource-btn');

                //Set scraping file downloaded
                $this->tempFilter[$key] = array_merge($value, [
                        'type'            => 'course',
                        'resource'        => !empty($tempResource) ? $tempResource : '[]',
                        'published'       => 1,
                        'marketplace'     => 1,
                        'allow_enrolment' => 'enquiry',
                        'author'          => 'Learning Heroes',
                        'source_id'       => $this->setSourceId($value['source_url']),
                        'packageUrl'      => $packageUrl->link()->getUri(),
                        'image'           => $thumbUrl->link()->getUri(),
                        'description'     => $descriptionUrl->link()->getUri(),
                    ]
                );
            }
        });
    }

    private function downloadFiles()
    {
        foreach ($this->courseData as $key => $value) {
            if (!empty($value['source_id'])) {
                $this->courseData[$key]['packageUrl'] = $this->getS3File($value['packageUrl'], $value['source_id']);
                $this->courseData[$key]['image'] = $this->getS3File($value['image'], $value['source_id']);
                $this->courseData[$key]['description'] = html_entity_decode($this->getDescription($value['description']));
                foreach ($value["resource"] as $k => $v) {
                    $this->courseData[$key]['resource'][$k] = $this->getS3File($v, $value['source_id']);
                }
            }
        }
    }

    /**
     * @param $url
     * @return string
     */
    private function setSourceId($url)
    {
        $this->_id = md5(trim($url));

        return $this->_id;
    }

    /**
     * @param $pdfFile
     * @return string
     */
    public function getDescription($pdfFile)
    {
        try {
            if ($this->client->makeDir($this->tempDir)) {
                $this->client->deleteAllFileInDir($this->tempDir);
            }
            $pdfFile = $this->client->downloadFile($pdfFile, $this->tempDir);
            if (@file_exists($pdfFile)) {
                return $this->client->readPdf($pdfFile);
            }

            return '';
        }
        catch (RequestException $e) {
            $this->logger($this->getName())->addError('Course url is error: ' . $e->getRequest()->getUri());

            return '';
        }
    }

    /**
     * @param $file
     * @param $source_id
     * @return bool|string
     */
    public function getS3File($file, $source_id = '')
    {
        try {
            if ($source_id == '') {
                $filePath = $file;
            }
            else {
                //$filePath = '/var/www/html/scraping/src/App/../../files/temp/_temp_1484723352.zip';
                $filePath = $this->downloadFileForLi($file, $source_id);
            }

            // uploading
            $this->setMessage('Uploading file to S3 ... ' . basename($filePath));

            // new object upload to S3
            $upload = new UploadHandle(array_merge($this->uploadParams, [
                    "timestamp" => time(),
                    'filename'  => basename($filePath),
                    'filePath'  => $filePath,
                ])
            );
            $result = $upload->getLink();
            if ($result) {
                $this->setMessage('Uploaded Done ' . $result);

                return $result;
            }
        }
        catch (RequestException $e) {
            $this->logger($this->getName())->addError('Course url is error: ' . $e->getRequest()->getUri());

            return '';
        }
    }

    /**
     * @param $file
     * @return bool
     */
    public function downloadFileForLi($file, $source_id = null)
    {
        $thisDir = !empty($source_id) ? $this->packageDir . $source_id . "/" : $this->packageDir . $this->_id . "/";
        $this->client->makeDir($thisDir);

        return $this->client->downloadFile($file, $thisDir);
    }

    /**
     * scraping course
     *
     * @param $parentElement
     * @return mixed
     */
    public function scrapingCourse($parentElement)
    {
        return $this->client->cursor->filter($parentElement . ' tr')->each(function ($node) use ($parentElement, $isScrom) {
            $th = $node->parents()->filter($parentElement)->children()->filter('th');
            $td = $node->filter("td");
            $url = $node->children()->filter('a')->last();

            // Check isset dom
            if ($td->count() > 0 && $th->count() > 0 && $url != self::BASE_URL) {
                // back select cursor to get parent tags
                return [
                    'tags'       => [$th->text()],
                    'title'      => html_entity_decode(trim($td->first()->text())),
                    'source_url' => $url->link()->getUri(),
                ];
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($row)
    {
        return array_merge($row,
            ['data' => [
                'source_id'       => $row['source_id'],
                'image'           => $row['image'],
                'published'       => 1,
                'marketplace'     => 1,
                'allow_enrolment' => 'enquiry',
                'tinCanPackage'   => $row['packageUrl'],
                'source_url'      => $row['source_url'],
                'author'          => $row['author'],
                'course_id'       => $row['source_id'],
                'course_url'      => $row['source_url'],
                'allow_enrolment' => 'enquiry',
            ]]);
    }

    public function getNote($item)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getStateData()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'learningHeroes';
    }

    public function getYearDefault()
    {
    }

    public function getDateTime($string)
    {
    }

    public function hasNoteId()
    {
        return false;
    }

}
