<?php

namespace App\Scraping\Course\Allconferences;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\Go1\Go1CloudinaryController;
use App\LoggerController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingFileUpload;
use App\Scraping\ScrapingInterface;
use App\LocaleMap;
use GuzzleHttp\Exception\RequestException;
use DateTime;
use Jasny\ISO\Countries;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DomCrawler\Crawler;


class AllconferencesScraping extends ScrapingBase implements ScrapingInterface
{
    /** @var  Application */
    const BASE_URL = 'http://www.allconferences.com/';
    const INDEX_SEARCH = 'http://www.allconferences.com/search/index/Category__parent_id:1/page:';

    private $parentParentParentCategories = [];

    /**
     * @var \App\Browser\Guzzle
     */
    private $client;
    private $configCtrl;
    private $start = 0;
    private $go1CloudinaryController;
    private $scrapingFileUpload;

    public function __construct(
        Guzzle $client,
        Go1CloudinaryController $go1CloudinaryController,
        ScrapingFileUpload $scrapingFileUpload,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        $this->client = $client;
        $this->configCtrl = $configCtrl;
        $this->go1CloudinaryController = $go1CloudinaryController;
        $this->scrapingFileUpload = $scrapingFileUpload;
        parent::__construct($configCtrl, $loggerCtrl);
    }

    public function setClient() {
        return new Guzzle([
            'base_uri' => self::BASE_URL,
            'timeout' => 60
        ]);
    }

    public function getCoursePerPage($page)
    {
        try {
            $content = $this->client->get($this::INDEX_SEARCH . $page)
                ->getBody()
                ->getContents();
        }
        catch (RequestException $e) {
            return false;
        }

        $crawler = new Crawler();
        $crawler->addContent($content);
        $courses = [];
        $crawler->filter('#categories_conferences .listing')->each(function (Crawler $item) use(&$courses) {
            $course['url'] = $item->filter('.conferenceHead a')->attr('href');
            $course['title'] = $item->filter('.conferenceHead a')->text();
            $course['source_id'] = $this->filterId($item->attr('id'));
            $courses[] = $course;
        });

        return $courses;
    }

    public function filterId($str) {
        preg_match("/(\d+)/", $str, $output);
        return ($output) ? $output[0] : '';
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null, $page = 0)
    {
        $courses = [];
        do {
            $this->setMessage('Getting page: ' . $page);
            if (!$result = $this->getCoursePerPage($page)) {
                break;
            }
            $courses = array_merge($courses, $result);
            $page++;
        } while (count($courses) <= $limit);
        return $this->getResultByLimit($courses, $limit);
    }

    public function getResultByLimit($courses, $limit)
    {
        $result = [];
        foreach ($courses as $index => $course) {
            if ($index == $limit) {
                break;
            }
            $result[] = $course;
        }

        return $result;
    }

    public function getCourseDetail($row)
    {
        $prop = null;
        try {
            $content = $this->client->get($row['url'])
                ->getBody()
                ->getContents();

            $crawler = new Crawler();
            $crawler->addContent($content);
            if ($crawler->filter('.conference-info h1')->count() > 0) {
                $prop['name'] = $crawler->filter('.conference-info h1')->text();
            }
            $prop['image_url'] = 'https://res.cloudinary.com/go1vn/image/upload/v1486348568/trbaw9dce46cl6zbcxwr.jpg';
            $prop['source_url'] = $row['url'];
            $prop['description'] = $this->getDescription($crawler);

            // get event of course
            if ($crawler->filter('.conference-schedule')->count() > 0) {
                $prop['event'] = $this->getEventOnCourse($crawler);
            }

            // get external url
            if ($crawler->filter('.conference-options .eventWebsite')->count() > 0) {
                $prop['external_url'] = $this->getConferenceWebsite($crawler->filter('.conference-options .eventWebsite'));
            }
            $prop['price'] = 0;
            $prop['tags'] = $this->getTagsOnCourse($crawler);
            $prop['tags'] = array_filter($prop['tags']);
        } catch (RequestException $e) {
            $this->logger($this->getName())->addError(
                'Course url is error: '.$e->getRequest()->getUri()
            );
        }

        return $prop;
    }

    public function getLocationOnCourse(Crawler $crawler) {
        $country = null;
        $address = null;
        $city = null;
        $location = [];

        $crawler->filter('.info_field')->each(function (Crawler $item) use(&$location) {
            $label = preg_replace('/\s+/', '', $item->filter('label')->text());
            switch ($label) {
                case 'Country:':
                    $location['country'] = Countries::getCode($item->filter('.venue_info a')->text());
                    break;
                case 'Address:':
                    $location['thoroughfare'] = $item->filter('.venue_info')->text();
                    break;
                case 'City:':
                    $location['locality'] = $item->filter('.venue_info a')->text();
                    break;
            }
        });

        return $location;
    }

    public function getEventOnCourse(Crawler $crawler) {
        $schedule = $crawler->filter('.conference-schedule');
        $begin_time = '';
        $end_time = '';
        $schedule->filter('td')->each(function (Crawler $item) use (&$begin_time, &$end_time) {
            $label = trim($item->filter('b')->text());
            switch ($label) {
                case 'Event Date/Time:':
                    $begin_time = trim(str_replace($label, '', $item->text()));
                    $begin_time = str_replace(array("\r", "\n"), '', $begin_time);
                    break;
                case 'End Date/Time:':
                    $end_time = trim(str_replace($label, '', $item->text()));
                    $end_time = str_replace(array("\r", "\n"), '', $end_time);
                    break;
            }
        });

        $events = $this->getDateTimeOnCourse($begin_time, $end_time);
        $events['locations'] = [$this->getLocationOnCourse($crawler->filter('#conferenceVenue .floatLeft'))];
        return $events;
    }

    public function getDateTimeOnCourse($begin_time, $end_time) {
        $start = $this->getDateTime($begin_time);
        $end = $this->getDateTime($end_time);
        return [
            'start' => $start,
            'end' => $end
        ];
    }

    public function getDateTime($timeString)
    {
        // default Dec 12, 2016 / 8:00 am - (EST)
        $timeString = str_replace('/', '', $timeString);
        $timeString = str_replace('-', '', $timeString);
        // case May 08, 2017 / 9:00 am - (GMT +8:00 hours)
        if (strpos($timeString, 'hours') !== false) {
            $timeString = str_replace('(GMT', '', $timeString);
            $timeString = rtrim($timeString, ')');
        }

        $dateTime = new \DateTime($timeString);
        $dateTime->setTimezone(new \DateTimeZone("UTC"));
        return $dateTime->format(DateTime::ATOM);
    }

    public function getConferenceWebsite(Crawler $crawler) {
        $onclick = $crawler->attr('onclick');
        $regex = "/window.open\(\'(.*)\'\)/";
        preg_match($regex, $onclick, $match);
        return $match[1];
    }

    public function getTagsOnCourse(Crawler $crawler) {
        $tags = [];
        $crawler->filter('#categoriesAssociated a')->text();
        $crawler->filter('#categoriesAssociated a')->each(function (Crawler $item) use(&$tags) {
            $tags []= $item->text();
        });
        return array_unique($tags);
    }

    public function getPortal($row)
    {
        $prop['type'] = 'portal';
        $prop['title'] = 'unknown';
        try {
            $content = $this->client->get($row['url'])
                ->getBody()
                ->getContents();
            $crawler = new Crawler();
            $crawler->addContent($content);
            if ($crawler->filter('.conference_logo')->count() > 0) {
                $prop['portal_logo'] = $crawler->filter('.conference_logo')->attr('src');
                if (!$this->scrapingFileUpload->getBySourceUrl($prop['portal_logo'])) {
                    $file = $this->go1CloudinaryController->uploadFile($this->getName(), $prop['portal_logo']);
                    $fileUpload = [
                        'source_url' => $prop['portal_logo'],
                        'remote_url' => $file->secure_url,
                        'provider' => $this->getName(),
                    ];
                    $this->scrapingFileUpload->insert($fileUpload);
                }
            }
            if ($crawler->filter('.orgConference span')->count() > 0) {
                $prop['title'] = $crawler->filter('.orgConference span')->text();
            }
            if ($prop['title'] == 'unknown' && isset($prop['portal_logo'])) {
                // get external url
                if ($crawler->filter('.conference-options .eventWebsite')->count() > 0) {
                    $prop['title'] = $this->getConferenceWebsite($crawler->filter('.conference-options .eventWebsite'));
                    $prop['title'] = $this->getConferenceWebsiteTitle($prop['title']);
                }
            }
            $prop['title'] = $this->formatPortalTitle($prop['title']);
            $prop['source_id'] = $row['source_id'];
            $prop['source_url'] = $row['url'];
        } catch (RequestException $e) {
            $this->logger($this->getName())->addError(
                'Course url is error: '.$e->getRequest()->getUri()
            );
            throw new Exception($e->getMessage());
        }

        // do not import portal if we do not known about it
        if ($prop['title'] == 'unknown') {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $this->setMessage('Scrapped course: ' . $prop['title']);
        return $prop;
    }

    public function getConferenceWebsiteTitle($title) {
        $domain = str_replace('www.', '', parse_url($title, PHP_URL_HOST));
        if (!$domain) {
            return $title;
        }
        return $domain;
    }

    // http://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
    public function formatPortalTitle($string) {
        $string = trim($string);
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    public function getDescription(Crawler $crawler)
    {
        $des = '';
        if ($crawler->filter('#conferenceDescription')->count() > 0) {
            $des .= '<h2 class="conferenceTitles">Description</h2>';
            $des .= '<div class="conferenceDescription">'.$crawler->filter('#conferenceDescription .description')->html().'</div>';
        }
        if ($crawler->filter('#conferenceSpeakers')->count() > 0) {
            $des .= '<h2 class="conferenceTitles">Conference Speakers</h2>';
            $des .= '<div class="conferenceSpeakers">'.$crawler->filter('#conferenceSpeakers .description')->html().'</div>';
        }

        if ($crawler->filter('#conferenceAddInfo')->count() > 0) {
            $des .= '<h2 class="conferenceTitles">Additional Information</h2>';
            $des .= '<div class="conferenceAddInfo">'.$crawler->filter('#conferenceAddInfo .description')->html().'</div>';
        }

        if ($crawler->filter('#conferenceExhibit')->count() > 0) {
            $des .= '<h2 class="conferenceTitles">Exhibits included</h2>';
            $des .= '<div class="conferenceExhibit">'.$crawler->filter('#conferenceExhibit .description')->html().'</div>';
        }
        if ($crawler->filter('.socialMediaSection')->count() > 0) {
            $des .= '<h2 class="conferenceTitles">Social Media</h2>';
            $des .= '<div class="socialMediaSection">'.$crawler->filter('.socialMediaSection .socialMediaPadding')->html().'</div>';
        }

        return $des;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($row)
    {
        $item = $this->getCourseDetail($row);
        // any conference with a start and end date in the past we can skip
        if ($this->isEventPast($item['event'])) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $record['title'] = trim($item['name']);
        $record['description'] = isset($item['description']) ? trim(
            $item['description']
        ) : '';
        $record['source_url'] = $item['source_url'];
        $record['language'] = isset($item['primaryLanguages'][0]) ? (new LocaleMap(
        ))->getLanguageCode(strtolower($item['primaryLanguages'][0])) : 'en';

        $record['source_id'] = $row['source_id'];
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';
        $record['pricing'] = isset($item['price']) ? [
            'price' => $item['price'],
            'currency' => 'USD',
        ] : [];
        $record['tags'] = $item['tags'];
        $record['event'] = $item['event'];

        $record['data']['image'] = isset($item['image']) ? $item['image'] : '';
        $record['data']['title'] = $record['title'];
        $record['data']['course_url'] = $record['source_url'];
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['allow_enrolment'] = 'enquiry';

        return $record;
    }

    public function isEventPast($event) {
        if (new DateTime($event['end']) < (new DateTime("now"))) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getStateData()
    {
        return [
            'start' => $this->start,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'allconferences';
    }
}
