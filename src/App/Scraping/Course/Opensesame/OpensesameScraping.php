<?php

namespace App\Scraping\Course\Opensesame;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\LoggerController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DomCrawler\Crawler;
use Exception;


class OpensesameScraping extends ScrapingBase implements ScrapingInterface
{
    const BASE_URL = 'https://www.opensesame.com';
    const MAXIMUM_ITEMS  = 300;

    //Global courses value to get all course by pages
    private $courses = [];
    private $page = 0;

    private $client;
    private $configCtrl;
    private $start = 0;


    /**
     * OpensesameScraping constructor.
     * @param Guzzle $client
     * @param ConfigController $configCtrl
     * @param LoggerController $loggerCtrl
     */
    public function __construct(Guzzle $client, ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->client = $client;
        $this->configCtrl = $configCtrl;
    }

    public function setClient() {
        return new Guzzle([
            'base_uri' => self::BASE_URL,
            'timeout' => 60
        ]);
    }

    /**
     * @param $page
     * @return mixed|string
     * @throws \Exception
     */
    public function getApiListCourse($page){
        $url = 'online-training-courses/search-content';
        $options['query'] = [
            'page' => $page,
            'solrsort' => 'ds_created asc',
            'language' => 'all',
            'f' => ['im_field_product_type:1065'],
        ];

        $this->client = $this->setClient();

        try {
            $res = $this->client->get($url,$options);
        }
        catch(RequestException $e) {
            throw new Exception('Can not get courses on opensesame');
        }

        if ($res->getStatusCode() == 200) {
            $courses = $res->getBody()->getContents();
            $courses = json_decode($courses,true);
            return $courses['searchResults'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        do {
            $this->setMessage('Getting page: ' . $this->page);
            $courses = $this->getApiListCourse($this->page);
            $results = [];
            foreach($courses as $course) {
                $results[] = $this->getCourse($course);
            }
            $this->courses = array_merge($this->courses, $results);
            $this->page++;

            // limit is available
            $condition1 = ($limit != null) && count($this->courses) < $limit;
            // limit is unavailable
            $condition2 = $limit == null && !empty($results);
        } while($condition1 || $condition2);
        return $this->courses;
    }

    /**
     * Get mapping category tag
     * @param $tagName
     * @return array
     */
    public function getMappingTag($tagName)
    {
        static $mappingTags;
        if (!$mappingTags) {
            $mappingTags = [
                'business skills' => $this->getCategories(
                    ['General Business Skills']
                ),
                'industry specific' => $this->getCategories(
                    ['Industry Specific']
                ),
                'technology' => $this->getCategories(['Technology']),
                'safety' => $this->getCategories(['Safety and Health']),
                'compliance' => $this->getCategories(['Compliance']),
                'certifications' => $this->getCategories(['Certifications']),
            ];
        }

        return isset(
            $mappingTags[strtolower(
                $tagName
            )]
        ) ? $mappingTags[strtolower($tagName)] : [];
    }

    /**
     * Map data
     *
     * @param $data
     * @return array
     */
    public function getCourse($item){
        $imageUrl = parse_url($item['imageUrl']);
        $imageUrl = ($imageUrl['path']) ?: $item['imageUrl'];
        $imageUrl = strtr($imageUrl,['/course_card/' => '/course_details_main_image/',]);
        return [
            'title' => $item['title'],
            'description' => $item['description'],
            'author'=> $item['seller']['name'],
            'seatTime' => $item['seatTime'],
            'price' => $item['priceRange']['regPrice'],
            'category' => $item['topLevelCategory']['title'],
            'source_url' => self::BASE_URL.'/'.trim($item['viewUrl'],'/'),
            'source_id' => $item['courseReferenceNid'],
            'image' => self::BASE_URL.$imageUrl
        ];
    }

    /**
     * @param $row
     * @return string
     */
    public function getCourseApiDetail($row){
        try {
            $content =  $this->client->get($row['source_url'])
                ->getBody()
                ->getContents();

            $crawl = new Crawler();
            $crawl->addContent($content);

            $crawl->filter('head script')->each(
                function ($node) use (&$data) {
                    $body = $node->html();
                    if (strpos($body, 'jQuery.extend(Drupal.settings') !== false) {
                        $body = mb_substr($body, stripos($body, '{'));
                        $body = mb_substr($body, 0, strripos($body, '}') + 1);
                        $data = json_decode($body, true);

                        if (json_last_error(
                            ) !== JSON_ERROR_NONE || !isset($data['os_handlebars'])
                        ) {
                            return false;
                        }
                        $data = $data['os_handlebars'];
                    }
                }
            );

            foreach ($data as $item) {
                if ($item['template'] === 'marketplace--course--detail') {
                    return $item['data'];
                }
            }
        } catch (RequestException $ex) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }
    }

    /**
     * @param $row
     * @param $detail
     * @return string
     */
    public function getDescription($row,$detail){
        $description = $row['description'];
        if (!empty($row['seatTime'])) {
            $description .= sprintf(
                '<p>Estimated length: %s</p>',
                $row['seatTime']
            );
        }
        if (!empty($row['objectives'])) {
            $objectives = [];
            foreach ($detail['objectives'] as $objective) {
                $objectives[] = "<li>$objective</li>";
            }
            $description .= sprintf(
                '<strong>Course Objectives</strong><ul>%s</ul>',
                implode(' ', $objectives)
            );
        }
        return $description;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($row)
    {
        $this->setMessage('Fetching course detail for '.$row['title']);
        $detail = $this->getCourseApiDetail($row);

        $data =  [
            'title'=>trim($row['title']),
            'source_url'=>$row['source_url'],
            'source_id'=>$row['source_id'],
            'marketplace' => 1,
            'published'=>1,
            'type' => 'course',
            'language'=>$this->getLanguage($detail),
            'description' => str_replace(
                "\n",
                '',
                $this->formatHtml($this->getDescription($row,$detail))
            ),
            'tags' => $this->getMappingTag($row['category']),
            'pricing'=>[
                'price' =>$row['price'],
                'currency' => 'USD',
            ],
            'author'=>['full_name' => $row['author'],
                'avatar' => $row['image'],
                'is_organization_name' => true,],
            'data'=>[
                'pricing'=>[
                    'price' =>$row['price'],
                    'currency' => 'USD',
                ],
                'description' => str_replace(
                    "\n",
                    '',
                    $this->formatHtml($this->getDescription($row,$detail))
                ),
                'image_url'=>$row['image'],
                'course_url'=>$row['source_url'],
                'course_id'=>$row['source_id'],
                'allow_enrolment'=>'enquiry',
            ],
        ];
        return $data;
    }


    /**
     * @param $detail
     * @return string
     */
    private function getLanguage($detail)
    {
        static $maps;
        $languages = $this->languageMaps();
        foreach ($detail['sidebar']['languages']['languageList'] as $language) {
            $language = strtolower(trim($language['name']));
            if (isset($maps[$language])) {
                return $maps[$language];
            }

            if (isset($languages[$language])) {
                $maps[$language] = $languages[$language];
                break;
            } else {
                // For the case language label with format "chinese (simplified)"
                foreach (array_keys($languages) as $languageLabel) {
                    if (strpos($language, $languageLabel) !== false) {
                        $maps[$language] = $languages[$languageLabel];
                        break;
                    }
                }
            }
        }

        return isset($maps[$language]) ? $maps[$language] : 'en';
    }

    private function languageMaps()
    {
        return [
            'english' => 'en',
            'spanish' => 'es',
            'japanese' => 'ja',
            'portuguese' => 'pt',
            'chinese' => 'zh',
            'german' => 'de',
            'italian' => 'it',
            'thai' => 'th',
            'french' => 'fr',
            'hungarian' => 'hu',
            'czech' => 'cs',
            'russian' => 'ru',
            'arabic' => 'am',
            'dutch' => 'nl',
            'korean' => 'ko',
            'indonesian' => 'id',
        ];
    }

    /**
     * @param $item
     * @return array
     */
    public function getNote($item)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getStateData()
    {
        return [
            'start' => $this->start
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'opensesame';
    }

    public function hasNoteId() {
        return false;
    }
}
