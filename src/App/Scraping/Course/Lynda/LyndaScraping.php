<?php

namespace App\Scraping\Course\Lynda;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use App\Scraping\ScrapingBase;
use App\LocaleMap;
use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

class LyndaScraping extends ScrapingBase implements ScrapingInterface
{
    const BASE_URL = 'https://www.lynda.com/';
    const SORT_BY_RELEASE_DATE_ASC = 1;
    const SORT_BY_RELEASE_DATE_DESC = 100;
    const SORT_BY_TITLE_ASC = 2;
    const SORT_BY_TITLE_DESC = 101;

    /** @var  mixed */
    protected $page;

    /**
     * @var array Authors avatar url lookup array
     */
    private $authors;

    /**
     * @var \App\Browser\Guzzle
     */
    private $client;
    private $scrapingData;
    private $configCtrl;
    private $logger;

    public function __construct(
        Guzzle $client,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerInterface $logger
    ) {
        $this->client = $client::getInstance(
            'lynda',
            [
                'base_uri' => self::BASE_URL,
                'timeout' => 60,
            ]
        );
        $this->scrapingData = $scrapingData;
        $this->configCtrl = $configCtrl;
        $this->logger = $logger;
        $this->authors = [];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lynda';
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        // Get scraping state
        if (!$this->page) {
            $state = $this->scrapingData->getScrapingState('lynda');
            $this->page = isset($state->data['page']) && $state->data['page'] > 1 ? $state->data['page'] : 1;
        }

        // Get course list
        try {
            $this->setMessage('Getting the courses list...');
            $courseListIterator = $this->getCourseListIterator(
                $this->page,
                self::SORT_BY_RELEASE_DATE_DESC
            );
            // Get courses content
            $courses = $this->getCoursesContent($courseListIterator);
        } catch (RequestException $e) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $this->page++;

        return $courses;
    }

    /**
     * Get course list's iterator
     * @param int $page
     * @param int $sortBy
     * @return \ArrayIterator|null
     * @throws Exception
     */
    public function getCourseListIterator(
        $page = 1,
        $sortBy = self::SORT_BY_RELEASE_DATE_DESC
    ) {
        $self = $this;
        $response = $this->client->get(
            'allcourses',
            [
                'query' => [
                    'page' => $page,
                    'ajax' => 1,
                    'sort' => $sortBy,
                ],
                'on_stats' => function (TransferStats $stats) use ($self) {
                    $self->setMessage(
                        $stats->getEffectiveUri()->__toString(),
                        'currentUrl'
                    );
                },
            ]
        );

        $responseData = (array)json_decode(
            $response->getBody()->getContents(),
            true
        );
        if ($responseData === false) {
            throw new Exception('The json data is not valid');
        }
        $responseData = reset($responseData);

        $coursesCrawler = new Crawler($responseData, self::BASE_URL);
        $coursesItr = $this->getNodeIterator(
            $coursesCrawler->filter('ul.course-list > li')
        );

        return $coursesItr;
    }

    /**
     * Get courses' DOM content
     * @param \ArrayIterator $courseListIterator
     * @return array
     */
    public function getCoursesContent(\ArrayIterator $courseListIterator)
    {
        $courses = [];

        // Get all courses content
        foreach ($courseListIterator as $courseDom) {
            $courseCrawler = new Crawler($courseDom);
            $courseLink = $this->getNodeAttr(
                $courseCrawler->filter('article a'),
                'href'
            );
            $name = $this->getNodeText(
                $courseCrawler->filter('article a'),
                false
            ) ?: $courseLink;
            $this->setMessage('Fetching course: '.trim($name));

            // Get courses
            $coursesResponse = $this->client->get($courseLink);
            $content = $coursesResponse->getBody()->getContents();
            if (strpos($content, 'rel="canonical"') !== false) {
                $courses[] = $content;
            } else {
                $this->logger->logger('lynda')->addWarning(
                    'Course has been ignored because the data structure is not supported',
                    ['url' => self::BASE_URL.trim($courseLink, '/')]
                );
            }
        }

        return $courses;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($courseResponse)
    {
        $course = [];
        $courseCrawler = new Crawler($courseResponse);

        // Course url
        $courseUrl = $this->getNodeAttr(
            $courseCrawler->filter('link[rel=canonical]'),
            'href'
        );
        // Course Id
        $courseId = explode('-', basename($courseUrl, '.html'));
        $courseId = reset($courseId);
        $course['source_id'] = (int)$courseId;
        $course['source_url'] = $courseUrl;
        $course['data']['course_id'] = $course['source_id'];
        $course['data']['course_url'] = $course['source_url'];

        // Course language
        $courseLanguage = $this->getNodeAttr(
            $courseCrawler->filter('meta[property="og:locale"]'),
            'content'
        );
        $course['language'] = (new LocaleMap())->getLanguageCode(
            strtolower(str_replace('_', '-', $courseLanguage))
        );
        $course['title'] = ($courseCrawler->filter('h1.default-title')->count(
            ) > 0) ? $courseCrawler->filter('h1')->text() : '';
        $authorLinkCrawler = $courseCrawler->filter('.author-thumb a');
        $authorId = explode(
            '-',
            basename($authorLinkCrawler->attr('href'), '.html')
        );
        $authorId = reset($authorId);
        $course['author'] = [
            'full_name' => ltrim($authorLinkCrawler->text()),
            'url' => substr(self::BASE_URL, 0, -1).$authorLinkCrawler->attr(
                    'href'
                ),
        ];
        // Check author avatar
        $authorAvarUrl = $this->getAuthorAvatarUrl($authorId);
        if ($authorAvarUrl) {
            $course['author']['avatar'] = $authorAvarUrl;
        }

        // Course image
        $course['data']['image_url'] = $this->getNodeAttr(
            $courseCrawler->filter('meta[property="og:image:secure_url"]'),
            'content'
        );
        $videoId = $this->getNodeAttr(
            $courseCrawler->filter('#banner-content-videotitle > a'),
            'data-start-video'
        );
        // Preview video link
        $course['data']['video_url'] = empty($videoId) ? $videoId : "https://www.lynda.com/player/embed/{$videoId}";

        // Course description
        $courseDescription = '';
        $courseDescriptionCrawler = $this->getNodeAtPos(
            $courseCrawler->filter('.course-description'),
            0
        );
        $courseDescriptionNodes = $courseDescriptionCrawler ? $courseDescriptionCrawler->childNodes : [];
        foreach ($courseDescriptionNodes as $childNode) {
            if (empty($childNode->nodeName) || ('div' === $childNode->nodeName && 'truncate-fade' === $childNode->getAttribute(
                        'class'
                    ))
            ) {
                continue;
            }
            $courseDescription .= $childNode->ownerDocument->saveHtml(
                $childNode
            );
        }
        $course['description'] = $this->formatHtml($courseDescription);

        // Course length
        $course['data']['course_length'] = $this->getNodeAttr(
            $courseCrawler->filter('meta[property="video:duration"]'),
            'content'
        );

        // Course subject
        $tagName = $this->getNodeAttr(
            $courseCrawler->filter('meta[property="video:tag"]'),
            'content'
        );
        if (!empty($tagName)) {
            $course['tags'] = $this->getMappingTag($tagName);
        }

        $course['data']['allow_enrolment'] = 'subscription';
        $course['marketplace'] = 1;
        $course['published'] = 1;
        $course['type'] = 'course';

        $this->validateCourseDetail($course);

        return $course;
    }

    /**
     * Validate course detail
     * @param array $course
     * @throws Exception
     */
    private function validateCourseDetail($course)
    {
        // Get empty course detail properties
        $emptyProps = [];
        array_walk_recursive(
            $course,
            function ($prop, $key) use (&$emptyProps) {
                if ($prop === '' || $prop === null) {
                    $emptyProps[] = $key;
                }
            }
        );

        if (!empty($emptyProps)) {
            // Validate mandatory course detail properties
            $mandatoryProps = ['source_id', 'source_url', 'title'];
            $intersectCheck = array_intersect($mandatoryProps, $emptyProps);
            if (count($intersectCheck) > 0) {
                throw new Exception(
                    'Course detail missing mandatory properties "'.implode(
                        '", "',
                        $intersectCheck
                    ).'"'
                );
            }

            // Log empty course detail properties
            $this->logger->logger('lynda')->addWarning(
                'Course detail missing properties "'.implode(
                    '", "',
                    $emptyProps
                ).'"',
                [$course]
            );
        }
    }

    public function getStateData()
    {
        return [
            'page' => $this->page ?: 1,
        ];
    }

    /**
     * Get mapping category tag for Lynda
     * @param $tagName
     * @return array
     */
    public function getMappingTag($tagName)
    {
        $mappingTags = [
            "design" => $this->getCategories(["Business", "Design"]),
            "video" => $this->getCategories(
                ["Technology", "Development", "Video"]
            ),
            "it" => $this->getCategories(
                ["Digital Literary & IT Skills", "IT"]
            ),
            "photography" => $this->getCategories(
                ["Personal Development", "Photography"]
            ),
            "web" => $this->getCategories(["Technology", "Development", "Web"]),
            "3d + animation" => $this->getCategories(
                ["Technology", "Design", "3D + Animation"]
            ),
            "business" => $this->getCategories(
                ["General Business Skills", "Business"]
            ),
            "developer" => $this->getCategories(
                ["Technology", "Development", "Developer"]
            ),
            "cad" => $this->getCategories(
                ["Business", "Industry Specific", "CAD"]
            ),
            "audio + music" => $this->getCategories(
                ["Digital Literary & IT Skills", "Audio + Music"]
            ),
            "education + elearning" => $this->getCategories(
                [
                    "Academics",
                    "Technology",
                    "Digital Literary & IT Skills",
                    "Education + Elearning",
                ]
            ),
            "marketing" => $this->getCategories(
                ["Sales & Marketing", "Marketing"]
            ),
            "video2brain" => $this->getCategories(
                ["Business", "Industry Specific", "video2brain"]
            ),
        ];

        if (isset($mappingTags[strtolower($tagName)])) {
            return $mappingTags[strtolower($tagName)];
        }

        return [$tagName];
    }

    /**
     * @param int $authorId
     * @return string|false
     */
    public function getAuthorAvatarUrl($authorId)
    {
        if (isset($this->authors[$authorId])) {
            // Get author avatar url from lookup array
            return $this->authors[$authorId];
        }

        $authorAvarUrl = "https://cdn.lynda.com/authors/{$authorId}_200x200_thumb.jpg";
        try {
            if ($this->client->head($authorAvarUrl)->getStatusCode() != 200) {
                $authorAvarUrl = false;
            }
        } catch (RequestException $e) {
            $authorAvarUrl = false;
        }

        // Save to author url lookup array
        $this->authors[$authorId] = $authorAvarUrl;

        return $authorAvarUrl;
    }

    private function getNodeIterator(Crawler $node, $default = null)
    {
        if ($default === null) {
            $default = new \ArrayIterator();
        }

        return $node->count() > 0 ? $node->getIterator() : $default;
    }

    private function getNodeAtPos(Crawler $node, $pos = 0, $default = null)
    {
        return $node->count() > 0 ? $node->getNode($pos) : $default;
    }

    private function getNodeChildren(Crawler $node, $default = null)
    {
        if ($default === null) {
            $default = new Crawler();
        }

        return $node->count() > 0 ? $node->children() : $default;
    }

    private function getNodeAttr(Crawler $node, $attr = '', $default = '')
    {
        return $node->count() > 0 ? $node->attr($attr) : $default;
    }

    private function getNodeText(Crawler $node, $default = '')
    {
        return $node->count() > 0 ? $node->text() : $default;
    }

}
