<?php

namespace App\Scraping\Course\E3Learning;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use GuzzleHttp\TransferStats;
use Symfony\Component\DomCrawler\Crawler;

class E3LearningScraping extends ScrapingBase implements ScrapingInterface
{
    private $client;
    private $configCtrl;

    public function __construct(Guzzle $client, ConfigController $configCtrl)
    {
        $this->client = $client::getInstance(
            'e3learning',
            [
                'base_uri' => 'https://www.e3learning.com.au',
                'on_stats' => function (TransferStats $stats) {
                    $this->setMessage($stats->getEffectiveUri(), 'currentUrl');
                },
            ]
        );
        $this->configCtrl = $configCtrl;
    }

    /**
     * @inheritdoc
     */
    public function getList($limit = null)
    {
        static $completed;
        if ($completed) {
            return;
        }

        $content = $this->client->get('content/store/store.jsp')
            ->getBody()
            ->getContents();
        $crawler = new Crawler($content);
        $data = [];
        $crawler->filter('#subscriptions .download .moreinfo')->each(
            function (Crawler $node) use (&$data, $crawler) {
                $id = $node->attr('data-id');
                if ($id) {
                    $wrapper = $crawler->filter('#product'.$id);
                    $data[$id]['image'] = 'https://www.e3learning.com.au'.$wrapper->filter(
                            '.image img'
                        )->last()->attr('src');
                    $data[$id]['category'] = $wrapper->attr(
                        'category'
                    ) ?: false;
                    $data[$id]['id'] = $id;
                }
            }
        );
        $completed = true;

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getDataDetail($data)
    {
        $content = $this->client->get(
            'content/store/productinfo.jsp',
            [
                'query' => [
                    'productid' => $data['id'],
                ],
            ]
        )->getBody()->getContents();
        $crawler = new Crawler($content);

        $record = [];
        $record['title'] = trim($crawler->filter('h2.title')->text());
        $record['source_id'] = $data['id'];
        $record['source_url'] = 'https://www.e3learning.com.au/content/store/productinfo.jsp?productid='.$data['id'];
        $record['data']['course_url'] = $record['source_url'];
        // Extract price from string format $49.999 (AUD)
        if (preg_match(
            '/([0-9]+(\.[0-9]+)*)+\s\(([a-z]+)\)/si',
            trim($crawler->filter('.price .value')->text()),
            $matches
        )) {
            $record['pricing'] = [
                'price' => $matches[1],
                'currency' => $matches[3],
            ];
        }

        $record['language'] = 'en';
        $record['data']['course_id'] = $data['id'];
        $record['data']['allow_enrolment'] = 'allow';
        $record['data']['image_url'] = $data['image'];
        if ($data['category']) {
            $record['tags'] = $this->getMappingTag($data['category']);
        }
        // Description
        $tmp = $crawler->filter('.moreinfo .intro');
        $description = '<p>'.$tmp->text().'</p>';
        $tmp = $tmp->nextAll();
        if ($tmp->count() > 0 && $tmp->nodeName() === 'p') {
            $description .= '<p>'.$tmp->text().'</p>';
        }
        $description .= $this->getExtraDescription(
            $crawler->filter('.moreinfo h3')
        );
        $record['description'] = $this->formatHtml($description);
        $record['author'] = [
            'full_name' => 'e3Learning',
            'avatar' => 'https://cdn.go1static.com/sites/accounts.gocatalyze.com/files/e3Learning_pos_rgb_smaller.png',
        ];
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';

        return $record;
    }

    /**
     * @inheritdoc
     */
    public function getStateData()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'e3learning';
    }

    private function getExtraDescription(Crawler $nodes)
    {
        $description = '';
        $nodes->each(
            function (Crawler $node) use (&$description) {
                $next = $node->nextAll();
                $title = trim($node->text());
                $i = 0;
                $description .= '<h4><strong>'.$title.'</strong></h4>';
                while (true) {
                    $n = $next->eq($i);
                    if ($n->count() < 1 || $n->nodeName() === 'h3' || strpos(
                            $n->attr('class'),
                            'price'
                        ) !== false
                    ) {
                        break;
                    }
                    $description .= strtr(
                        '<TAG>CONTENT</TAG>',
                        [
                            'CONTENT' => $n->html(),
                            'TAG' => $n->nodeName(),
                        ]
                    );
                    $i++;
                }
            }
        );

        return $description;
    }

    private function getMappingTag($tagName)
    {
        static $mappingTags;
        if (!$mappingTags) {
            $mappingTags = [
                'health and aged care' => $this->getCategories(
                    ['Compliance', 'Health & Aged Care']
                ),
                'workplace health and safety' => $this->getCategories(
                    ['Compliance', 'Workplace Health & Safety']
                ),
                'breastfeeding' => $this->getCategories(
                    ['Compliance', 'Breastfeeding']
                ),
                'mid safe' => $this->getCategories(['Compliance', 'Mid Safe']),
                'financial services' => $this->getCategories(
                    ['Compliance', 'Financial Services']
                ),
                'nurse safe' => $this->getCategories(
                    ['Compliance', 'Nurse Safe']
                ),
                'first aid courses' => $this->getCategories(
                    ['Compliance', 'First Aid Courses']
                ),
                'hazard guides' => $this->getCategories(
                    ['Compliance', 'Hazard Guides']
                ),
                'incontinence' => $this->getCategories(
                    ['Compliance', 'Incontinence']
                ),
                'workplace relations' => $this->getCategories(
                    ['Compliance', 'Workplace Relations']
                ),
                'nurse safe plus' => $this->getCategories(
                    ['Compliance', 'Nurse Safe PLUS']
                ),
                'competition and consumer law' => $this->getCategories(
                    ['Compliance', 'Competition and Consumer Law']
                ),
                'inductions' => $this->getCategories(
                    ['Compliance', 'Inductions']
                ),
                'flexible endoscopes - ultrasound probes' => $this->getCategories(
                    ['Compliance', 'Flexible Endoscopes - Ultrasound Probes']
                ),
                'emergency procedures' => $this->getCategories(
                    ['Compliance', 'Emergency Procedures']
                ),
                'disability training' => $this->getCategories(
                    ['Compliance', 'Disability Training']
                ),
                'privacy and fraud awareness' => $this->getCategories(
                    ['Compliance', 'Privacy and Fraud Awareness']
                ),
                'infection control' => $this->getCategories(
                    ['Compliance', 'Infection Control']
                ),
                'childsafe' => $this->getCategories(
                    ['Compliance', 'Childsafe']
                ),
                'manual handling in the care sector' => $this->getCategories(
                    ['Compliance', 'Manual Handling in the Care Sector']
                ),
                'medication management' => $this->getCategories(
                    ['Compliance', 'Medication Management']
                ),
                'sterilising services' => $this->getCategories(
                    ['Compliance', 'Sterilising Services']
                ),
                'occupational health and safety' => $this->getCategories(
                    ['Compliance', 'Occupational Health and Safety']
                ),
                'local government (sa)' => $this->getCategories(
                    ['Compliance', 'Local Government (SA)']
                ),
                'dementia' => $this->getCategories(['Compliance', 'Dementia']),
                'local government (vic)' => $this->getCategories(
                    ['Compliance', 'Local Government (VIC)']
                ),
            ];
        }

        return isset(
            $mappingTags[strtolower(
                $tagName
            )]
        ) ? $mappingTags[strtolower($tagName)] : [];
    }
}
