<?php

namespace App\Scraping\Course\Udacity;


use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use Exception;
use InvalidArgumentException;

class UdacityScraping extends ScrapingBase implements ScrapingInterface
{
    const BASE_URL = 'https://www.udacity.com';
    const COURSE_TYPE_COURSE = 'courses';
    const COURSE_TYPE_DEGREE = 'degrees';

    /** @var int */
    private $page;


    /**
     * @var \App\Browser\Guzzle
     */
    private $client;
    private $scrapingData;
    private $configCtrl;

    /**
     * UdacityScraping constructor.
     * @param \App\Browser\Guzzle $client
     * @param \App\Scraping\ScrapingData $scrapingData
     * @param \App\ConfigController $configCtrl
     */
    public function __construct(
        Guzzle $client,
        ScrapingData $scrapingData,
        ConfigController $configCtrl
    ) {
        $this->client = $client::getInstance(
            'udacity',
            [
                'base_uri' => self::BASE_URL,
                'timeout' => 60,
            ]
        );
        $this->scrapingData = $scrapingData;
        $this->configCtrl = $configCtrl;
    }

    /**
     * @inheritdoc
     */
    public function getList($limit = null)
    {
        // Get scraping state
        if (!$this->page) {
            $state = $this->scrapingData->getScrapingState('udacity');
            $this->page = isset($state->data['page']) && $state->data['page'] > 1 ? $state->data['page'] : 1;
        }

        // Udacity just has one page
        if ($this->page >= 3) {
            return [];
        }

        $this->setMessage('Getting the courses list...');

        switch ($this->page) {
            case 1:
                $courses = $this->getCourses(self::COURSE_TYPE_COURSE);
                break;
            case 2:
                $courses = $this->getCourses(self::COURSE_TYPE_DEGREE);
                break;
            default:
                $courses = $this->getCourses();
                break;
        }

        $this->page++;

        return $courses;
    }

    public function getCourses($type = self::COURSE_TYPE_COURSE)
    {
        // validate course type
        if (!in_array(
            $type,
            [self::COURSE_TYPE_COURSE, self::COURSE_TYPE_DEGREE]
        )
        ) {
            throw new InvalidArgumentException('Course type is not valid.');
        }

        $self = $this;
        try {
            $response = $this->client->get(
                "public-api/v1/{$type}",
                [
                    'on_stats' => function (TransferStats $stats) use (&$self) {
                        $self->setMessage(
                            $stats->getEffectiveUri(),
                            'currentUrl'
                        );
                    },
                ]
            );
        } catch (RequestException $e) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $json = json_decode($response->getBody()->getContents(), true);
        if (!$json) {
            throw new Exception('The json data is not valid.');
        }

        return $json[$type];
    }

    /**
     * @inheritdoc
     */
    public function getDataDetail($course)
    {
        if (!$course['available'] || !$course['public_listing']) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $record = [];

        // Course ID & URL
        $record['source_id'] = $course['key'];
        $record['source_url'] = self::BASE_URL.'/course/'.$course['slug'];
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['course_url'] = $record['source_url'];

        // Name
        $record['title'] = $course['title'];

        // Description
        $record['description'] = $this->renderDescription($course);

        // Short description
        $record['data']['short_description'] = $this->formatHtml(
            $course['short_summary']
        );

        // Source Image URI
        $record['data']['image_url'] = $course['image'];

        // Enrolment Type
        $record['data']['allow_enrolment'] = substr(
            $course['key'],
            0,
            2
        ) == 'nd' ? 'subscription' : 'enquiry';

        // Author
        $author = isset($course['affiliates'][0]) ? $course['affiliates'][0] : [
            'name' => 'Udacity',
            'image' => 'http://res.cloudinary.com/go1/image/upload/v1468233871/logo_mark_9fe087a553be4f083e55902df9f650bcc2fad097_gac93i.jpg',
        ];
        $record['author'] = [
            'id' => 'UDA-'.strtolower(str_replace(' ', '-', $author['name'])),
            'full_name' => $author['name'],
            'avatar' => isset($author['image']) ? $author['image'] : '',
        ];

        // Tags
        $record['tags'] = $this->getMappingTag($course['tracks']);

        // Video URL
        if (!empty($course['teaser_video']['youtube_url'])) {
            $record['data']['video_url'] = $course['teaser_video']['youtube_url'];
        }
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';
        $record['language'] = 'en';

        return $record;
    }

    /**
     * @inheritdoc
     */
    public function getStateData()
    {
        return [
            'page' => $this->page ?: 1,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'udacity';
    }

    /**
     * Get mapping category tag for Udacity
     * @param string|array $tagNames
     * @return array
     */
    public function getMappingTag($tagNames)
    {
        $tagNames = (array)$tagNames;
        $returnTags = [];
        static $mappingTags = null;
        if (null === $mappingTags) {
            $mappingTags = [
                'android' => $this->getCategories(['Development', 'Android']),
                'data science' => $this->getCategories(
                    ['Technology', 'Academic', 'Data Science']
                ),
                'georgia tech masters in cs' => $this->getCategories(
                    ['Georgia Tech Masters in CS']
                ),
                'ios' => $this->getCategories(['Development', 'iOS']),
                'non-tech' => $this->getCategories(['Academic', 'Non-Tech']),
                'software engineering' => $this->getCategories(
                    [
                        'Academic',
                        'Technology',
                        'Business',
                        'Software Engineering',
                    ]
                ),
                'web development' => $this->getCategories(
                    ['Web Development', 'Development']
                ),
            ];
        }

        foreach ($tagNames as $tagName) {
            $cmpTagName = strtolower($tagName);
            // Find mapping tags, omit 'All' tag
            if (isset($mappingTags[$cmpTagName])) {
                $returnTags = array_merge(
                    $returnTags,
                    $mappingTags[$cmpTagName]
                );
            } elseif ($cmpTagName !== 'all') {
                $returnTags[] = $tagName;
            }
        }

        $returnTags = empty($returnTags) ? ['Technology'] : array_values(
            array_unique($returnTags)
        );

        return $returnTags;
    }

    /**
     * Render description HTML
     *
     * @param array $course
     * @return string
     */
    private function renderDescription(array $course)
    {
        $description = '';

        $replaceHeadingTag = function ($text) {
            $text = $this->formatHtml($text);
            $text = str_replace(['<h1>', '<h2>', '<h3>'], '<h4>', $text);
            $text = str_replace(['</h1>', '</h2>', '</h3>'], '</h4>', $text);

            return $text;
        };

        if (!empty($course['summary'])) {
            $description .= $course['summary'];
        }
        if (!empty($course['expected_learning'])) {
            $expected = "<h3>Why Take This Course?</h3>".$replaceHeadingTag(
                    $course['expected_learning']
                );
            $description .= $expected;
        }
        if (!empty($course['required_knowledge'])) {
            $required = "<h3>Prerequisites and Requirements</h3>".$replaceHeadingTag(
                    $course['required_knowledge']
                );
            $description .= $required;
        }
        if (!empty($course['projects']) && is_array($course['projects'])) {
            $projects = "<h3>What Will I Learn?</h3>";
            foreach ($course['projects'] as $project) {
                $projects .= "<h4>{$project['name']}</h4>".$replaceHeadingTag(
                        $project['description']
                    );
            }
            $description .= $projects;
        }
        if (!empty($course['syllabus'])) {
            $syllabus = "<h3>Syllabus</h3>".$replaceHeadingTag(
                    $course['syllabus']
                );
            $description .= $syllabus;
        }

        return $this->formatHtml($description);
    }
}