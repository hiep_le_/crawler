<?php

namespace App\Scraping\Course\Coursera;

use App\Application;
use App\Browser\Guzzle;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use App\LocaleMap;
use GuzzleHttp\Exception\RequestException;
use DateTimeZone;
use DateTime;
use GuzzleHttp\TransferStats;

class CourseraScraping extends ScrapingBase implements ScrapingInterface
{
    private $clientApi;
    private $client;
    private $scrapingData;
    private $limit = 50;
    private $start = 0;

    const COURSERA_TIMEZONE = 'America/Los_Angeles';

    public function __construct(
        Guzzle $client,
        ScrapingData $scrapingData,
        ConfigController $configCtrl
    ) {
        $self = $this;

        $this->clientApi = $client::getInstance(
            'coursera_api',
            [
                'base_uri' => 'https://api.coursera.org',
                'on_stats' => function (TransferStats $stats) use (&$self) {
                    $self->setMessage($stats->getEffectiveUri(), 'currentUrl');
                },
            ]
        );

        $this->client = $client::getInstance(
            'coursera',
            [
                'base_uri' => 'https://www.coursera.org',
                'on_stats' => function (TransferStats $stats) use (&$self) {
                    $self->setMessage($stats->getEffectiveUri(), 'currentUrl');
                },
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        // Init scrape
        if (!$this->start) {
            $state = $this->scrapingData->getScrapingState('coursera');
            $this->start = isset($state->data['start']) && $state->data['start'] > 0 ? $state->data['start'] : 0;
        }

        try {
            $this->setMessage('Getting list of courses');
            $response = $this->clientApi->get(
                'api/courses.v1',
                [
                    'query' => [
                        'start' => $this->start,
                        'limit' => $this->limit,
                        'fields' => implode(
                            ',',
                            [
                                'domainTypes',
                                'partnerIds',
                                'startDate',
                                'workload',
                                'categories',
                                'photoUrl',
                                'primaryLanguages',
                                'partnerLogo',
                                'description',
                                'partners.v1(squareLogo,rectangularLogo)',
                            ]
                        ),
                        'includes' => 'partnerIds',
                    ],
                ]
            );
        } catch (RequestException $e) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $data = json_decode($response->getBody()->getContents(), true);

        $this->start = $this->start + $this->limit;

        return $this->processData($data);
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($item)
    {
        $record['title'] = trim($item['name']);
        $record['description'] = trim($item['description']);
        if (!empty($item['workload'])) {
            $record['description'] .= "<p><strong>Commitment</strong>: {$item['workload']}</p>";
        }
        $record['source_url'] = "https://www.coursera.org/learn/{$item['slug']}";
        $record['language'] = isset($item['primaryLanguages'][0]) ? (new LocaleMap(
        ))->getLanguageCode(strtolower($item['primaryLanguages'][0])) : 'en';
        $record['source_id'] = $item['slug'];
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';
        $record['data']['image_url'] = $item['promoPhoto'];
        $record['data']['course_url'] = $record['source_url'];
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['allow_enrolment'] = 'subscription';
        $record['author'] = [
            'full_name' => $item['author']['name'],
            'avatar' => $item['author']['squareLogo'],
            'is_organization_name' => true,
        ];
        if (isset($item['event'])) {
            $record['event']['timezone'] = $item['event']['timezone'];
            $record['event']['start'] = $item['event']['start'];
            $record['event']['end'] = $item['event']['end'];
        }
        if (!empty($item['domainTypes'])) {
            $record['tags'] = [];
            foreach ($item['domainTypes'] as $domainType) {
                $record['tags'] = array_merge(
                    $record['tags'],
                    $this->getMappingTag($domainType['domainId'])
                );
            }
            $record['tags'] = array_unique(array_filter($record['tags']));
        }

        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function getStateData()
    {
        return [
            'start' => $this->start,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'coursera';
    }

    private function processData($data)
    {
        $partners = [];
        foreach ($data['linked']['partners.v1'] as $partner) {
            $partners[$partner['id']] = $partner;
        }
        $items = [];
        foreach ($data['elements'] as $item) {
            try {
                $this->setMessage('Getting course description');
                $response = $this->client->get(
                    'api/onDemandCourses.v1',
                    [
                        'query' => [
                            'q' => 'slug',
                            'slug' => $item['slug'],
                        ],
                    ]
                );
            } catch (RequestException $e) {
                $this->app->logger('coursera')->addInfo(
                    sprintf(
                        'Course %s has been ignored because coursera not published it',
                        $item['name']
                    ),
                    ['slug' => $item['slug']]
                );
                continue;
            }
            $items[$item['slug']] = $item;
            $content = reset(
                json_decode(
                    $response->getBody()->getContents(),
                    true
                )['elements']
            );
            $items[$item['slug']]['description'] = $content['description'];
            $items[$item['slug']]['course_id'] = $content['id'];
            $items[$item['slug']]['promoPhoto'] = $content['promoPhoto'];
            if (isset($partners[reset($item['partnerIds'])])) {
                $items[$item['slug']]['author'] = $partners[reset(
                    $item['partnerIds']
                )];
            }

            try {
                $this->setMessage('Getting event information');
                $event = $this->client->get(
                    'api/onDemandSessions.v1',
                    [
                        'query' => [
                            'q' => 'currentOpenByCourse',
                            'courseId' => $items[$item['slug']]['course_id'],
                        ],
                    ]
                );
                $event = json_decode($event->getBody()->getContents(), true);
                if (!empty($event['elements'][0]['startedAt'])) {
                    $timezone = new DateTimeZone(self::COURSERA_TIMEZONE);
                    $start = new DateTime(
                        date('c', $event['elements'][0]['startedAt'] / 1000)
                    );
                    $start->setTimezone($timezone);
                    if (!empty($event['elements'][0]['endedAt'])) {
                        $end = new DateTime(
                            date('c', $event['elements'][0]['endedAt'] / 1000)
                        );
                        $end->setTimezone($timezone);
                    } else {
                        $end = clone $start;
                        $end->setTime(23, 59);
                    }
                    $items[$item['slug']]['event'] = [
                        'start' => $start->format(self::LO_EVENT_DATE_FORMAT),
                        'end' => $end->format(self::LO_EVENT_DATE_FORMAT),
                        'timezone' => self::COURSERA_TIMEZONE,
                    ];
                }
            } catch (RequestException $e) {
            }
        }

        return $items ?: Application::SCRAPING_STATUS_CONTINUE;
    }

    /**
     * Get mapping category tag for Coursera
     * @param $tagName
     * @return array
     */
    private function getMappingTag($tagName)
    {
        static $mappingTags;
        if (!$mappingTags) {
            $mappingTags = [
                'arts-and-humanities' => $this->getCategories(
                    ['Academics', 'Arts & Humanities']
                ),
                'business' => $this->getCategories(['Business']),
                'computer-science' => $this->getCategories(
                    ['Academics', 'Computer Science']
                ),
                'data-science' => $this->getCategories(['Academics']),
                'language-learning' => $this->getCategories(
                    ['Personal Development', 'Language Learning']
                ),
                'life-sciences' => $this->getCategories(['Academics']),
                'math-and-logic' => $this->getCategories(
                    ['Academics', 'Maths & Science']
                ),
                'personal-development' => $this->getCategories(
                    ['Personal Development']
                ),
                'physical-science-and-engineering' => $this->getCategories(
                    ['Technology']
                ),
                'social-sciences' => $this->getCategories(
                    ['Academics', 'Social Science']
                ),
            ];
        }

        return isset(
            $mappingTags[strtolower(
                $tagName
            )]
        ) ? $mappingTags[strtolower($tagName)] : [];
    }
}
