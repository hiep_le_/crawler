<?php

namespace App\Scraping\Course\Open2Study;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use Doctrine\DBAL\Types\GuidType;
use Symfony\Component\DomCrawler\Crawler;

class Open2StudyScraping extends ScrapingBase implements ScrapingInterface
{
    private $client;
    private $configCtrl;

    const BASE_URL = 'https://www.open2study.com';

    public function __construct(Guzzle $client, ConfigController $configCtrl)
    {
        $this->client = $client::getInstance(
            'open2study',
            [
                'base_uri' => self::BASE_URL,
            ]
        );
        $this->configCtrl = $configCtrl;
    }

    /**
     * @inheritdoc
     */
    public function getList($limit = null)
    {
        // Open2study just have one page for list courses
        static $ran;
        if ($ran) {
            return null;
        }

        $courses = [];
        $content = $content = $this->client->get('courses')
            ->getBody()
            ->getContents();

        // 1. Get course information
        $crawler = new Crawler();
        $crawler->addContent($content);
        $crawler->filter('.view-subjects-block .view-content .views-row')->each(
            function ($node) use (&$courses) {
                $title = trim($node->filter('.adblock_course_title')->text());
                $url = trim($node->filter('a')->attr('href'), '/');
                $courses[$title] = $this->getCourse($url);
                $courses[$title]['title'] = $title;
                $courses[$title]['image_url'] = $node->filter(
                    '.image-style-course-logo-subjects-block'
                )->attr('src');
            }
        );

        // 2. Get list of categories
        $categories = $this->getRemoteCategories($content);

        // 3. Map category with course
        foreach ($categories as $id => $category) {
            $this->setMessage(
                sprintf('Map the course with category "%s".', $category)
            );
            $this->setMessage(self::BASE_URL.'/views/ajax', 'currentUrl');
            $content = json_decode(
                $this->client->post(
                    'views/ajax',
                    [
                        'form_params' => [
                            'field_subject_category_tid' => $id,
                            'view_name' => 'subjects_block',
                            'view_display_id' => 'subjects_page_block',
                        ],
                    ]
                )->getBody()->getContents()
            );
            if (!empty($content[1]->data)) {
                $crawler = new Crawler();
                $crawler->addContent($content[1]->data);
                $crawler->filter(
                    '.view-subjects-block .view-content .views-row'
                )->each(
                    function ($node) use (&$courses, $category) {
                        $title = utf8_decode(
                            trim($node->filter('.adblock_course_title')->text())
                        );
                        $courses[$title]['categories'][$category] = $category;
                    }
                );
            }
        }

        $ran = true;

        return $courses;
    }

    /**
     * @inheritdoc
     */
    public function getDataDetail($course)
    {
        $record['title'] = $course['title'];
        $record['language'] = 'en';
        $record['source_id'] = $course['source_id'];
        $record['source_url'] = $course['source_url'];
        $record['data']['image_url'] = $course['image_url'];
        $record['data']['video_url'] = $course['video_url'];
        $record['data']['course_url'] = $record['source_url'];
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['allow_enrolment'] = 'enquiry';
        $record['author'] = $course['author'];
        $record['author']['is_organization_name'] = true;
        $record['description'] = $this->renderDescription($course);
        if ($course['categories']) {
            $record['tags'] = [];
            foreach ($course['categories'] as $category) {
                $record['tags'] = array_merge(
                    $record['tags'],
                    $this->getMappingTag($category)
                );
            }
        }
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';

        return $record;
    }

    /**
     * @inheritdoc
     */
    public function getStateData()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'open2study';
    }

    /**
     * Get mapping category tag for Lynda
     * @param $tagName
     * @return array
     */
    private function getMappingTag($tagName)
    {
        static $mappingTags;
        if (!$mappingTags) {
            $mappingTags = [
                'education & training' => $this->getCategories(['Academics']),
                'science & technology' => $this->getCategories(['Technology']),
                'marketing & advertising' => $this->getCategories(
                    ['Sales & Marketing']
                ),
                'business' => $this->getCategories(['General Business Skills']),
                'finance' => $this->getCategories(['Sales & Marketing']),
                'health & medicine' => $this->getCategories(['Academics']),
                'management' => $this->getCategories(
                    ['General Business Skills']
                ),
                'arts & humanities' => $this->getCategories(
                    ['Arts & Humanities']
                ),
            ];
        }

        if (isset($mappingTags[strtolower($tagName)])) {
            return $mappingTags[strtolower($tagName)];
        }

        return [$tagName];
    }

    private function getRemoteCategories($content)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($content);
        $categories = [];
        $crawler->filter('#edit-field-subject-category-tid option')->each(
            function ($node) use (&$categories) {
                $categories[$node->attr('value')] = $node->text();
            }
        );
        unset($categories['All']);

        return $categories;
    }

    private function getCourse($courseUrl)
    {
        $url = self::BASE_URL.'/'.$courseUrl;
        $this->setMessage('Getting course content.');
        $this->setMessage($url, 'currentUrl');
        $content = $this->client->get($courseUrl)->getBody()->getContents();
        $crawler = new Crawler();
        $crawler->addContent($content);
        $course['author']['url'] = self::BASE_URL.$crawler->filter(
                '#provider-logo a'
            )->attr('href');
        $course['author']['avatar'] = $crawler->filter('#provider-logo img')
            ->attr('src');
        $course['author']['full_name'] = $this->getAuthorName(
            $course['author']['url']
        );
        $course['source_id'] = str_replace(
            '/enrol/',
            '',
            $crawler->filter('.enrol_now_button a')->attr('href')
        );
        $course['source_url'] = $url;
        $course['short_description'] = $crawler->filter('.offering_body h3')
            ->text();
        $course['video_url'] = $crawler->filter('iframe.media-youtube-player')
            ->attr('src');
        $course['what_it_about'] = $crawler->filter(
            '.readmore-container .full-body'
        )->text();
        $course['what_will_i_learn'] = $crawler->filter(
            '.whatyouwilllearncontainer'
        )->html();
        $course['wherecouldlead_careers'] = $crawler->filter(
            '.wherecouldlead_careers'
        )->html();
        $crawler->filter('.whatsinvolvedcontainer tr')->each(
            function ($node) use (&$course) {
                $course['whats-involved'][] = [
                    'title' => $node->filter('.title')->text(),
                    'description' => $node->filter('.desc')->text(),
                ];
            }
        );

        return $course;
    }

    private function getAuthorName($authorUrl)
    {
        $this->setMessage('Getting author information.');
        $content = $this->client->get($authorUrl)->getBody()->getContents();
        $crawler = new Crawler();
        $crawler->addContent($content);

        return trim(
            str_replace(
                'courses and instructors',
                '',
                $crawler->filter('#page-title')->text()
            )
        );
    }

    private function renderDescription($course)
    {
        $output = [];
        if (!empty($course['short_description'])) {
            $output['short_description'] = sprintf(
                '<div>%s</div>',
                $course['short_description']
            );
        }

        if (!empty($course['what_it_about'])) {
            $output['what_it_about'] = sprintf(
                '<h4><strong>What\'s it about?</strong></h4><div>%s</div>',
                $course['what_it_about']
            );
        }

        if (!empty($course['whats-involved'])) {
            foreach ($course['whats-involved'] as $item) {
                $output['whats-involved'][] = sprintf(
                    '<li>%s (%s)</li>',
                    $item['title'],
                    $item['description']
                );
            }
            $output['whats-involved'] = '<h4><strong>What\'s involved?</strong></h4><ul>'.implode(
                    $output['whats-involved']
                ).'</ul>';
        }

        if (!empty($course['what_will_i_learn'])) {
            $output['what_will_i_learn'] = str_replace(
                '<h2>What will I learn?</h2>',
                '<h4><strong>What will I learn?</strong></h4>',
                $course['what_will_i_learn']
            );
        }

        if (!empty($course['wherecouldlead_careers'])) {
            $output['wherecouldlead_careers'] = str_replace(
                '<h2>Where could this lead me?</h2>',
                '<h4><strong>Where could this lead me?</strong></h4>',
                $course['wherecouldlead_careers']
            );
        }

        return $this->formatHtml(implode('', $output));
    }
}
