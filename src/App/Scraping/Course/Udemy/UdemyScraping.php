<?php

namespace App\Scraping\Course\Udemy;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\LocaleMap;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;

class UdemyScraping extends ScrapingBase implements ScrapingInterface
{
    const MAXIMUM_ITEMS = 50;
    private $app;
    private $clientId;
    private $clientSecret;
    private $page;
    private $client;
    private $configCtrl;
    private $scrapingData;

    public function __construct(
        Guzzle $client,
        ScrapingData $scrapingData,
        ConfigController $configCtrl
    )
    {
        $this->client = $client::getInstance(
            'udemy',
            [
                'base_uri' => 'https://www.udemy.com/api-2.0/',
                'cookie'   => true,
                'timeout'  => 60,
            ]
        );
        $this->scrapingData = $scrapingData;
        $this->configCtrl = $configCtrl;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'udemy';
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        // Get scraping state
        if (!$this->page) {
            $state = $this->scrapingData->getScrapingState('udemy');
            $this->page = isset($state->data['page'])
            && $state->data['page'] > 0 ? $state->data['page'] : 1;
        }

        // Get the course list from udemy API
        $this->setMessage('Getting the courses list...');
        try {
            $this->getClientAccess();
            $courses = $this->getCourses($this->page);
        }
        catch (RequestException $e) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $this->page++;

        return $courses;
    }

    /**
     * Get courses from Udemy api
     *
     * @param int $page
     * @param int $pageSize
     * @return array
     * @throws Exception
     */
    public function getCourses($page = 1, $pageSize = self::MAXIMUM_ITEMS)
    {
        // Get the course list from udemy API
        $self = $this;
        $response = $this->client->get(
            'courses',
            [
                'query'    => [
                    'fields'    => [
                        'course' => '@all',
                    ],
                    'ordering'  => 'newest',
                    'page_size' => $pageSize,
                    'page'      => $page,
                ],
                'on_stats' => function (TransferStats $stats) use (&$self) {
                    $self->setMessage($stats->getEffectiveUri(), 'currentUrl');
                },
            ]
        );

        $json = json_decode($response->getBody()->getContents(), true);
        if (!$json) {
            throw new Exception('The json data is not valid.');
        }

        return $json['results'];
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($course)
    {
        $record = [];
        $record['title'] = $course['title'];
        $record['source_id'] = $course['id'];
        $record['source_url'] = 'https://www.udemy.com/' . trim(
                $course['url'],
                '/'
            );
        $record['data']['course_url'] = 'https://www.udemy.com/' . trim(
                $course['url'],
                '/'
            );
        if ($course['is_paid']) {
            $record['pricing'] = [
                'price'    => $course['price_detail']['amount'],
                'currency' => $course['price_detail']['currency'],
            ];
        }
        $record['language'] = (new LocaleMap())->getLanguageCode(
            strtolower(str_replace('_', '-', $course['locale']['locale']))
        );
        $record['data']['course_length'] = $course['estimated_content_length'];
        $record['data']['course_id'] = $course['id'];
        $record['data']['allow_enrolment'] = 'subscription';
        $record['data']['image_url'] = $this->getImageUrl($course);
        if (isset($course['primary_category']['title'])) {
            $record['tags'] = $this->getMappingTag(
                $course['primary_category']['title']
            );
        }
        $description = [];
        $description[] = '<div>' . $course['description'] . '</div>';
        $description[] = $this->courseDescriptionItemRender(
            'What am I going to get from this course?',
            $course['what_you_will_learn_data']
        );
        $description[] = $this->courseDescriptionItemRender(
            'What is the target audience?',
            $course['who_should_attend_data']
        );
        $description[] = $this->courseDescriptionItemRender(
            'What are the requirements?',
            $course['requirements_data']
        );
        $record['description'] = $this->formatHtml(implode("\n", $description));
        $record['author'] = [
            'full_name' => $course['visible_instructors'][0]['display_name'],
            'avatar'    => $course['visible_instructors'][0]['image_100x100'],
            'url'       => 'https://www.udemy.com/' . trim(
                    $course['visible_instructors'][0]['url'],
                    '/'
                ),
            'job_title' => $course['visible_instructors'][0]['job_title'],
        ];
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';

        return $record;
    }

    public function getNote($item, $page = 1, $pageSize = self::MAXIMUM_ITEMS)
    {
        $loId = $item['lo_id'];
        // Get the course list from udemy API
        $self = $this;
        $response = $this->client->get(
            "courses/{$loId}/reviews",
            [
                'query'    => [
                    'page_size' => $pageSize,
                    'page'      => $page,
                ],
                'on_stats' => function (TransferStats $stats) use (&$self) {
                    $self->setMessage($stats->getEffectiveUri(), 'currentUrl');
                },
            ]
        );

        $json = json_decode($response->getBody()->getContents(), true);
        if (!$json) {
            throw new Exception('The json data is not valid.');
        }

        $results = $json['results'];
        $reviews = [];
        foreach ($results as $result) {
            $review = [];
            $review['title'] = $result['title'];
            $review['author_name'] = $result['author_name'];
            $review['note'] = $result['note'];
            $review['date'] = $result['date'];
            $review['rating'] = $result['rating'];
            $reviews[] = $review;
        }

        return $reviews;
    }

    public function getImageUrl($course)
    {
        // Check the image
        try {
            // Try to use image 750x422
            $this->client->head($course['image_750x422']);
            $imageUrl = $course['image_750x422'];
        }
        catch (Exception $e) {
            // Some cases the image 750x422 can not usable, try to use image 480x270
            $imageUrl = $course['image_480x270'];
        }

        return $imageUrl;
    }

    public function getStateData()
    {
        return [
            'page' => $this->page ?: 1,
        ];
    }

    /**
     * Get the access token to request to the Udemy API
     */
    public function getClientAccess()
    {
        // 1. Go to the udemy
        // 2. Get client id and client secret from the responses
        if (!$this->clientId && !$this->clientSecret) {
            if ($this->client->get('https://www.udemy.com/courses')
                             ->getStatusCode() == 200
            ) {
                foreach ($this->client->getCookieJar()->toArray() as $cookie) {
                    if ($this->clientId && $this->clientSecret) {
                        break;
                    }
                    elseif ($cookie['Name'] === 'client_id') {
                        $this->clientId = $cookie['Value'];
                    }
                    elseif ($cookie['Name'] === 'client_secret') {
                        $this->clientSecret = $cookie['Value'];
                    }
                }
            }
            $this->client->getConfig('handler')->push(
                Guzzle::addHeader(
                    'Authorization',
                    'Basic ' . base64_encode(
                        "$this->clientId:$this->clientSecret"
                    )
                ),
                'prepare_body'
            );
        }
    }

    private function courseDescriptionItemRender($title, $data)
    {
        if (!empty($data['items'])) {
            $output = "<h4>$title</h4>";
            $output .= '<ul>';
            foreach ($data['items'] as $item) {
                $output .= "<li>$item</li>";
            }
            $output .= '</ul>';

            return '<div>' . $output . '</div>';
        }
    }

    /**
     * Get mapping category tag
     *
     * @param $tagName
     * @return array
     */
    public function getMappingTag($tagName)
    {
        // Set tags for mapping
        $mappingTags = array_change_key_case(
            [
                "development"          => $this->getCategories(
                    ["Technology", "Development"]
                ),
                "business"             => $this->getCategories(["Business"]),
                "it & software"        => $this->getCategories(
                    [
                        "Technology",
                        "Digital Literary & IT Skills",
                        "IT & Software",
                    ]
                ),
                "office productivity"  => $this->getCategories(
                    [
                        "Technology",
                        "Digital Literary & IT Skills",
                        "Office Productivity",
                    ]
                ),
                "personal development" => $this->getCategories(
                    ["Personal Development"]
                ),
                "design"               => $this->getCategories(["Technology", "Design"]),
                "lifestyle"            => $this->getCategories(
                    ["Personal Development", "Lifestyle"]
                ),
                "photography"          => $this->getCategories(
                    [
                        "Business",
                        "Personal Development",
                        "Industry Specific",
                        "Photography",
                    ]
                ),
                "health & fitness"     => $this->getCategories(
                    ["Personal Development", "Health & Fitness"]
                ),
                "teacher training"     => $this->getCategories(
                    ["Business", "Industry Specific", "Teacher Training"]
                ),
                "music"                => $this->getCategories(
                    ["Personal Development", "Music"]
                ),
                "academics"            => $this->getCategories(["Academics"]),
                "language"             => $this->getCategories(
                    ["Academics", "Personal Development", "Language"]
                ),
                "test prep"            => $this->getCategories(
                    ["Compliance", "Academics", "Test Prep"]
                ),
            ],
            CASE_LOWER
        );

        if (isset($mappingTags[strtolower($tagName)])) {
            return $mappingTags[strtolower($tagName)];
        }

        return [$tagName];
    }
}
