<?php

namespace App\Scraping\Course\Course360Training;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\LoggerController;
use App\Scraping\ScrapingBase;
use App\Scraping\ScrapingInterface;
use App\LocaleMap;
use GuzzleHttp\Exception\RequestException;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use League\Csv\Reader;


class Course360TrainingScraping extends ScrapingBase implements ScrapingInterface
{
    /** @var  Application */
    const BASE_URL = 'http://www.360training.com/';

    private $parentParentParentCategories = [];

    /**
     * @var \App\Browser\Guzzle
     */
    private $client;
    private $configCtrl;
    private $start = 0;

    public function __construct(Guzzle $client, ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        $this->client = $client::getInstance('360training', [
            'base_uri' => self::BASE_URL,
            'timeout' => 60
        ]);
        $this->configCtrl = $configCtrl;
        parent::__construct($configCtrl, $loggerCtrl);
    }

    /**
     * {@inheritdoc}
     */
    public function getList($limit = null)
    {
        $fileUrl = $this->configCtrl->getRootDir() . '/files/360training20160615.csv';
        $reader = Reader::createFromPath($fileUrl);
        $data = $reader
            ->stripBom(false)
            ->fetchAssoc();
        $data = iterator_to_array($data, false);

        $courses = [];
        $count = 0;
        foreach ($data as $row_id => $row) {
            if ($limit && $count >= $limit) {
                break;
            }
            $row['source_id'] = $row['Remote ID'];
            $courses[] = $row;
            $count++;
        }
        return $courses;
    }

    public function getCourseDetail($row)
    {
        $prop = null;
        try {
            $content = $this->client->get($row['Course URL'])->getBody()->getContents();
            $crawler = new Crawler();
            $crawler->addContent($content);
            if ($crawler->filter('h1.title')->count() > 0) {
                $prop['name'] = $crawler->filter('h1.title')->text();
            }
            if ($crawler->filter('.product-image img')->count() > 0) {
                $prop['image'] = rtrim($this::BASE_URL, '/') . $crawler->filter('.product-image img')->attr('src');
            }

            $prop['source_url'] = $crawler->filter('link[rel=canonical]')->attr('href');
            if ($crawler->filter('.descCont')->count() > 0) {
                $prop['description'] = $crawler->filter('.descCont')->text();
            }
            $prop['rating_loId'] = $crawler->filter('.StarRatings')->count();
            if ($crawler->filter('.pricing-tab .subtotal')->count() > 0) {
                $prop['price'] = $crawler->filter('.pricing-tab .subtotal')->text();
            }

            $html = $crawler->html();
            preg_match('/AjaxLoadMoreCourseReviewsView.+/', $html, $matches);
            if ($matches) {
                $prop['reviews_url'] = $prop['source_url'] . '/' . $matches[0] . '&firstIndex=0&lastIndex=99999';
            }
            $prop['id'] = $row['Remote ID'];
            $prop['tags'] = explode(",", $row['Tags']);
            $prop['tags'] = array_filter($prop['tags']);
        }
        catch (RequestException $e) {
            $this->logger($this->getName())->addError('Course url is error: ' . $e->getRequest()->getUri());
        }
        return $prop;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataDetail($row)
    {
        $item = $this->getCourseDetail($row);
        $record['title'] = trim($item['name']);
        $record['description'] = isset($item['description']) ? trim($item['description']) : '';
        if (!empty($item['workload'])) {
            $record['description'] .= "<p><strong>Commitment</strong>: {$item['workload']}</p>";
        }
        $record['source_url'] = $item['source_url'];
        $record['language'] = isset($item['primaryLanguages'][0]) ? (new LocaleMap())->getLanguageCode(strtolower($item['primaryLanguages'][0])) : 'en';
        $record['source_id'] = $item['id'];
        $record['marketplace'] = 1;
        $record['published'] = 1;
        $record['type'] = 'course';
        $record['pricing'] = isset($item['price']) ? ['price' => $item['price'], 'currency' => 'USD'] : [];
        $record['data']['title'] = $record['title'];
        $record['data']['image_url'] = isset($item['image']) ? $item['image'] : '';
        $record['data']['course_url'] = $record['source_url'];
        $record['data']['reviews_url'] = isset($item['reviews_url']) ? $item['reviews_url'] : '';
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['allow_enrolment'] = 'enquiry';
        return $record;
    }

    public function getNote($item)
    {
        $data = $item->data;
        if (!isset($data['data']['reviews_url']) || !$data['data']['reviews_url']) {
            return Application::SCRAPING_STATUS_CONTINUE;
        }

        $currentNotes = $this->client->get($data['data']['reviews_url'])->getBody()->getContents();
        $crawler = new Crawler();
        $crawler->addContent($currentNotes);
        $crawler->filter('.SingleReviewBox')->each(function ($elements) use (&$reviews) {
            // user name
            $reviewTitle = $elements->filter('.ReviewTitle')->text();
            preg_match('/(.+) From/', $reviewTitle, $matches);
            $review['title'] = $reviewTitle;
            $review['author_name'] = $matches[1];
            $review['note'] = $elements->filter('p')->text();
            $review['date'] = $elements->filter('.ReviewDate')->text();
            $review['date'] = $this->getDateTime($review['date']);
            $review['rating'] = $elements->filter('.ion-ios-star')->count();
            $reviews[] = $review;
        });
        // save data to scraping
        $record['title'] = trim($data['data']['title']);
        $record['source_url'] = $data['data']['reviews_url'];
        $record['source_id'] = $data['data']['course_id'];
        $record['type'] = 'note';
        $record['data']['course_id'] = $record['source_id'];
        $record['data']['reviews'] = $reviews;
        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function getStateData()
    {
        return [
            'start' => $this->start
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'course360training';
    }

    /**
     * Get mapping category tag for course360training
     * @param $tagName
     * @return array
     */
    private function getMappingTag($tagName)
    {
    }

    public function getYearDefault()
    {
        return [
            'a' => 1,
            'two' => 2,
            'three' => 3,
            'four' => 4,
            'five' => 5,
            'six' => 6,
            'seven' => 7,
            'eight' => 8,
            'nine' => 9,
            'ten' => 10
        ];
    }

    public function getDateTime($string)
    {
        preg_match('/Posted (.*) ago/', $string, $match);
        $date = explode('and', $match[1]);

        if (isset($date[1]))
        {
            $datetime = new DateTime('- ' . $date[1]);
            foreach($this->getYearDefault() as $yearString => $yearNumber) {
                $subdate = explode(' ', $match[0]);
                $match[0] = ltrim($match[0]);
                $date[0] = preg_replace("/^(".$yearString.")/", $yearNumber, $date[0]);
            }
            $datetime->modify('- ' . $date[0]);
        }
        else
        {
            $datetime = new DateTime('- ' . $date[0]);
        }
        return $datetime->getTimestamp();
    }

    public function hasNoteId() {
        return false;
    }

}
