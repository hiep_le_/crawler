<?php

namespace App;

use Pimple\Container;

class ConfigController
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get configuration by key
     *
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function getConfig($key)
    {
        static $cache;
        if (empty($cache[$key])) {
            $parents = explode('/', $key);
            $ref = $this->app;
            foreach ($parents as $parent) {
                if (is_array($ref) && array_key_exists($parent, $ref)) {
                    $ref = &$ref[$parent];
                }
                elseif ($ref instanceof Container) {
                    $ref = $ref[$parent];
                }
                else {
                    throw new \Exception(sprintf('The configuration is invalid for %s.', $key));
                }
            }
            $cache[$key] = $ref;
        }

        return $cache[$key];
    }

    /**
     * Get the application root
     */
    public function getRootDir()
    {
        return __DIR__ . '/../../';
    }

    public function getImportOptions($key)
    {
        return $this->getConfig('import.options/' . $key);
    }
}
