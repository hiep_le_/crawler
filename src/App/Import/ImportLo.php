<?php

namespace App\Import;

use App\ConfigController;
use App\Exception\RuntimeException;
use App\Go1\Go1LoController;
use App\Go1\Go1ScrapingController;
use App\Libraries\GoutteBase;
use App\LoggerController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingDataService;
use App\Scraping\ScrapingInterface;

class ImportLo extends Import implements ImportInterface
{
    private $go1LoCtrl;
    private $go1ScrapingController;
    private $scrapingData;

    public function __construct(
        Go1LoController $go1LoCtrl,
        Go1ScrapingController $go1ScrapingController,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    )
    {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1LoCtrl = $go1LoCtrl;
        $this->go1ScrapingController = $go1ScrapingController;
        $this->scrapingData = $scrapingData;
    }

    public function getName()
    {
        return 'lo';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['learning_pathway', 'course', 'module'];
    }

    /**
     * @inheritdoc
     */
    public function push(ScrapingInterface $provider, $item)
    {
        $providerName = $provider->getName();
        $scraping = $this->scrapingData->getScrapingDataBySourceId($item->source_id, $providerName);
        $instanceName = $this->configCtrl->getImportOptions("providers/{$providerName}/instance");
        $loId = null;
        switch ($item->action) {
            case 'update':
                $prepareLo = $this->prepareUpdateLo($instanceName, $scraping);
                $this->go1LoCtrl->updateLo($providerName, $prepareLo, $item->remote_id);
                $loId = $item->remote_id;
                $this->isCreated = false;
                break;

            case 'insert':
                $authorMail = $this->configCtrl->getImportOptions("providers/{$providerName}/mail");
                $prepareLo = $this->prepareCreateLo($instanceName, $authorMail, $scraping);
                $lo = $this->go1LoCtrl->createLo($providerName, $prepareLo);
                $loId = $lo->id;
                $this->go1ScrapingController->createMapping($item->provider, $item->source_id, $lo->id, $item->type);
                $this->isCreated = true;
                break;
        }

        if (!$this->go1ScrapingController->getMapping(
            $providerName,
            $item->source_id,
            $this->getName()
        )) {
            $this->go1ScrapingController->createMapping(
                $providerName,
                $item->source_id,
                $loId,
                $this->getName()
            );
        }
    }

    public function prepareUpdateLo($instanceName, $scraping)
    {
        $data = [
            'instance'    => $instanceName,
            'title'       => $scraping->data['title'],
            //            'description' => GoutteBase::formatHtml($scraping->data['description']),
            'description' => $scraping->data['description'],
            'pricing'     => $scraping->data['pricing'],
            'data'        => $scraping->data['data'],
        ];

        return $data;
    }

    public function prepareCreateLo($instanceName, $authorMail, $scraping)
    {
        return [
            'type'        => $scraping->type,
            'instance'    => $instanceName,
            'language'    => $scraping->data['language'],
            'title'       => $scraping->title,
            'description' => $scraping->data['description'],
            'tags'        => isset($scraping->data['tags']) ? $scraping->data['tags'] : [],
            'published'   => $scraping->data['published'],
            'marketplace' => $scraping->data['marketplace'],
            'pricing' => $scraping->data['pricing'],
            'image' => (isset($scraping->data['data']['image_url'])) ? $scraping->data['data']['image_url'] : '',
            'event' => (isset($scraping->data['event'])) ? $scraping->data['event'] : [],
            'data' => $scraping->data['data'],
            'author' => $authorMail
        ];
    }
}
