<?php

namespace App\Import;

use App\ConfigController;
use App\Go1\Go1NoteController;
use App\Go1\Go1ScrapingController;
use App\Go1\Go1VoteController;
use App\LoggerController;
use App\Scraping\ScrapingInterface;
use Symfony\Component\DomCrawler\Crawler;
use App\Scraping\ScrapingController;
use App\Scraping\ScrapingData;

class ImportNoteVote extends Import implements ImportInterface
{
    private $go1NoteCtrl;
    private $go1VoteCtrl;
    private $scrapingCtrl;
    private $scrapingData;
    private $go1ScrapingController;

    public function __construct(
        Go1NoteController $go1NoteCtrl,
        Go1VoteController $go1VoteCtrl,
        Go1ScrapingController $go1ScrapingController,
        ScrapingController $scrapingCtrl,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1NoteCtrl = $go1NoteCtrl;
        $this->go1VoteCtrl = $go1VoteCtrl;
        $this->go1ScrapingController = $go1ScrapingController;
        $this->scrapingCtrl = $scrapingCtrl;
        $this->scrapingData = $scrapingData;
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['note', 'vote'];
    }

    public function getName()
    {
        return 'note';
    }

    /**
     * @inheritdoc
     */
    public function push(ScrapingInterface $provider = null, $item)
    {
        $providerName = $provider->getName();
        $scraping = $this->scrapingData->getScrapingDataBySourceId($item->course_id, $providerName, 'note');
        $map = $this->go1ScrapingController->getMapping($providerName, $item->course_id, 'course');
        $loId = $map->remote_id;
        if (!$provider->hasNoteId()) {
            $this->importNoteNoId($scraping, $loId, $providerName);
        }
        else {
            $noteScraping = $this->getNoteScraping($scraping, $item->source_id);
            // $remoteId is id on #note or #vote, it only has value when note or vote is created
            $map = $this->go1ScrapingController->getMapping($item->source_id, $providerName, $item->type);
            $remoteId = $map->remote_id;

            ($item->type == 'note')
                && $this->importNoteId($noteScraping, $remoteId, $loId, $providerName, $item->action);
            ($item->type == 'vote')
                && $this->importVoteId($noteScraping, $remoteId, $loId, $providerName, $item->action);
            ($this->isCreated)
                && $this->go1ScrapingController->createMapping($providerName, $item->source_id, $remoteId, $item->type);
        }
    }

    public function getNoteScraping($scraping, $sourceId)
    {
        $reviews = $scraping->data['data']['reviews'];
        foreach($reviews as $review)
        {
            if ($review['id'] == $sourceId) {
                return $review;
            }
        }
    }

    public function importNoteNoId($scraping, $loId, $providerName)
    {
        $data = $scraping->data['data'];
        $reviews = $data['reviews'];
        $this->go1NoteCtrl->deleteNoteByLoId($loId, $providerName);
        $this->go1VoteCtrl->deleteVoteByLoId($loId, $providerName);

        foreach($reviews as $review)
        {
            // create note
            $note = $this->prepareCreateNote($review, $loId);
            $noteUuid = $this->go1NoteCtrl->createNote($note, $providerName);
            // create vote
            $vote = $this->prepareCreateVote($review, $loId);
            $voteId = $this->go1VoteCtrl->createVote($vote, $providerName);
        }
        $this->isCreated = true;
    }

    public function importNoteId($noteScraping, $remoteId, $loId, $providerName, $action)
    {
        if ($action == 'insert') {
            $note = $this->prepareCreateNote($noteScraping, $loId);
            $noteUuid = $this->go1NoteCtrl->createNote($note, $providerName);
            $this->entityId = $noteUuid;
            $this->isCreated = true;
        }
        else {
            $note = $this->prepareUpdateNote($noteScraping);
            $this->go1NoteCtrl->updateNote($note, $remoteId, $providerName);
        }
    }

    public function importVoteId($noteScraping, $remoteId, $loId, $providerName, $action)
    {
        if ($action == 'insert') {
            $vote = $this->prepareCreateVote($noteScraping, $loId);
            $voteUuid = $this->go1VoteCtrl->createVote($vote, $providerName);
            $this->entityId = $voteUuid;
            $this->isCreated = true;
        }
        else {
            $vote = $this->prepareUpdateVote($noteScraping);
            $this->go1VoteCtrl->updateVote($vote, $remoteId, $providerName);
        }
    }

    public function prepareCreateNote($noteScraping, $loId)
    {
        return [
            'loId' => $loId,
            'author' => $noteScraping['author_name'],
            'date' => $noteScraping['date'],
            'title' => $noteScraping['title'],
            'content' => $noteScraping['note'],
        ];
    }

    public function prepareUpdateNote($noteScraping)
    {
        return [
          'author' => $noteScraping['author_name'],
          'date' => $noteScraping['date'],
          'title' => $noteScraping['title'],
          'content' => $noteScraping['note'],
        ];
    }

    public function prepareCreateVote($noteScraping, $loId)
    {
        // create vote
        return [
            'value' => $noteScraping['rating'],
            'entity_type' => 'lo',
            'profile_id' => 0,
            'entity_id' => $loId
        ];
    }

    public function prepareUpdateVote($noteScraping)
    {
        return [
            'value' => $noteScraping['rating']
        ];
    }
}
