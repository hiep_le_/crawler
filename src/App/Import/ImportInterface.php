<?php

namespace App\Import;

use App\Scraping\ScrapingInterface;

interface ImportInterface
{
    /**
     * The types of data type supported
     * @return array
     */
    public function supportedTypes();

    /**
     * Push the data to remote service
     * @param \App\Scraping\ScrapingInterface $provider
     * @param $item
     * @return mixed
     */
    public function push(ScrapingInterface $provider, $item);

    /**
     * Determine a entity is created or updated
     * @return mixed
     */
    public function isCreated();

    /**
     * Determine a record will be ignore when error
     * @return boolean
     */
    public function isIgnoreOnError();

    /**
     * This method calling every item processed
     * @return mixed
     */
    public function destroy();

    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'import');

    public function getName();
}
