<?php

namespace App\Import;

use App\Exception\RuntimeException;

trait ImportTrait
{
    private $entityId;
    private $isCreated;
    private $isIgnoreOnError;
    protected $app;
    private $data;

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Set the entity created/updated id
     * @param $id
     * @return $this
     */
    protected function setEntityId($id)
    {
        $this->entityId = $id;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getEntityIdImported()
    {
        return $this->entityId;
    }

    /**
     * Set the created/updated status for current entity
     * @param $status
     * @return $this
     */
    protected function setIsCreated($status)
    {
        $this->isCreated = $status;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isCreated()
    {
        return $this->isCreated;
    }

    /**
     * @inheritdoc
     */
    public function isIgnoreOnError()
    {
        return $this->isIgnoreOnError;
    }

    /**
     * Mark a record must be ignored on error
     */
    public function mustIgnoreOnError()
    {
        $this->isIgnoreOnError = true;
    }

    /**
     * Validate the data type
     * @param $data
     * @throws RuntimeException
     */
    protected function validateItemType(array $data)
    {
        if (!in_array($data['type'], $this->supportedTypes())) {
            throw new RuntimeException(sprintf('Not supported the data type %s', $data['type']));
        }
    }

    /**
     * Get the current scraping data
     */
    protected function getData()
    {
        return $this->data;
    }

    /**
     * @inheritdoc
     */
    public function setData(array $data)
    {
        $this->validateItemType($data);
        $this->data = $data;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function destroy()
    {
        $this->data = null;
        $this->isCreated = false;
        $this->entityId = null;
        $this->isIgnoreOnError = false;
    }
}
