<?php

namespace App\Import;

use App\Import\Go1\LearningItem;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ImportServiceProvider implements ServiceProviderInterface
{
    /**
     * @inheritdoc
     */
    public function register(Container $c)
    {
        $c['import.lo'] = function (Container $c) {
            return new ImportLo(
                $c['go1.lo.ctrl'],
                $c['go1.scraping.ctrl'],
                $c['scraping.data'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['import.li'] = function (Container $c) {
            return new ImportLi(
                $c['go1.li.ctrl'],
                $c['go1.scraping.ctrl'],
                $c['scraping.data'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['import.lo_portal'] = function (Container $c) {
            return new ImportLoVirtualPortal(
                $c['go1.lo.ctrl'],
                $c['go1.portal.ctrl'],
                $c['go1.scraping.ctrl'],
                $c['scraping.data'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['import.portal'] = function (Container $c) {
            return new ImportPortal(
                $c['go1.portal.ctrl'],
                $c['go1.user.ctrl'],
                $c['scraping.file_upload'],
                $c['scraping.data'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };

        $c['import.notevote'] = function (Container $c) {
            return new ImportNoteVote(
                $c['go1.note.ctrl'],
                $c['go1.vote.ctrl'],
                $c['go1.scraping.ctrl'],
                $c['scraping.ctrl'],
                $c['scraping.data'],
                $c['config.ctrl'],
                $c['logger.ctrl']
            );
        };
        $c['import.ctrl'] = function (Container $c) {
            return new ImportController($c['application']);
        };
    }
}
