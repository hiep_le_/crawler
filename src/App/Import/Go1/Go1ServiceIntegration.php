<?php

namespace App\Import\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Exception\RuntimeException;
use App\Import\ImportTrait;
use App\LoggerController;
use Exception;

abstract class Go1ServiceIntegration
{
    use ImportTrait;

    protected $instance;
    protected $configCtrl;
    protected $loggerCtrl;
    protected $isIgnoreMap;

    public function __construct(ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        $this->configCtrl = $configCtrl;
        $this->loggerCtrl = $loggerCtrl;
        $this->isIgnoreMap = true;
    }

    /**
     * @param $provider
     * @throws Exception
     */
    protected function preparing($provider)
    {
        $this->instance = $this->getInstance($this->getImportOptions("providers/$provider/instance"));
    }

    /**
     * Get instance object
     * @param $instanceName
     * @return object
     * @throws Exception
     */
    protected function getInstance($instanceName)
    {
        static $cache;

        if (empty($cache[$instanceName])) {
            $response = Guzzle::getInstance('normal')->get("{$this->getImportOptions('services/portal')}/$instanceName");
            if (!$cache[$instanceName] = json_decode($response->getBody()->getContents())) {
                throw new Exception(sprintf('Not found instance %s.', $instanceName));
            }
        }

        return $cache[$instanceName];
    }

    /**
     * Get the import options
     * @param $key
     * @return mixed
     * @throws Exception
     */
    protected function getImportOptions($key)
    {
        return $this->app->container['ctrl.scraping']->getConfig('import.options/go1/' . $key);
    }

    public function isCreated()
    {
        return $this->isCreated;
    }

    public function isIgnoreMap()
    {
        return $this->isIgnoreMap;
    }

    public function isIgnoreOnError()
    {
        return $this->isIgnoreOnError;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    public function getEntityId()
    {
        return $this->entityId;
    }

    public function destroy()
    {
        $this->data = null;
        $this->entityId = null;
    }

    public function getName()
    {
        return 'li';
    }


    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'import')
    {
        $path = $this->configCtrl->getRootDir() . "/var/logs/import_{$name}.log";
        $this->loggerCtrl->setPathLogger($path);
        return $this->loggerCtrl->getLogger();
    }
}
