<?php

namespace App\Import;

use App\ConfigController;
use App\Exception\RuntimeException;
use App\Go1\Go1LoController;
use App\Go1\Go1PortalController;
use App\Go1\Go1ScrapingController;
use App\Go1\Go1UserController;
use App\LoggerController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingDataService;
use App\Scraping\ScrapingInterface;


class ImportLoVirtualPortal extends Import implements ImportInterface
{
    private $go1LoCtrl;
    private $scrapingData;
    private $go1PortalController;
    private $go1ScrapingController;
    private $go1UserController;

    public function __construct(
        Go1LoController $go1LoCtrl,
        Go1PortalController $go1PortalController,
        Go1ScrapingController $go1ScrapingController,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1LoCtrl = $go1LoCtrl;
        $this->go1PortalController = $go1PortalController;
        $this->go1ScrapingController = $go1ScrapingController;
        $this->scrapingData = $scrapingData;
    }

    public function getName()
    {
        return 'lo';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['learning_pathway', 'course', 'module'];
    }

    /**
     * @inheritdoc
     */
    public function push(ScrapingInterface $provider, $item)
    {
        $providerName = $provider->getName();
        $instanceName = $this->getInstanceNameVirtual($item);
        $rootInstanceName = $this->configCtrl->getImportOptions("providers/{$providerName}/instance");
        if (!$instanceName) {
            $instanceName = $rootInstanceName;
        }

        // importMapData
        $scraping = $this->scrapingData->getScrapingDataBySourceId($item->source_id, $providerName);
        switch ($item->action) {
            case 'update':
                $prepareLo = $this->prepareUpdateLo($instanceName, $scraping);
                $this->go1LoCtrl->updateLo($providerName, $prepareLo, $item->remote_id);
                $this->isCreated = false;
                break;

            case 'insert':
                $authorMail = $this->configCtrl->getImportOptions("providers/{$providerName}/mail");
                $prepareLo = $this->prepareCreateLo($instanceName, $authorMail, $scraping);
                $lo = $this->go1LoCtrl->createLo($providerName, $prepareLo);
                $this->go1ScrapingController->createMapping($providerName, $item->source_id, $lo->id, $item->type);
                $this->isCreated = true;
                break;
        }
    }

    public function getInstanceNameVirtual($item) {
        $portalMap = $this->go1ScrapingController->getMapping($item->provider, $item->source_id, 'portal');
        // we only insert courses which have portals
        if ($portalMap) {
            $portalId = $portalMap->remote_id;
            $portal = $this->go1PortalController->loadPortal($portalId);
            if ($portal && $this->go1PortalController->isIndexPortal($portal)) {
                return $portal->title;
            }
        }
    }

    public function prepareUpdateLo($instanceName, $scraping)
    {
        return [
            'instance' => $instanceName,
            'title' => $scraping->data['title'],
            'description' => $scraping->data['description'],
            'pricing' => $scraping->data['pricing'],
            'data' => $scraping->data['data'],
        ];
    }

    public function prepareCreateLo($instanceName, $authorMail, $scraping)
    {
        return [
            'type' => $scraping->type,
            'instance' => $instanceName,
            'language' => $scraping->data['language'],
            'title' => $scraping->title,
            'description' => $scraping->data['description'],
            'tags' => isset($scraping->data['tags']) ? $scraping->data['tags'] : [],
            'event' => $scraping->data['event'],
            'published' => $scraping->data['published'],
            'marketplace' => $scraping->data['marketplace'],
            'pricing' => $scraping->data['pricing'],
            'data' => $scraping->data['data'],
            'author' => $authorMail
        ];
    }
}
