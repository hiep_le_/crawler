<?php

namespace App\Import;

use App\ConfigController;
use App\LoggerController;

class Import
{
    private $data;
    protected $entityId = null;
    protected $isCreated = false;
    protected $isIgnoreOnError = true;
    protected $isIgnoreMap = false;
    /**
     * @var \App\ConfigController
     */
    protected $configCtrl;
    /**
     * @var \App\LoggerController
     */
    protected $loggerCtrl;

    /**
     * Import constructor.
     * @param \App\ConfigController $configCtrl
     * @param \App\LoggerController $loggerCtrl
     */
    public function __construct(ConfigController $configCtrl, LoggerController $loggerCtrl)
    {
        $this->configCtrl = $configCtrl;
        $this->loggerCtrl = $loggerCtrl;
        $this->isIgnoreMap = true;
    }

    public function isCreated() {
        return $this->isCreated;
    }

    public function isIgnoreMap() {
        return $this->isIgnoreMap;
    }

    public function isIgnoreOnError() {
        return $this->isIgnoreOnError;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }

    public function destroy() {
        $this->data = null;
        $this->entityId = null;
    }


    /**
     * @param string $name
     * @return \Monolog\Logger
     */
    public function logger($name = 'import')
    {
        $path = $this->configCtrl->getRootDir() . "/var/logs/import_{$name}.log";
        $this->loggerCtrl->setPathLogger($path);
        return $this->loggerCtrl->getLogger();
    }
}
