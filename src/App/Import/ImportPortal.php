<?php

namespace App\Import;

use App\ConfigController;
use App\Exception\RuntimeException;
use App\Go1\Go1LoController;
use App\Go1\Go1PortalController;
use App\Go1\Go1UserController;
use App\LoggerController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingDataService;
use App\Scraping\ScrapingFileUpload;
use App\Scraping\ScrapingInterface;


class ImportPortal extends Import implements ImportInterface
{
    private $go1PortalCtrl;
    private $go1UserController;
    private $scrapingFileUpload;
    private $scrapingData;

    public function __construct(
        Go1PortalController $go1PortalCtrl,
        Go1UserController $go1UserController,
        ScrapingFileUpload $scrapingFileUpload,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    ) {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1PortalCtrl = $go1PortalCtrl;
        $this->go1UserController = $go1UserController;
        $this->scrapingFileUpload = $scrapingFileUpload;
        $this->scrapingData = $scrapingData;
    }

    public function getName()
    {
        return 'portal';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['portal'];
    }

    /**
     * @inheritdoc
     */
    public function push(ScrapingInterface $provider, $item)
    {
        $providerName = $provider->getName();
        $scraping = $this->scrapingData->getScrapingDataBySourceId($item->source_id, $providerName);
        switch ($item->action) {
            case 'insert':
                $authorMail = $scraping->data['title'] . '@mygo1.com';
                $portalStep1 = $this->prepareCreatePortalStep1($authorMail, $scraping);
                $portalStep2 = $this->prepareCreatePortalStep2($authorMail, $scraping);
                $portal = $this->go1PortalCtrl->createPortal($providerName, $portalStep1, $portalStep2);
                $portalLogo = $scraping->data['portal_logo'];
                $fileUpload = $this->scrapingFileUpload->getBySourceUrl($portalLogo);
                $settings = [
                    'configuration.index_portal' => 1,
                    'files.logo' => $fileUpload->remote_url
                ];
                $this->go1PortalCtrl->updateSettingPortal($providerName, $portal['title'], $settings);
                $this->entityId = $portal['id'];
                $this->isCreated = true;
                break;
        }
    }

    public function prepareCreatePortalStep1($authorMail, $scraping)
    {
        return [
            'instance' => $scraping->data['title'] . '.mygo1.com',
            'email' => $authorMail,
            'password' => 'dvs-dv88',
            'first_name' => 'Admin',
            'last_name' => 'User',
        ];
    }

    public function prepareCreatePortalStep2($authorMail, $scraping)
    {
        return [
            'instance' => $scraping->data['title'] . '.mygo1.com',
            'email' => $authorMail,
            'license' => 20,
            'price' => 3740,
            'product' => 'marketplace',
            'source' => 'onboarding',
            'description' => 'index_portal'
        ];
    }
}
