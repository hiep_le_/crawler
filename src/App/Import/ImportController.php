<?php

namespace App\Import;

use App\Application;
use Exception;

class ImportController
{
    private $app;

    public function __construct(Application $application)
    {
        $this->app = $application;
    }

    /**
     * Get the list of types name
     *
     * @return array
     */
    public function getTypeNames()
    {
        return array_filter(array_map(function ($name) {
            $nameInfo = explode('.', $name);
            if (count($nameInfo) === 2 && $nameInfo[0] === 'import') {
                if (!in_array($nameInfo[1], ['ctrl', 'data', 'options'])) {
                    return $nameInfo[1];
                }
            }
        }, $this->app->keys()));
    }

    /**
     * Get import instance by data type
     * @param $typeName
     * @return mixed
     * @throws Exception
     */
    public function getType($typeName)
    {
        $typeNames = $this->getTypeNames();
        if (!in_array($typeName, $typeNames)) {
            throw new Exception(sprintf('Not found type %s', $typeName));
        }
        $instance = $this->app['import.' . $typeName];
        if (!$instance instanceof ImportInterface) {
            throw new Exception(sprintf('Type %s must be instance of ImportInterface', $typeName));
        }
        return $instance;
    }
}
