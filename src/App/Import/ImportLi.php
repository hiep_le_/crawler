<?php
namespace App\Import;
use App\ConfigController;
use App\Go1\Go1LiController;
use App\Go1\Go1ScrapingController;
use App\LoggerController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;

class ImportLi extends Import implements ImportInterface
{
    private $go1LiCtrl;
    private $go1ScrapingController;
    private $scrapingData;

    public function __construct(
        Go1LiController $go1LiCtrl,
        Go1ScrapingController $go1ScrapingController,
        ScrapingData $scrapingData,
        ConfigController $configCtrl,
        LoggerController $loggerCtrl
    )
    {
        parent::__construct($configCtrl, $loggerCtrl);
        $this->go1LiCtrl = $go1LiCtrl;
        $this->go1ScrapingController = $go1ScrapingController;
        $this->scrapingData = $scrapingData;
    }

    public function getName() {
        return 'li';
    }

    /**
     * @inheritdoc
     */
    public function supportedTypes()
    {
        return ['activities', 'attendance', 'h5p', 'iframe', 'video', 'quiz', 'resource', 'tincan', 'workshop'];
    }

    /**
     * @param \App\Scraping\ScrapingInterface $provider
     * @param $item
     */
    public function push(ScrapingInterface $provider, $item)
    {
        $providerName = $provider->getName();
        $data = $item->data;
        $scraping = $this->scrapingData->getScrapingDataBySourceId($data->source_id, $providerName);
        $instanceName = $this->configCtrl->getImportOptions("providers/{$providerName}/instance");
        switch ($data->action) {
            case 'update':
                $prepareLi = $this->prepareUpdateLi($instanceName, $scraping);
                $this->go1LiCtrl->updateLi($providerName, $prepareLi, $data->remote_id);
                $this->isCreated = false;
                break;
            case 'insert':
                $authorMail = $this->configCtrl->getImportOptions("providers/{$providerName}/mail");
                $prepareLi = $this->prepareCreateLi($instanceName, $authorMail, $scraping);
                $lo = $this->go1LiCtrl->createLi($providerName, $prepareLi, $data->remote_id);
                $this->go1ScrapingController->createMapping($item->provider, $item->source_id, $lo->id, $item->type);
                $this->isCreated = true;
                break;
        }
    }

    public function prepareUpdateLi($instanceName, $scraping)
    {
        return [
            'instance'    => $instanceName,
            'title'       => $scraping->data['title'],
            'description' => $scraping->data['description'],
            'data'        => !empty($scraping->data['resource']) ? array_merge($scraping->data['data'], ['resource' => $scraping->data['resource']]) : $scraping->data['data'],
        ];
    }

    public function prepareCreateLi($instanceName, $authorMail, $scraping)
    {
        return [
            'packageUrl'  => $scraping->data['packageUrl'],
            'type'        => $scraping->type,
            'instance'    => $instanceName,
            'language'    => $scraping->data['language'],
            'title'       => $scraping->title,
            'description' => $scraping->data['description'],
            'tags'        => isset($scraping->data['tags']) ? $scraping->data['tags'] : [],
            'published'   => $scraping->data['published'],
            'marketplace' => $scraping->data['marketplace'],
            'pricing'     => $scraping->data['pricing'],
            'data'        => !empty($scraping->data['resource']) ? array_merge($scraping->data['data'], ['resource' => $scraping->data['resource']]) : $scraping->data['data'],
            'author'      => $authorMail,
        ];
    }
}