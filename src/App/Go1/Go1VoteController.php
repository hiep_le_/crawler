<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use Exception;

class Go1VoteController
{
    private $client;
    private $configCtrl;
    private $voteUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $voteUrl
    ) {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->voteUrl = $voteUrl;
    }

    public function deleteVoteByLoId($loId, $provider) {
        $url = $this->voteUrl . "/star/lo/{$loId}";
        $jwt = $this->configCtrl->getImportOptions("providers/{$provider}/jwt");
        $options['headers']['Authorization'] = 'Bearer '. $jwt;
        $votes = $this->client->get($url,$options)->getBody()->getContents();
        $votes = json_decode($votes, true);

        foreach($votes as $vote) {
            $this->client->delete(
                $this->voteUrl . "/star/{$vote['id']}",
                $options
            );
        }
    }

    public function createVote($vote, $providerName) {
        $url = $this->voteUrl . "/star";
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options['json'] = $vote;
        $options['headers']['Authorization'] = 'Bearer '. $jwt;

        $res = $this->client->post($url, $options);
        if ($res->getStatusCode() == 201) {
            $id = json_decode($res->getBody()->getContents(), true);
            return $id;
        }
        throw new Exception('Can not create vote');
    }

    public function updateVote($vote, $remoteId, $providerName) {
        $url = $this->voteUrl . "/star/{$remoteId}";
        $options['json'] = $vote;
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options['headers']['Authorization'] = 'Bearer '. $jwt;

        $res = $this->client->put($url, $options);
        if ($res->getStatusCode() != 201) {
            throw new Exception('Can not update vote');
        }
    }
}