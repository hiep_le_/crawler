<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingInterface;
use Exception;

class Go1CloudinaryController
{
    private $client;
    private $configCtrl;
    private $cloudinaryUrl;

    public function __construct(Guzzle $client, ConfigController $configCtrl, $cloudinaryUrl) {
        $this->client = $client;
        $this->configCtrl = $configCtrl;
        $this->cloudinaryUrl = $cloudinaryUrl;
    }

    public function getInfo()
    {
        $url = $this->cloudinaryUrl . "/info";
        $res = $this->client->request('GET', $url);
        if (!$res->getStatusCode() == 200) {
            throw new Exception('Can not get info of cloudinary');
        }
        return json_decode($res->getBody()->getContents());
    }

    public function getSignature($providerName)
    {
        $mail = $this->configCtrl->getImportOptions("providers/$providerName/mail");
        $jwt = $this->configCtrl->getImportOptions("providers/$providerName/jwt");
        $url = "$this->cloudinaryUrl/sign";
        $options['json'] = [
            'options' => [
                'timestamp' => time(),
                'tags' => ['app:apiom', "email:$mail"]
            ]
        ];

        $options['headers']['Authorization'] = 'Bearer ' . $jwt;
        $cloudinary = $this->getInfo();
        $cloudinary->options = $options['json']['options'];
        $res = $this->client->post($url, $options);
        return [$cloudinary, json_decode($res->getBody()->getContents())];
    }

    public function uploadFile($providerName, $link)
    {
        list($info, $sign) = $this->getSignature($providerName);
        $url = "https://api.cloudinary.com/v1_1/{$info->cloud_name}/image/upload";
        $apiKey = $info->api_key;
        $multipart = [
            ['name' => 'api_key', 'contents' => $apiKey],
            ['name' => 'timestamp', 'contents' => $info->options['timestamp']],
            ['name' => 'tags', 'contents' => implode(',', $info->options['tags'])],
            ['name' => 'signature', 'contents' => $sign->sign],
            ['name' => 'file', 'contents' => fopen($link, 'r')],
        ];

        $res = $this->client->post($url, ['multipart' => $multipart]);
        if ($res->getStatusCode() != 200) {
            throw new Exception('Can not upload file');
        }

        $file = json_decode($res->getBody()->getContents());
        return $file;
    }

}