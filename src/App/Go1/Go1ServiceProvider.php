<?php

namespace App\Go1;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class Go1ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['go1.lo.ctrl'] = function (Container $c) {
            return new Go1LoController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['lo_url']
            );
        };
        $c['go1.li.ctrl'] = function (Container $c) {
            return new Go1LiController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['lo_url']
            );
        };
        $c['go1.note.ctrl'] = function (Container $c) {
            return new Go1NoteController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['note_url'],
                $c['note_firebase_url']
            );
        };
        $c['go1.vote.ctrl'] = function (Container $c) {
            return new Go1VoteController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['vote_url']
            );
        };
        $c['go1.user.ctrl'] = function (Container $c) {
            return new Go1UserController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['user_url']
            );
        };
        $c['go1.mail.ctrl'] = function (Container $c) {
            return new Go1MailController(
                $c['browser.guzzle'],
                $c['config.ctrl']
            );
        };
        $c['go1.portal.ctrl'] = function (Container $c) {
            return new Go1PortalController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['portal_url'],
                $c['onboard_url']
            );
        };
        $c['go1.cloudinary.ctrl'] = function (Container $c) {
            return new Go1CloudinaryController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['cloudinary_url']
            );
        };
        $c['go1.user.ctrl'] = function (Container $c) {
            return new Go1UserController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['user_url']
            );
        };
        $c['go1.scraping.ctrl'] = function (Container $c) {
            return new Go1ScrapingController(
                $c['browser.guzzle'],
                $c['config.ctrl'],
                $c['scraping_url']
            );
        };
    }
}