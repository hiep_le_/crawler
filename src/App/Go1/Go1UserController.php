<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Exception\RuntimeException;
use Symfony\Component\Config\Definition\Exception\Exception;

class Go1UserController
{
    private $client;
    private $configCtrl;
    private $userUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $userUrl
    ) {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->userUrl = $userUrl;
    }

    /**
     * Check an user exist in our service
     * @param $provider
     * @param string $email
     * @return bool
     * @throws RuntimeException
     */
    public function checkUserExist($provider, $email)
    {
        $instance = $this->configCtrl->getImportOptions(
            "providers/{$provider}/instance"
        );
        $response = $this->client->get(
            "{$this->userUrl}/found/$instance/$email"
        );
        if ($response = json_decode($response->getBody()->getContents())) {
            return (bool)$response->found;
        } else {
            throw new RuntimeException('Can not check user exist.');
        }
    }

    public function createUser($mail)
    {
        $url = "$this->userUrl/account/";
        $accountName = $this->configCtrl->getImportOptions("providers/account/instance");
        $user = [
            'instance' => $accountName,
            'email'      => $mail,
            'password'   => 'dvs-dv8a',
            'first_name' => 'Admin',
            'last_name'  => 'User',
        ];
        $response = $this->client->request('post', $url, ['json' => $user]);
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody()->getContents());
        }
        throw new Exception('Can not create user');
    }
}