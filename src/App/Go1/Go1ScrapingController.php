<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Exception\RuntimeException;
use App\Scraping\ScrapingInterface;
use Exception;
use GuzzleHttp\Exception\ClientException;

class Go1ScrapingController
{
    private $client;
    private $configCtrl;
    private $scrapingUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $scrapingUrl
    ) {
        $this->client = $client;
        $this->configCtrl = $configCtrl;
        $this->scrapingUrl = $scrapingUrl;
    }

    /**
     * @param $portal
     * @return mixed
     * @throws \Exception
     */
    public function getMapping($provider, $sourceId, $type)
    {
        $options['query'] = [
            'provider' => $provider,
            'source_id' => $sourceId,
            'type' => $type
        ];

        $res = $this->client->get($this->scrapingUrl . '/mapping', $options);
        if($res->getStatusCode() == 201) {
            $content = json_decode($res->getBody()->getContents(), true);
            return $content;
        }
        throw new Exception('Can not get scraping_mapping');
    }

    public function createMapping($providerName, $sourceId, $remoteId, $type)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");

        $options['json'] = [
            'provider' => $providerName,
            'source_id' => $sourceId,
            'remote_id' => (string)$remoteId,
            'type' => $type
        ];
        $options['headers']['Authorization'] = 'Bearer ' . $jwt;
        $res = $this->client->request('post', $this->scrapingUrl, $options);

        if ($res->getStatusCode() !== 201) {
            throw new Exception('Can not create scraping_mapping');
        }
    }
}