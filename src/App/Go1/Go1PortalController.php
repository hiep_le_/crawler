<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Exception\RuntimeException;
use App\Scraping\ScrapingInterface;
use Exception;
use GuzzleHttp\Exception\ClientException;

class Go1PortalController
{
    private $client;
    private $configCtrl;
    private $portalUrl;
    private $onboardUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $portalUrl,
        $onboardUrl
    ) {
        $this->client = $client;
        $this->configCtrl = $configCtrl;
        $this->portalUrl = $portalUrl;
        $this->onboardUrl = $onboardUrl;
    }

    /**
     * @param $portal
     * @return mixed
     * @throws Exception
     */
    public function loadPortal($portal)
    {
        $res = $this->client->get($this->portalUrl.'/'.$portal);
        if($res->getStatusCode() == 200) {
            $content = json_decode($res->getBody()->getContents());
            return $content;
        }
        throw new Exception('Can not load portal');
    }

    /**
     * @mixed $portal
     */
    public function isIndexPortal($portal)
    {
        if (property_exists($portal->configuration, 'index_portal')) {
            return true;
        }
        return false;
    }

    public function createPortal($providerName, $portalStep1, $portalStep2)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options['json'] = $portalStep1;
        $options['headers']['Authorization'] = 'Bearer ' . $jwt;
        $res = $this->client->request('post', $this->onboardUrl . "/step1", $options);

        if ($res->getStatusCode() !== 201) {
            throw new Exception('Can not create portal on step 1');
        }
        $portal = json_decode($res->getBody()->getContents(), true);

        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options['json'] = $portalStep2;
        $options['headers']['Authorization'] = 'Bearer ' . $jwt;
        $res = $this->client->request('post', $this->onboardUrl . "/step2", $options);
        if ($res->getStatusCode() == 204) {
            return $portal['portal'];
        }
        throw new Exception('Can not create portal on step 2');
    }

    public function updateSettingPortal($providerName, $portalName, $settings)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options['json'] = $settings;
        $options['headers']['Authorization'] = 'Bearer ' . $jwt;
        $res = $this->client->request('post', $this->portalUrl . "/{$portalName}", $options);
        if ($res->getStatusCode() !== 200) {
            throw new Exception('Can not update portal settings');
        }
    }
}