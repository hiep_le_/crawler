<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use Exception;

class Go1LiController
{
    const INTER_ACTIVE_URL = 'https://api.mygo1.com/v3/interactive-li-service/upload/';
    private $client;
    private $loUrl;
    private $configCtrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $loUrl
    ) {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->loUrl = $loUrl;
    }

    /**
     * @param $liId
     * @param $provider
     * @return bool
     */
    protected function getLI($provider, $liId)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/$provider/jwt");
        $response = $this->client->get(
            $this->loUrl . '/li/null',
            [
                'timeout' => -1,
                'query'   => [
                    'jwt'   => $jwt,
                    'ids'   => [$liId],
                    'limit' => 1,
                ],
            ]
        );

        if ($li = json_decode($response->getBody()->getContents())) {
            $li = reset($li);

            return $li->id == $liId ? $li : false;
        }

        return false;
    }

    public function createModule($provider, array $data, $LoId)
    {

        try {
            sleep(mt_rand(1, 3));
            // get config array
            $paramConfigs = $this->configCtrl->getImportOptions("providers/$provider");

            $response = $this->client->post(
                $this->loUrl . '/lo',
                [
                    'timeout' => -1,
                    'json'    => [
                        'author'      => $paramConfigs['mail'],
                        'instance'    => $paramConfigs['instance_id'],
                        'title'       => $data['title'] . " Module",
                        'published'   => 1,
                        'description' => '',
                        'type'        => 'module',
                        'enrollable'  => true,
                        'courseId'    => $LoId,
                        'link'        => [
                            'sourceId' => $LoId,
                            'weight'   => 0,
                        ],
                    ],
                    'query'   => [
                        'jwt' => $paramConfigs['jwt'],
                    ],
                ]
            );
            $content = $response->getBody()->getContents();
            if ($response = json_decode($content)) {
                if (!empty($response->id)) {
                    return $response->id;
                }
            }

            return false;
        }
        catch (RequestException $e) {
            echo "Error at request" . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * @param array $data
     * @param       $LoId
     * @param       $type
     * @param       $name
     * @return string
     */
    public function createLI($provider, array $data, $LoId, $type = 'zip', $name = '')
    {
        sleep(mt_rand(1, 3));
        // get config array
        $paramConfigs = $this->configCtrl->getImportOptions("providers/$provider");

        if (!empty($LoId)) {
            $moduleId = $this->searchModule($paramConfigs['instance_id'], $LoId);
        }
        if (empty($moduleId)) {
            //create Module
            $moduleId = $this->createModule($provider, $data, $LoId);
        }

        if ($moduleId && !empty($data['packageUrl'])) {

            try {
                if ($name == '' && $type == 'zip') {
                    // Step2 create LI
                    $response = $this->client->post(
                        $this->loUrl . '/li',
                        [
                            'timeout' => -1,
                            'json'    => [
                                'author'      => $paramConfigs['mail'],
                                'instance'    => $paramConfigs['instance_id'],
                                'title'       => $data['title'] . " Tin Can Package",
                                'published'   => 1,
                                'description' => '',
                                'data'        => [
                                    'interactiveType' => 'url',
                                    'url'             => $data['packageUrl'],
                                    'image'           => $data['data']['image'],
                                    'thumb'           => $data['data']['image'],
                                ],
                                'type'        => 'interactive',
                                'enrollable'  => true,
                                'link'        => [
                                    'sourceId' => $moduleId,
                                    'weight'   => 0,
                                ],
                            ],
                            'query'   => [
                                'jwt' => $paramConfigs['jwt'],
                            ],
                        ]
                    );
                    $content = $response->getBody()->getContents();
                    if ($liContent = json_decode($content)) {
                        $this->createInteractive($provider, $data, $liContent);
                    }

                    //insert resource
                    if (!empty($data['data']['resource'])) {
                        foreach ($data['data']['resource'] as $key => $value) {
                            $data['packageUrl'] = $value;
                            $type = pathinfo($value, PATHINFO_EXTENSION);
                            $this->createLI($provider, $data, $LoId, $type, $key);
                        }
                    }
                }
                else {
                    if (in_array($type, ['pdf', 'docx', 'jpeg', 'jpg', 'png'])) {
                        // Step2 create LI
                        $response = $this->client->post(
                            $this->loUrl . '/li',
                            [
                                'timeout' => -1,
                                'json'    => [
                                    'author'      => $paramConfigs['mail'],
                                    'instance'    => $paramConfigs['instance_id'],
                                    'title'       => $name,
                                    'published'   => 1,
                                    'description' => '',
                                    'data'        => [
                                        'path' => $data['packageUrl'],
                                    ],
                                    'type'        => 'iframe',
                                    'enrollable'  => true,
                                    'link'        => [
                                        'sourceId' => $moduleId,
                                        'weight'   => 0,
                                    ],
                                ],
                                'query'   => [
                                    'jwt' => $paramConfigs['jwt'],
                                ],
                            ]
                        );
                    }

                    if (in_array($type, ['mov', 'mp4'])) {
                        // Step2 create LI
                        $response = $this->client->post(
                            $this->loUrl . '/li',
                            [
                                'timeout' => -1,
                                'json'    => [
                                    'author'      => $paramConfigs['mail'],
                                    'instance'    => $paramConfigs['instance_id'],
                                    'title'       => $name,
                                    'published'   => 1,
                                    'description' => '',
                                    'data'        => [
                                        'videoType' => 'upload',
                                        'upload'    => $data['packageUrl'],
                                        'image'     => $data['data']['image'],
                                        'thumb'     => $data['data']['image'],
                                    ],
                                    'type'        => 'video',
                                    'enrollable'  => true,
                                    'link'        => [
                                        'sourceId' => $moduleId,
                                        'weight'   => 0,
                                    ],
                                ],
                                'query'   => [
                                    'jwt' => $paramConfigs['jwt'],
                                ],
                            ]
                        );
                    }

                    $content = $response->getBody()->getContents();
                    if ($liContent = json_decode($content)) {
                        $this->createInteractive($provider, $data, $liContent);
                    }
                }
            }
            catch (RequestException $e) {
                echo "Error at request" . $e->getRequest()->getUri();

                return '';
            }
        }
    }

    public function createInteractive($provider, array $data, $liContent)
    {
        try {
            sleep(mt_rand(1, 3));
            // get config array
            $paramConfigs = $this->configCtrl->getImportOptions("providers/$provider");
            $response = $this->client->post(
                self::INTER_ACTIVE_URL . $liContent->instance_id,
                [
                    'timeout' => -1,
                    'json'    => [
                        'scormId'    => $liContent->id,
                        'scormTitle' => 'Interactive LI',
                        'scormUrl'   => $data['packageUrl'],
                    ],
                    'query'   => [
                        'jwt' => $paramConfigs['jwt'],
                    ],
                ]
            );

            //            $test = $response->getBody()->getContents();
            return true;
        }
        catch (RequestException $e) {
            echo "Error at request" . $e->getRequest()->getUri();

            return '';
        }
    }

    public function updateLi($provider, $LIId, array $data)
    {
        $response = Guzzle::getInstance('normal')->put($this->scrapingCtrl->getImportOptions('services/lo') . '/li', [
            'json'  => $data,
            'query' => [
                'jwt' => $this->scrapingCtrl->getImportOptions("providers/$provider/jwt"),
            ],
        ]);

        $content = $response->getBody()->getContents();
        if (!$response = json_decode($content)) {
            throw new Exception(sprintf('Can not create new Lo [data: %s, response: %s].', json_encode($data), print_r($content, true)));
        }

        return $response->id ? $response : false;
    }

    /**
     * @param $instance_id
     * @param $loId
     * @return array
     */
    public function searchModule($instance_id, $loId)
    {
        $loUrl = $this->loUrl . '/lo/' . $instance_id . '?sort=lo.timestamp&author=true';
        $loUrl .= '&direction=DESC&enrolmentCount=1&loId=' . $loId;
        try {
            $loItems = $this->client->get($loUrl)->getBody()->getContents();
            if ($loItems) {
                $loItems = json_decode($loItems, true);
                if (!empty($loItems[0]['items'][0]['id'])) {
                    return $loItems[0]['items'][0]['id'];
                }
            }

            return [];
        }
        catch (RequestException $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    public function searchLi($data, $providerName)
    {
        $instance_id = $this->configCtrl->getImportOptions("providers/$providerName/instance_id");

        $loUrl = $this->loUrl . '/lo/' . $instance_id . '?sort=lo.timestamp&author=true';
        $loUrl .= '&direction=DESC&enrolmentCount=1&title=' . $data['title'];
        try {
            $response = $this->client->get($loUrl)->getBody()->getContents();
            if (!empty($response)) {
                $loItems = json_decode($response, true);
                if (!empty($loItems) && !empty($loItems[0]['data']['course_id']) && $loItems[0]['data']['course_id'] == $data['data']['course_id']) {
                    return $loItems[0]['id'];
                }
            }
        }
        catch (RequestException $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }
}