<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\ScrapingInterface;
use Exception;

class Go1NoteController
{
    private $client;
    private $configCtrl;
    private $noteUrl;
    private $noteFirebaseUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $note_url,
        $note_firebase_url
    ) {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->noteUrl = $note_url;
        $this->noteFirebaseUrl = $note_firebase_url;
    }

    public function deleteNoteByLoId($loId, $provider)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$provider}/jwt");
        $url = $this->noteFirebaseUrl . "/scrap/{$loId}.json";
        $notes = $this->client->get($url)
            ->getBody()->getContents();
        $notes = json_decode($notes, true);

        $options['headers']['Authorization'] = 'Bearer '. $jwt;

        if ($notes) {
            $this->client->delete(
                $this->noteUrl . "/scrap/lo/{$loId}",
                $options
            );
        }
    }

    public function createNote($note, $providerName)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options = [
            'json' => $note,
            'headers' => [
                'Authorization' => 'Bearer ' . $jwt
            ]
        ];

        $res = $this->client->post($this->noteUrl . "/scrap",$options);
        if ($res->getStatusCode() == 201) {
            $content = json_decode($res->getBody()->getContents(), true);
            return $content['uuid'];
        }
        throw new Exception('Can not create note');
    }

    public function updateNote($note, $noteId, $providerName)
    {
        $url = $this->noteUrl . "/scrap/note/{$noteId}";
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $options = [
            'json' => $note,
            'headers' => [
                'Authorization' => 'Bearer ' . $jwt
            ]
        ];

        $res = $this->client->put($url, $options);
        if ($res->getStatusCode() != 201) {
            throw new Exception('Can not update note');
        }
    }

    public function pushNotes($reviews, $loId, $providerName, ScrapingInterface $provider)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/{$providerName}/jwt");
        $url = $this->noteUrl . "/scrap";

        foreach($reviews as $review)
        {
            $noteData = [
                'loId' => $loId,
                'author' => $review['author_name'],
                'date' => $provider->getDateTime($review['date']),
                'title' => $review['title'],
                'content' => $review['note'],
                'externalId' => 0
            ];

            $options = [
                'json' => $noteData,
                'headers' => [
                    'Authorization' => 'Bearer ' . $jwt
                ]
            ];

            $this->client->post($url, $options);
        }
    }


}