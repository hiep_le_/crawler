<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\ConfigController;

class Go1MailController
{

    private $client;
    private $configCtrl;
    private $mailUrl;

    public function __construct(Guzzle $client, ConfigController $configCtrl)
    {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->mailUrl = rtrim(
            $configCtrl->getConfig('mail.options/endpoint'),
            '/'
        );
    }

    /**
     * Send email
     * @param string|array $recipients
     * @param string $subject
     * @param string $body
     * @param string $html
     * @param array $context
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function sendMail(
        $recipients,
        $subject,
        $body,
        $html = '',
        $context = []
    ) {
        $recipients = (array)$recipients;
        $jwt = $this->configCtrl->getConfig('mail.options/jwt');
        $url = $this->mailUrl.'/?jwt='.$jwt;
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. $jwt
            ],
            'json' => [
                'subject' => $subject,
                'body' => $body,
                'html' => $html,
                'context' => $context,
            ],
        ];

        $promises = [];
        foreach ($recipients as $recipient) {
            $options['json']['recipient'] = $recipient;
            $promises[$recipient] = $this->client->postAsync($url, $options);
        }

        $responses = \GuzzleHttp\Promise\settle($promises)->wait();

        $responses = array_map(
            function ($response) {
                return isset($response['value']) && $response['value']->getStatusCode(
                    ) === 200;
            },
            $responses
        );

        return array_product($responses) === 1;
    }

    /**
     * Send email for scraping error
     * @param string $providerName
     * @param string $errorMsg
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function sendMailDataProviderTestFailed($providerName, $errorMsg)
    {
        $recipients = $this->configCtrl->getConfig('maintainers');
        $time = date('Y-m-d H:i:s');
        $subject = "Test failed for data provider $providerName on [{$time}]";
        $body = file_get_contents(
            $this->configCtrl->getRootDir(
            ).'/resources/Mail/provider-test-failed-template.html'
        );

        $result = $this->sendMail(
            $recipients,
            $subject,
            strip_tags($body),
            $body,
            [
                '!date' => date('Y-m-d H:i:s'),
                '!providerName' => $providerName,
                '!errorMsg' => $errorMsg,
            ]
        );

        return $result;
    }

    public function sendMailScrapingThrowException($providerName, $exception)
    {
        $recipients = $this->configCtrl->getConfig('maintainers');
        $subject = "The scraping process for $providerName has stopped.";
        $body = file_get_contents(
            $this->configCtrl->getRootDir(
            ).'/resources/Mail/scraping-has-stopped-template.html'
        );
        $result = $this->sendMail(
            $recipients,
            $subject,
            strip_tags($body),
            $body,
            [
                '!date' => date('Y-m-d H:i:s'),
                '!providerName' => $providerName,
                '!errorMsg' => $exception->__toString(),
            ]
        );

        return $result;
    }
}
