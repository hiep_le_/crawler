<?php

namespace App\Go1;

use App\Browser\Guzzle;
use App\Exception\RuntimeException;
use App\ConfigController;
use Exception;
use GuzzleHttp\Exception\RequestException;

class Go1LoController
{
    /**
     * @var \App\Browser\Guzzle
     */
    private $client;

    /**
     * @var \App\ConfigController
     */
    private $configCtrl;

    /**
     * @var string
     */
    private $loUrl;

    public function __construct(
        Guzzle $client,
        ConfigController $configCtrl,
        $loUrl
    ) {
        $this->client = $client::getInstance('normal');
        $this->configCtrl = $configCtrl;
        $this->loUrl = $loUrl;
    }

    /**
     * @param $LIId
     * @param $provider
     * @return bool
     */
    protected function getLo($provider, $loId)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/$provider/jwt");
        $options = [
            'query' => [
                'jwt'   => $jwt,
                'ids'   => [$loId],
                'limit' => 1,
            ],
            'headers' => [
                'Authorization' => 'Bearer '. $jwt
            ]
        ];

        $response = $this->client->get(
            $this->loUrl . '/li/null',
            $options
        );

        if ($lo = json_decode($response->getBody()->getContents())) {
            $lo = reset($lo);

            return $lo->id == $loId ? $lo : false;
        }

        return false;
    }

    /**
     * @param       $provider
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function createLo($provider, array $data)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/$provider/jwt");
        $response = $this->client->post(
            $this->loUrl . '/lo',
            [
                'json'  => $data,
                'headers' => [
                    'Authorization' => 'Bearer '. $jwt
                ]
            ]
        );

        $content = $response->getBody()->getContents();
        if (!$response = json_decode($content)) {
            throw new Exception(
                sprintf(
                    'Can not create new Lo [data: %s, response: %s].',
                    json_encode($data),
                    print_r($content, true)
                )
            );
        }

        return $response->id ? $response : false;
    }

    /**
     * @param       $provider
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function updateLo($provider, array $data, $loId)
    {
        $jwt = $this->configCtrl->getImportOptions("providers/$provider/jwt");
        $response = $this->client->put($this->loUrl . "/lo/{$loId}", [
            'json'  => $data,
            'headers' => [
                'Authorization' => 'Bearer '. $jwt
            ]
        ]);
        if ($response->getStatusCode() !== 204) {
            throw new Exception(sprintf('Can not update Lo [data: %s, loId: %s].', json_encode($data), print_r($loId)));
        }

        return true;
    }

    public function searchLo($data, $providerName)
    {
        $loUrl = $this->loUrl.'/lo/null?sort=lo.timestamp&author=true';
        $loUrl .= '&direction=DESC&enrolmentCount=1&title=' . $data['title'];
        try {
            $loItems = $this->client->get($loUrl)->getBody()->getContents();
            if ($loItems) {
                $loItems = json_decode($loItems, true);
                foreach ($loItems as $lo) {
                    if (isset($lo['data']['course_id']) &&
                        $lo['data']['course_id'] == $data['data']['course_id']
                    ) {
                        return $lo['id'];
                    }
                }
            }
        }
        catch (RequestException $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }
}