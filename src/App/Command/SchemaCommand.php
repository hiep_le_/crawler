<?php

namespace App\Command;

use App\Application;
use App\SchemaDefinition;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Exception;

class SchemaCommand extends Command
{
    private $app;
    
    public function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct();
    }
    
    public function configure()
    {
        $this->setName('schema');
        $this->setDescription('Manage the db schema.');
        $this->addArgument('action', InputArgument::OPTIONAL, 'The action you want to perform <comment>[install, drop, testing]</comment>.');
        $this->addOption('y', 'y', InputOption::VALUE_NONE, 'Force drop the database');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $dbname = $this->app['dbs.options']['default']['dbname'];
        $db = $this->app['db'];
        $action = $input->getArgument('action');
        switch ($action) {
            case 'install':
            case 'testing':
                // Try create new the empty database
                if ($action == 'install')
                {
                    call_user_func(function () use ($db, $dbname) {
                        $params = $db->getParams();
                        unset($params['dbname']);
                        $tmpConn = DriverManager::getConnection($params);
                        if (!isset($params['path'])) {
                            $dbname = $tmpConn->getDatabasePlatform()->quoteSingleIdentifier($dbname);
                        }
                        $tmpConn->getSchemaManager()->createDatabase($dbname);
                        $tmpConn->close();
                    });
                }

                // Install the schema
                $db->beginTransaction();
                try {
                    $schema = (new SchemaDefinition())->definition();
                    foreach ($schema->toSql($db->getDatabasePlatform()) as $sql) {
                        $db->exec($sql . ';');
                    }

                    $db->commit();
                    $output->writeln('The database schemas has been installed.');
                }
                catch (Exception $e) {
                    $db->rollBack();
                    ($action == 'install') && $db->getSchemaManager()->dropDatabase($dbname);
                    throw $e;
                }
                break;

            case 'drop':
                if (!$input->getOption('y')) {
                    $helper = $this->getHelper('question');
                    $question = new ConfirmationQuestion('This action cannot be undone do you want to continue <comment>[y/n]</comment>?', false);

                    if (!$helper->ask($input, $output, $question)) {
                        return;
                    }
                }
                $db->beginTransaction();
                try {
                    // Drop database
                    $db->getSchemaManager()->dropDatabase($dbname);
                    $db->commit();
                    $output->writeln('The database schemas has been dropped.');
                } catch (Exception $e) {
                    $db->rollBack();
                    throw new $e;
                }
                break;

            default:
                $this->getApplication()->printHelp($this, $input, $output);
                break;
        }
    }
}