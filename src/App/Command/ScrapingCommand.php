<?php

namespace App\Command;

use App\Application;
use App\Scraping\ScrapingInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Scraping\ScrapingController;
use App\Scraping\ScrapingData;
use App\DataProvider\DataProviderInterface;

class ScrapingCommand extends Command
{
    const TYPE = ['portal', 'course', 'note'];
    private $app;

    /**
     * @var ScrapingController
     */
    private $scrapingCtrl;

    /**
     * @var ScrapingData
     */
    private $scrapingData;

    /**
     * @var array
     */
    private $providerNames;

    /**
     * @var ScrapingInterface
     */
    private $provider;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->scrapingCtrl = $this->app['scraping.ctrl'];
        $this->scrapingData = $this->app['scraping.data'];
        $this->providerNames = $this->scrapingCtrl->getProviderNames();
        parent::__construct();
    }

    public function configure()
    {
        $providerNames = implode(', ', $this->providerNames);
        $types = implode(', ', self::TYPE);
        $this->setName('scraping');
        $this->addArgument('provider', InputArgument::REQUIRED, "The data provider identifier [Allows: <comment>$providerNames</comment>]");
        $this->addOption('type', null, InputOption::VALUE_REQUIRED, "The type identifier [Allows: <comment>$types</comment>]]", 'course');
        $this->addOption('override', 'o', InputOption::VALUE_OPTIONAL, 'scraping only note', 0);
        $this->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit records.', null);
        $this->setDescription('Scraping data from the target site');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $providerName = $input->getArgument('provider');
        $type = $input->getOption('type');
        $override = $input->getOption('override');
        $limit = $input->getOption('limit');
        $this->provider = $this->scrapingCtrl->getProvider($providerName);
        // Validate the data provider id
        if (!in_array($providerName, $this->providerNames)) {
            $output->writeln('<error>The data provider is invalid.</error>');
            return false;
        }
        $progress = new ProgressBar($output);
        $this->provider->setProgress($progress);
        $progress->setFormat("%current% [%bar%] %elapsed%\n<info>%message%</info>\n");
        $this->setMessage($progress, 'Starting...');
        $progress->start();

        while (!$this->context('stop_loop', false)) {
            $data = $this->provider->getList($limit);
            if (!$data || ($limit && $progress->getProgress() >= $limit)) {
                $this->setMessage($progress, sprintf(
                    "Scraping has been completed \n<comment>* Created: %s</comment>",
                    $this->context('created', 0)
                ));
                $this->context('stop_loop', true, true);
            }
            else {
                try {
                    $this->scrapingData($data, $type, $override, $this->provider, $progress);
                    $this->context('stop_loop', true, true);
                }
                catch (Exception $e) {
                    $this->provider->logger($providerName)->addError($e);
                    throw new Exception($e->getMessage());
//                     $this->['service.mail']->sendMailScrapingThrowException($providerName, $e);
                }
            }
        }
        $progress->finish();
    }

    public function scrapingData($data, $type, $override, ScrapingInterface $provider, $progress)
    {
        $providerName = $provider->getName();
        foreach ($data as $key => $item) {
            // skip if data is scrapped
            if (!$override && $this->scrapingData->getScrapingDataBySourceId(
                    $item['source_id'],
                    $this->provider->getName(),
                    $type
                )
            ) {
                continue;
            }
            // Process the course data
            switch ($type) {
                case 'note':
                    $detail = $provider->getNote($item);
                    break;
                case 'course':
                    $detail = $provider->getDataDetail($item);
                    break;
                case 'portal':
                    $detail = $provider->getPortal($item);
                    break;
            }
            if ($detail === Application::SCRAPING_STATUS_CONTINUE) {
                $this->provider->logger($providerName)->addInfo(
                    '[Get Course detail] - Continue.'
                );
                continue;
            }
            // Save course to scraping_data
            $this->scrapingData->saveScrapingData($detail, $providerName);
            $this->context('created', $this->context('created', 0) + 1, true);
            $this->setMessage($progress, 'Scraping: ' . $detail['title']);
        }
    }

    /**
     * @param        $progress
     * @param        $message
     * @param string $name
     */
    private function setMessage($progress, $message, $name = 'message')
    {
        $progress->setMessage($message, $name);
        $progress->display();
    }

    /**
     * @param      $name
     * @param null $value
     * @param bool $assign
     * @return mixed
     */
    private function context($name, $value = null, $assign = false)
    {
        static $context;

        // Assign value for variable If variable not found or $assign = true
        ($assign || !isset($context[$name])) && $context[$name] = $value;

        return $context[$name];
    }

    //    private function validateDataStructure($providerName, OutputInterface $output)
    //    {
    //        // Validate data provider test if found test provider
    //        $testDataProviders = $this->container['ctrl.scraping']->getTestDataProviders();
    //        if (!in_array($providerName, $testDataProviders)) {
    //            $errorMsg = sprintf('Not found test class for %s data provider.', $providerName);
    //            $this->getApplication()->logger($providerName)->addError($errorMsg);
    //
    //            throw new Exception($errorMsg);
    //        } else {
    //            $testProcess = $this->container['ctrl.scraping']->getDataProviderTest($providerName);
    //
    //            try {
    //                $output->writeln("Testing {$providerName} data structure...");
    //                $testProcess->mustRun();
    //                $this->getApplication()->logger($providerName)->addInfo($testProcess->getOutput());
    //                $output->writeln('Test case is passed The scraping is now continued');
    //            } catch (ProcessFailedException $e) {
    //                // Send error email to maintainers
    //                $this->container['service.mail']->sendMailDataProviderTestFailed($providerName, $e->getMessage());
    //                $this->getApplication()->logger($providerName)->addError($testProcess->getOutput());
    //                $this->getApplication()->logger($providerName)->addError($e);
    //
    //                throw new Exception(sprintf('%s data structure is not valid.', $providerName));
    //            }
    //        }
    //    }
}
