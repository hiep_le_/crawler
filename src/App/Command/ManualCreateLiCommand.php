<?php
/**
 * @author Tien Nguyen
 * @author Tien Nguyen <tien.nguyen@go1.com>
 * Date: 24/01/2017
 * Time: 13:10
 */

namespace App\Command;

use App\Application;
use App\Browser\Guzzle;
use App\ConfigController;
use App\Scraping\Course\LearningHeroes\LearningHeroesScraping;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ManualCreateLiCommand extends Command
{
    const INTER_ACTIVE_URL = 'https://api.mygo1.com/v3/interactive-li-service/upload/';
    private $app;
    private $client;
    private $loUrl;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->client = Guzzle::getInstance('normal');
        $this->loUrl = $this->app['lo_url'];
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('manual_li')
            ->setDescription('Manual Create Li.')
            ->setHelp("This command allows you Create Li...");

        $this->addArgument('path', InputArgument::REQUIRED, 'direct file path.');
        $this->addArgument('loId', InputArgument::REQUIRED, 'id of Lo');
        $this->addArgument('title', InputArgument::REQUIRED, 'Title of Li');;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Create Li',
            '============',
            '',
        ]);

        $uploadModel = new LearningHeroesScraping($this->app['config.ctrl'], $this->app['logger.ctrl'], $this->app['db']);

        $fileUploaded = $uploadModel->getS3File($input->getArgument('path'));
        $output->writeln($fileUploaded);

        $data = $this->searchLo($input->getArgument('loId'));
        $this->createLI(
            array_merge($data, ['packageUrl' => $fileUploaded]),
            $input->getArgument('loId'),
            pathinfo($fileUploaded, PATHINFO_EXTENSION),
            $input->getArgument('title')
        );
    }

    public function searchLo($loId)
    {
        $instance_id = $this->app['config.ctrl']->getImportOptions("providers/learningheroes/instance_id");
        $loUrl = $this->loUrl . '/lo/' . $instance_id . '?sort=lo.timestamp&author=true';
        $loUrl .= '&direction=DESC&enrolmentCount=1&loId=' . $loId;
        try {
            $loItems = $this->client->get($loUrl)->getBody()->getContents();
            if ($loItems) {
                $loItems = json_decode($loItems, true);

                $result = [
                    'title'     => $loItems[0]['title'],
                    'module_id' => $loItems[0]['items'][0]['id'],
                    'data'      => [
                        'image' => $loItems[0]['data']['image'],
                        'thumb' => $loItems[0]['data']['image'],
                    ],
                ];

                return $result;
            }
        }
        catch (RequestException $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    public function createModule($provider, array $data, $LoId)
    {

        try {
            // get config array
            $paramConfigs = $this->app['config.ctrl']->getImportOptions("providers/$provider");

            $response = $this->client->post(
                $this->loUrl . '/lo',
                [
                    'timeout' => -1,
                    'json'    => [
                        'author'      => $paramConfigs['mail'],
                        'instance'    => $paramConfigs['instance_id'],
                        'title'       => $data['title'] . " Module",
                        'published'   => 1,
                        'description' => '',
                        'type'        => 'module',
                        'enrollable'  => true,
                        'courseId'    => $LoId,
                        'link'        => [
                            'sourceId' => $LoId,
                            'weight'   => 0,
                        ],
                    ],
                    'query'   => [
                        'jwt' => $paramConfigs['jwt'],
                    ],
                ]
            );

            $content = $response->getBody()->getContents();

            if ($response = json_decode($content)) {
                if (!empty($response->id)) {
                    return $response->id;
                }
            }

            return false;
        }
        catch (RequestException $e) {
            echo "Error at request" . $e->getRequest()->getUri();

            return '';
        }
    }

    /**
     * @param array $data
     * @param       $LoId
     * @param       $type
     * @param       $name
     * @return string
     */
    public function createLI(array $data, $LoId, $type, $name)
    {
        // get config array
        $paramConfigs = $this->app['config.ctrl']->getImportOptions("providers/learningheroes");

        if (!empty($data['module_id'])) {
            $moduleId = $data['module_id'];
        }

        if (empty($moduleId)) {
            //create Module
            $moduleId = $this->createModule('learningheroes', $data, $LoId);
        }

        if ($moduleId && !empty($data['packageUrl'])) {

            try {
                if (in_array($type, ['pdf', 'docx', 'jpeg', 'jpg', 'png'])) {
                    // Step2 create LI
                    $response = $this->client->post(
                        $this->loUrl . '/li',
                        [
                            'timeout' => -1,
                            'json'    => [
                                'author'      => $paramConfigs['mail'],
                                'instance'    => $paramConfigs['instance_id'],
                                'title'       => $name,
                                'published'   => 1,
                                'description' => '',
                                'data'        => [
                                    'path' => $data['packageUrl'],
                                ],
                                'type'        => 'iframe',
                                'enrollable'  => true,
                                'link'        => [
                                    'sourceId' => $moduleId,
                                    'weight'   => 0,
                                ],
                            ],
                            'query'   => [
                                'jwt' => $paramConfigs['jwt'],
                            ],
                        ]
                    );
                }
                if ($type == 'zip') {
                    // Step2 create LI
                    $response = $this->client->post(
                        $this->loUrl . '/li',
                        [
                            'timeout' => -1,
                            'json'    => [
                                'author'      => $paramConfigs['mail'],
                                'instance'    => $paramConfigs['instance_id'],
                                'title'       => $name,
                                'published'   => 1,
                                'description' => '',
                                'data'        => [
                                    'interactiveType' => 'url',
                                    'url'             => $data['packageUrl'],
                                    'image'           => $data['data']['image'],
                                    'thumb'           => $data['data']['image'],
                                ],
                                'type'        => 'interactive',
                                'enrollable'  => true,
                                'link'        => [
                                    'sourceId' => $moduleId,
                                    'weight'   => 0,
                                ],
                            ],
                            'query'   => [
                                'jwt' => $paramConfigs['jwt'],
                            ],
                        ]
                    );
                }
                if (in_array($type, ['mov', 'mp4'])) {
                    // Step2 create LI
                    $response = $this->client->post(
                        $this->loUrl . '/li',
                        [
                            'timeout' => -1,
                            'json'    => [
                                'author'      => $paramConfigs['mail'],
                                'instance'    => $paramConfigs['instance_id'],
                                'title'       => $name,
                                'published'   => 1,
                                'description' => '',
                                'data'        => [
                                    'videoType' => 'upload',
                                    'upload'    => $data['packageUrl'],
                                    'image'     => $data['data']['image'],
                                    'thumb'     => $data['data']['image'],
                                ],
                                'type'        => 'video',
                                'enrollable'  => true,
                                'link'        => [
                                    'sourceId' => $moduleId,
                                    'weight'   => 0,
                                ],
                            ],
                            'query'   => [
                                'jwt' => $paramConfigs['jwt'],
                            ],
                        ]
                    );
                }

                $content = $response->getBody()->getContents();
                if ($liContent = json_decode($content)) {
                    $this->createInteractive('learningheroes', $data, $liContent);
                }
            }
            catch (RequestException $e) {
                echo "Error at request" . $e->getRequest()->getUri();

                return '';
            }
        }
    }

    public function createInteractive($provider, array $data, $liContent)
    {
        try {
            // get config array
            $paramConfigs = $this->app['config.ctrl']->getImportOptions("providers/$provider");
            $response = $this->client->post(
                self::INTER_ACTIVE_URL . $liContent->instance_id,
                [
                    'json'  => [
                        'scormId'    => $liContent->id,
                        'scormTitle' => 'Interactive LI',
                        'scormUrl'   => $data['packageUrl'],
                    ],
                    'query' => [
                        'jwt' => $paramConfigs['jwt'],
                    ],
                ]
            );

            //            $test = $response->getBody()->getContents();
            return true;
        }
        catch (RequestException $e) {
            echo "Error at request" . $e->getRequest()->getUri();

            return '';
        }
    }

}