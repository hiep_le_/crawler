<?php

namespace App\Command;

use App\Application;
use App\Exception\RuntimeException;
use App\PrepareImport\PrepareImportInterface;
use App\Scraping\ScrapingController;
use App\Scraping\ScrapingData;
use App\Scraping\ScrapingInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PrepareImportCommand extends Command
{
    private $app;
    /** @var \App\PrepareImport\PrepareImportController */
    private $controller;
    /**
     * @var ScrapingController
     */
    private $scrapingCtrl;
    /**
     * @var ScrapingData
     */
    private $scrapingData;
    /** @var  array */
    private $providerNames;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->controller = $this->app['prepare_import.ctrl'];
        $this->scrapingCtrl = $this->app['scraping.ctrl'];
        $this->scrapingData = $this->app['scraping.data'];
        $this->providerNames = $this->scrapingCtrl->getProviderNames();
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $providerNames = implode(', ', $this->providerNames);
        $typeNames = implode(', ', $this->controller->getTypeNames());

        $this->setName('prepare_import');
        $this->addArgument('provider', InputArgument::REQUIRED, "The data provider identifier [Allows: <comment>$providerNames</comment>]");
        $this->addOption('type', null, InputOption::VALUE_REQUIRED, "The type name [Allows: <comment>".$typeNames."</comment>]");
        $this->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit records.', null);
        $this->setDescription('Repair Import, Check data (may be lo, li, note) on scrap promote to know we will insert, update, delete data');
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $typeName = $input->getOption('type');
        /** @var PrepareImportInterface $prepareImport */
        $prepareImport = $this->controller->getType($typeName);
        $providerName = $input->getArgument('provider');
        /** @var ScrapingInterface $provider */
        $provider = $this->scrapingCtrl->getProvider($providerName);
        $limit = $input->getOption('limit');

        $progress = new ProgressBar($output);
        $progress->setFormat("%current% [%bar%] %elapsed%\n<info>%message%</info>\n");
        $this->setMessage($progress, 'Starting...');
        $progress->start();

        while (!$this->context('stop_loop', false)) {
            $queuedImports = $this->scrapingData->getScrapingRecords($prepareImport->supportedTypes(), $providerName, $limit);

            if (!$queuedImports || ($limit && $progress->getProgress() >= $limit)) {
                $this->setMessage($progress, sprintf(
                    "The prepare import data has been imported successfully.\n<comment>* Ignored: %s\n* Updated: %s\n* Created: %s</comment>",
                    $this->context('ignored', 0),
                    $this->context('updated', 0),
                    $this->context('created', 0)
                ));
                $this->context('stop_loop', true, true);
            } else {
                foreach ($queuedImports as $item) {
                    try {
                        $this->setMessage($progress, 'Importing: ' . $item->title);
                        $prepareImport->push($provider, $item);
                        if ($prepareImport->isCreated()) {
                            $this->context('created', $this->context('created', 0) + 1, true);
                            $prepareImport->logger($prepareImport->getName())->addInfo(sprintf('Record with source id %s from %s has been queued for create new (%s:%s:%s).', $item->source_id, $item->provider, $typeName, $item->type, $prepareImport->getEntityId()));
                        } else {
                            $this->context('updated', $this->context('updated', 0) + 1, true);
                            $prepareImport->logger($prepareImport->getName())
                                ->addInfo(sprintf('Record with source id %s from %s has been queued for update (%s:%s:%s).', $item->source_id, $item->provider, $typeName, $item->type, $prepareImport->getEntityId()));
                        }
                    } catch (RuntimeException $ex) {
                        $prepareImport->logger($prepareImport->getName())->addError($ex->getMessage());
                        throw new Exception($ex->getMessage());
                    }
                    $progress->advance();
                    $prepareImport->destroy();
                }
                $this->context('stop_loop', true, true);
            }
        }

        $progress->finish();
    }

    /**
     * @param $progress
     * @param $message
     * @param string $name
     */
    private function setMessage($progress, $message, $name = 'message')
    {
        $progress->setMessage($message, $name);
        $progress->display();
    }

    /**
     * @param $name
     * @param null $value
     * @param bool $assign
     * @return mixed
     */
    private function context($name, $value = null, $assign = false)
    {
        static $context;

        // Assign value for variable If variable not found or $assign = true
        ($assign || !isset($context[$name])) && $context[$name] = $value;

        return $context[$name];
    }
}
