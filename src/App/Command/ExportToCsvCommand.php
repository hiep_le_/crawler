<?php

namespace App\Command;

use App\Application;
use App\ConfigController;
use App\Scraping\ScrapingData;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WyriHaximus\HtmlCompress\Factory;

class ExportToCsvCommand extends Command
{
    /** @var Application */
    private $app;
    /** @var  ConfigController */
    private $configCtrl;
    /** @var  ScrapingData */
    private $scrapingData;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->configCtrl = $this->app['config.ctrl'];
        $this->scrapingData = $this->app['scraping.data'];
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('export2csv');
        $this->setDescription('Export course data to csv.');
        $this->addArgument('provider', InputArgument::REQUIRED, 'The data provider name');
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $provider = $input->getArgument('provider');
        $fp = $this->getFileResource($this->configCtrl->getRootDir(), $provider, $this->app->getEnvironment());

        $header = $this->getCsvHeader();

        $output->writeln('Wait a moment...');

        $data = $this->scrapingData->getRecordQueuedImport([], $provider, null);
        // Add header
        fputcsv($fp, $header);
        foreach ($data as $item) {
            $row = [
                'Name' => $item->title,
                'Description' => isset($item->data['description']) ? Factory::constructFastest()->compress($item->data['description']) : '',
                'Source Image URI' => $item->data['data']['image_url'],
                'Source Video URI' => isset($item->data['data']['video_url']) ? $item->data['data']['video_url'] : '',
                'Allow Enrolment' => $item->data['data']['allow_enrolment'],
                'Remote ID' => $item->source_id,
                'Course URL' => $item->data['data']['course_url'],
                'Marketplace' => isset($item->data['marketplace']) ? $item->data['marketplace'] : 1,
                'Sharing' => isset($item->data['sharing']) ? $item->data['sharing'] : 1,
                'Type' => $item->data['type'],
                'Language' => $item->data['language'],
                'Author Name' => $item->data['author']['full_name'],
                'Author Image' => isset($item->data['author']['avatar']) ? $item->data['author']['avatar'] : '',
                'Author Job Title' => isset($item->data['author']['job_title']) ? $item->data['author']['job_title'] : '',
                'Author Is Organization Name' => isset($item->data['author']['is_organization_name']) && $item->data['author']['is_organization_name'],
                'Price per user' => isset($item->data['pricing']['price']) ? $item->data['pricing']['price'] : '',
                'Currency' => isset($item->data['pricing']['currency']) ? $item->data['pricing']['currency'] : '',
                'Tags' => isset($item->data['tags']) ? implode(',', $item->data['tags']) : '',
                'Event Start Date' => isset($item->data['event']['start']) ? $item->data['event']['start'] : '',
                'Event End Date' => isset($item->data['event']['end']) ? $item->data['event']['end'] : '',
                'Event TimeZone' => isset($item->data['event']['timezone']) ? $item->data['event']['timezone'] : '',
            ];
            fputcsv($fp, $row);
        }
        $output->writeln(sprintf('%s\'s courses has been exported successfully.', $provider));
        fclose($fp);
    }

    private function getFileResource($rootDir, $provider, $environment)
    {
        $dir = $rootDir . 'var/export/csv/' . date('d-m-Y') . '/' . $environment . '/';
        $file = $provider . '.csv';

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $fp = fopen($dir . $file, 'w+');

        if (!$fp) {
            throw new Exception(sprintf('Can not open file %s', $dir . $file));
        }

        return $fp;
    }

    private function getCsvHeader()
    {
        return [
            'Name',
            'Description',
            'Source Image URI',
            'Source Video URI',
            'Allow Enrolment',
            'Remote ID',
            'Course URL',
            'Marketplace',
            'Sharing',
            'Type',
            'Language',
            'Author Name',
            'Author Image',
            'Author Job Title',
            'Author Is Organization Name',
            'Price per user',
            'Currency',
            'Tags',
            'Event Start Date',
            'Event End Date',
            'Event TimeZone',
        ];
    }
}
