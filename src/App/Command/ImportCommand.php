<?php

namespace App\Command;

use App\Application;
use App\Exception\RuntimeException;
use App\Import\ImportInterface;
use App\Import\ImportMapData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Scraping\ScrapingController;
use App\Scraping\ScrapingData;
use Exception;

class ImportCommand extends Command
{
    private $app;

    /** @var \App\Import\ImportController */
    private $controller;
    /** @var ScrapingController  */
    private $scrapingCtrl;
    /** @var ScrapingData */
    private $scrapingData;
    private $providerNames;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->controller = $this->app['import.ctrl'];
        $this->scrapingCtrl = $this->app['scraping.ctrl'];
        $this->scrapingData = $this->app['scraping.data'];
        $this->providerNames = $this->scrapingCtrl->getProviderNames();
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('import');
        $providerNames = implode(', ', $this->providerNames);
        $typeNames = implode(', ', $this->controller->getTypeNames());
        $this->setDescription('Import scraped data');
        $this->addArgument('provider', InputArgument::REQUIRED, "The data provider identifier [Allows: <comment>$providerNames</comment>]");
        $this->addOption('type', null, InputOption::VALUE_REQUIRED, "The name of import plugin [Allows: <comment>$typeNames</comment>]");
        $this->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit records.', null);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $providerName = $input->getArgument('provider');
        $typeName = $input->getOption('type');
        $limit = $input->getOption('limit');
        $provider = $this->scrapingCtrl->getProvider($providerName);
        /** @var $import ImportInterface */
        $import = $this->controller->getType($typeName);
        $progress = new ProgressBar($output);
        $progress->setFormat("%current% [%bar%] %elapsed%\n<info>%message%</info>\n");
        $this->setMessage($progress, 'Starting...');
        $progress->start();
        while (!$this->context('stop_loop', false)) {
            $queuedImports = $this->scrapingData->getPrepareImportRecords($import->supportedTypes(), $providerName, $limit);
            if (!$queuedImports || ($limit && $progress->getProgress() >= $limit)) {
                $this->setMessage($progress, sprintf(
                    "The scraping data has been imported successfully.\n<comment>* Ignored: %s\n* Updated: %s\n* Created: %s</comment>",
                    $this->context('ignored', 0),
                    $this->context('updated', 0),
                    $this->context('created', 0)
                ));
                $this->context('stop_loop', true, true);
            }
            else {
                foreach ($queuedImports as $item) {
                    try {
                        $this->setMessage($progress, 'Importing prepare_import_data with id: ' . $item->id);
                        $import->push($provider, $item);
                        $this->scrapingData->deletePrepareImportRecord($item);
                        if ($import->isCreated()) {
                            $this->context('created', $this->context('created', 0) + 1, true);
                            $import->logger($import->getName())->addInfo(sprintf('Record with source id %s from %s has been queued for create new (%s:%s).', $item->source_id, $item->provider, $typeName, $item->type));
                        } else {
                            $this->context('updated', $this->context('updated', 0) + 1, true);
                            $import->logger($import->getName())->addInfo(sprintf('Record with source id %s from %s has been queued for update (%s:%s).', $item->source_id, $item->provider, $typeName, $item->type));
                        }
                    } catch (RuntimeException $ex) {
                        if ($import->isIgnoreOnError()) {
                            $this->scrapingData->updateScrapingRecordQueueStatus($item->scraping_data_id, false);
                            $this->context('ignored', $this->context('ignored', 0) + 1, true);
                            $this->controller->getLogger($typeName)->addInfo('Record has been ignored', ['data' => $item]);
                        }
                        $this->controller->getLogger($typeName)->addError($ex);
                    }
                    $progress->advance();
                    $import->destroy();
                }
                $this->context('stop_loop', true, true);
            }
        }
        $progress->finish();
    }

    /**
     * @param $progress
     * @param $message
     * @param string $name
     */
    private function setMessage($progress, $message, $name = 'message')
    {
        $progress->setMessage($message, $name);
        $progress->display();
    }

    /**
     * @param $name
     * @param null $value
     * @param bool $assign
     * @return mixed
     */
    private function context($name, $value = null, $assign = false)
    {
        static $context;

        // Assign value for variable If variable not found or $assign = true
        ($assign || !isset($context[$name])) && $context[$name] = $value;

        return $context[$name];
    }
}
