<?php

namespace App\Test\Scraping\Allconferences;

use App\Test\ScrapingTestCase;
use go1\Scraping\controller\ScrapingMappingCreateController;
use go1\Scraping\domain\scraping\ScrapingMappingRepository;
use Silex\Application\UrlGeneratorTrait;
use go1\schema\mock\UserMockTrait;
use Symfony\Component\HttpFoundation\Request;

class ScrapingMappingGetControllerTest extends ScrapingTestCase
{
    use UserMockTrait;
    private $app;
    private $data;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
    }

    public function testGet()
    {
        $this->app->extend('scraping_mapping.repository', function () {
            return $this->getScrapingMapping();
        });

        $this->data = [
            'provider' => 'something',
            'source_id' => (string)rand(0, 100),
            'type' => 'something'
        ];

        // case get
        $req = Request::create('/mapping', 'GET');
        $req->query->replace($this->data);
        $res = $this->app->handle($req);
        $this->assertEquals(201, $res->getStatusCode());
        $mapping = json_decode($res->getContent(), true);
        $this->assertNotNull($mapping);
    }

    public function getScrapingMapping()
    {
        $scrapingMapping = $this->getMockBuilder(ScrapingMappingRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();

        $scrapingMapping->expects($this->once())
            ->method('get')
            ->willReturnCallback(function () {
                return (object) $this->data;
            });

        return $scrapingMapping;
    }


}