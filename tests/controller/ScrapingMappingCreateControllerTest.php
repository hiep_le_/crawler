<?php

namespace App\Test\Scraping\Allconferences;

use App\Test\ScrapingTestCase;
use go1\Scraping\controller\ScrapingMappingCreateController;
use go1\Scraping\domain\scraping\ScrapingMappingRepository;
use Silex\Application\UrlGeneratorTrait;
use go1\schema\mock\UserMockTrait;
use Symfony\Component\HttpFoundation\Request;

class ScrapingMappingCreateControllerTest extends ScrapingTestCase
{
    use UserMockTrait;
    private $app;

    /**
     * @var ScrapingMappingCreateController $scrapingMappingCreate
     */
    private $scrapingMappingCreate;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
    }

    public function testCreate()
    {
        $this->app->extend('scraping_mapping.repository', function () {
            return $this->getScrapingMapping();
        });

        $data = [
            'provider' => 'something',
            'source_id' => (string)rand(0, 100),
            'remote_id' => (string)rand(0, 100),
            'type' => 'something'
        ];
        $req = Request::create('/', 'POST');
        $req->request->replace($data);
        $req->query->replace(['jwt' => $this->getJwt('admin@dev.com', null, null, ['Admin on #Accounts'])]);
        $res = $this->app->handle($req);
        $this->assertEquals(201, $res->getStatusCode());
        $mapping = json_decode($res->getContent());
        $this->assertNotNull($mapping->scraping_mapping_id);
    }

    public function getScrapingMapping() {
        $scrapingMapping = $this->getMockBuilder(ScrapingMappingRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['insert'])
            ->getMock();

        $scrapingMapping->expects($this->once())
            ->method('insert')
            ->willReturnCallback(function () {
                return rand(0, 100);
            });

        return $scrapingMapping;
    }
}