<?php
namespace App\Test\PrepareImport;

use App\PrepareImport\PrepareImportData;
use App\Test\ScrapingTestCase;

class PrepareImportDataTest extends ScrapingTestCase
{
    protected $app;
    /** @var PrepareImportData */
    protected $prepareImportData;
    private   $data;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->prepareImportData = new PrepareImportData($this->app['db']);
        $this->data = [
            'source_id'  => rand(0, 100),
            'source_url' => 'something',
            'remote_id'  => rand(0, 100),
            'action'     => 'insert',
            'provider'   => 'something',
            'type'       => 'lo',
        ];
        $this->prepareImportData->insertPrepareImport($this->data);
    }

    /**
     * test create and get
     */
    public function testGetPrepareImport()
    {
        $record = $this->prepareImportData->getPrepareImport(
            $this->data['source_id'],
            $this->data['provider'],
            $this->data['type']
        );
        $this->assertNotEmpty($record);
    }

    public function testUpdatePrepareImport()
    {
        $record = $this->prepareImportData->getPrepareImport(
            $this->data['source_id'],
            $this->data['provider'],
            $this->data['type']
        );
        $this->data['provider'] = 'provider is changed';
        $this->prepareImportData->updatePrepareImport($this->data, $record->id);
        $record2 = $this->prepareImportData->getPrepareImport(
            $this->data['source_id'],
            $this->data['provider'],
            $this->data['type']
        );
        $this->assertEquals('provider is changed', $record2->provider);
    }
}
