<?php

namespace App\PrepareImport;

use App\Test\ScrapingTestCase;

class PrepareImportServiceProviderTest extends ScrapingTestCase
{
    public function testRegister()
    {
        $app = $this->getApp();
        $this->assertInstanceOf('App\PrepareImport\PrepareImportData', $app['prepare_import.data']);
        $this->assertInstanceOf('App\PrepareImport\PrepareImportLo', $app['prepare_import.lo']);
        $this->assertInstanceOf('App\PrepareImport\PrepareImportNoteVote', $app['prepare_import.notevote']);
    }
}
