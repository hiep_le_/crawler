<?php

namespace App\Test\PrepareImport;

use App\PrepareImport\PrepareImportController;
use App\Test\ScrapingTestCase;

class PrepareImportControllerTest extends ScrapingTestCase
{
    protected $app;
    /** @var PrepareImportController */
    protected $prepareImportController;


    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->prepareImportController = new PrepareImportController($this->app);
    }

    public function testGetTypeNames()
    {
        $typeNames = $this->prepareImportController->getTypeNames();
        $this->assertCount(4, $typeNames);
    }

    /**
     * @expectedException \Exception
     */
    public function testInvalidGetType()
    {
        $this->prepareImportController->getType('something');
    }

    public function testValidGetType()
    {
        $type = $this->prepareImportController->getType('notevote');
        $this->assertInstanceOf('App\PrepareImport\PrepareImportNoteVote', $type);
        $type = $this->prepareImportController->getType('lo');
        $this->assertInstanceOf('App\PrepareImport\PrepareImportLo', $type);
        $type = $this->prepareImportController->getType('portal');
        $this->assertInstanceOf('App\PrepareImport\PrepareImportPortal', $type);
    }
}
