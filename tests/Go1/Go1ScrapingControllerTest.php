<?php

namespace App\Test\Go1;

use App\Browser\Guzzle;
use App\Go1\Go1PortalController;
use App\Go1\Go1ScrapingController;
use App\Import\Import;
use App\Test\ScrapingTestCase;
use go1\util\PortalChecker;
use GuzzleHttp\Psr7\Response;


class Go1ScrapingControllerTest extends ScrapingTestCase
{
    protected $app;

    /**
     * @var Go1ScrapingController go1ScrapinglCtrl
     */
    protected $go1ScrapinglCtrl;


    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
    }

    public function testGetMapping()
    {
        $provider = 'something';
        $sourceId = 10;
        $type = 'something';

        $this->app->extend("go1.scraping.ctrl", function() {
            return $this->getMockScraping();
        });

        $this->go1ScrapinglCtrl = $this->app['go1.scraping.ctrl'];
        $this->go1ScrapinglCtrl->getMapping($provider, $sourceId, $type);
    }

    public function getMockScraping() {
        $scraping = $this->getMockBuilder(Go1ScrapingController::class)
            ->setConstructorArgs([
                $this->getClient(),
                $this->app['config.ctrl'],
                $this->app['scraping_url']
            ])
            ->setMethods()
            ->getMock();
        return $scraping;
    }

    public function getClient() {
        $client = $this->getMockBuilder(Guzzle::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();

        $client->expects($this->once())->method('get')
            ->willReturnCallback(function() {
                return new Response(201, [], json_encode(['foo' => 'bar']));
            });
        return $client;
    }
}