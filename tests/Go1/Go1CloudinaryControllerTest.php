<?php

namespace App\Test\Go1;

use App\Browser\Guzzle;
use App\Go1\Go1CloudinaryController;
use App\Test\ScrapingTestCase;
use GuzzleHttp\Psr7\Response;

class Go1CloudinaryControllerTest extends ScrapingTestCase
{
    protected $app;

    /**
     * @var Go1CloudinaryController go1CloudinaryCtrl
     */
    protected $go1CloudinaryCtrl;

    private $providerName;
    private $link;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->providerName = 'allconferences';
        $this->link = 'http://content.allconferences.com/images/conference/logos/big/image_103657.jpg';
    }

    public function testUploadFile()
    {
        $this->app->extend('go1.cloudinary.ctrl', function () {
            return $this->getMockCloudinary();
        });

        $this->go1CloudinaryCtrl = $this->app['go1.cloudinary.ctrl'];
        $providerName = 'allconferences';
        $file = $this->go1CloudinaryCtrl->uploadFile($providerName, $this->link);
        $this->assertNotEmpty($file);
    }

    public function getMockClient() {
        $client = $this->getMockBuilder(Guzzle::class)
            ->disableOriginalConstructor()
            ->setMethods(['post'])
            ->getMock();
        $client->expects($this->once())
            ->method('post')
            ->willReturnCallback(function () {
                return new Response(200, [], json_encode(['something']));
            });
        return $client;
    }

    public function getMockCloudinary() {
        $cloudinary = $this->getMockBuilder(Go1CloudinaryController::class)
            ->setConstructorArgs([
                $this->getMockClient(),
                $this->app['config.ctrl'],
                $this->app['cloudinary_url']
            ])
            ->setMethods(['getSignature'])
            ->getMock();

        $cloudinary->expects($this->once())
            ->method('getSignature')
            ->with($this->providerName)
            ->willReturnCallback(function () {
                return [
                    (object)[
                        'cloud_name' => 'something',
                        'api_key' => 'something',
                        'options' => ['timestamp' => 'something', 'tags' => ['st']],
                    ],
                    (object)[
                        'sign' => 'something'
                    ]
                ];
            });
        return $cloudinary;
    }
}