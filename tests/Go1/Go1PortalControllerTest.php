<?php

namespace App\Test\Go1;

use App\Browser\Guzzle;
use App\Go1\Go1PortalController;
use App\Import\Import;
use App\Test\ScrapingTestCase;
use go1\util\PortalChecker;
use GuzzleHttp\Psr7\Response;
use Exception;

class Go1PortalControllerTest extends ScrapingTestCase
{
    protected $app;

    /**
     * @var Go1PortalController go1PortalCtrl
     */
    protected $go1PortalCtrl;

    /**
     * @var PortalChecker
     */
    protected $portalChecker;


    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->portalChecker = $this->app['portal_checker'];
    }

    public function testLoadPortal()
    {
        $portalId = rand(0, 100);
        $this->app->extend('go1.portal.ctrl', function () {
            $portalCtrl = $this->getMockBuilder(Go1PortalController::class)
                ->setConstructorArgs([
                   $this->getClient(),
                   $this->app['config.ctrl'],
                   $this->app['portal_url'],
                   $this->app['onboard_url']
                ])
                ->setMethods()
                ->getMock();
            return $portalCtrl;
        });
        $this->go1PortalCtrl = $this->app['go1.portal.ctrl'];
        $res = $this->go1PortalCtrl->loadPortal($portalId);
        $this->assertNotNull($res);
    }

    public function testLoadPortalIsVirtual()
    {
        $portalId = '352512';
        $this->app->extend('go1.portal.ctrl', function () {
            $portalCtrl = $this->getMockBuilder(Go1PortalController::class)
                ->setConstructorArgs([
                    $this->getClientVirtual(),
                    $this->app['config.ctrl'],
                    $this->app['portal_url'],
                    $this->app['onboard_url']
                ])
                ->setMethods()
                ->getMock();
            return $portalCtrl;
        });
        $this->go1PortalCtrl = $this->app['go1.portal.ctrl'];
        $portal = $this->go1PortalCtrl->loadPortal($portalId);
        $this->assertTrue($this->portalChecker->isVirtual($portal));
    }

    public function getClient() {
        $client = $this->getMockBuilder(Guzzle::class)
            ->setMethods(['get'])
            ->getMock();
        $client->expects($this->once())
            ->method('get')
            ->willReturnCallback(function () {
               return new Response(200, [], json_encode(['so' => 'so']));
            });

        return $client;
    }

    public function getClientVirtual() {
        $client = $this->getMockBuilder(Guzzle::class)
            ->setMethods(['get'])
            ->getMock();

        $portal = (object)[
            'configuration' => (object) [
                'is_virtual' => true
            ]
        ];

        $client->expects($this->once())
            ->method('get')
            ->willReturnCallback(function () use ($portal) {
               return new Response(200, [], json_encode($portal));
            });
        return $client;
    }
}