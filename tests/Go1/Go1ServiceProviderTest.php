<?php

namespace App\Test\Go1;

use App\Go1\Go1ScrapingController;
use App\Test\ScrapingTestCase;

class Go1ServiceProviderTest extends ScrapingTestCase
{
    public function testRegister()
    {
        $app = $this->getApp();
        $this->assertInstanceOf(Go1ScrapingController::class,$app['go1.scraping.ctrl']);
    }
}
