<?php

namespace App\Test\Go1;

use App\Import\Import;
use App\Test\ScrapingTestCase;
use GuzzleHttp\Psr7\Response;


class Go1UserControllerTest extends ScrapingTestCase
{
    protected $app;

    /**
     * @var Import
     */
    protected $import;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->import = $this->app['ctrl.import'];
    }

//    public function testCheckUserExist()
//    {
//        $clientMock = $this->getMockBuilder(get_class($this->app['browser.guzzle']))
//            ->disableOriginalConstructor()
//            ->setMethods('get')
//            ->getMock();
//
//        $clientMock->expects($this->once())->method(['get', 'api/account/found/../'])
//            ->willReturn(
//                new Response([
//                    'something else'
//                ])
//            );
//
//        $mock = $this->getMockBuilder(get_class($this->import))
//            ->setConstructorArgs($clientMock, $this->app['ctrl.scraping'])
//            ->getMock();
//
//        $mock->checkUserExest('something', 'mail');
//    }
//
//    public function testPushUser()
//    {
//        $clientMock = $this->getMockBuilder(get_class($this->app['browser.guzzle']))
//            ->disableOriginalConstructor()
//            ->setMethods('post')
//            ->getMock();
//        $clientMock->expects($this->once())->method(['post', 'account_url', ['body_json']])
//            ->willReturn(new Response(['something']));
//
//        $mock = $this->getMockBuilder(get_class($this->import))
//            ->setConstructorArgs($clientMock, $this->app['ctrl.scraping'])
//            ->setMethods(['checkUserExist'])
//            ->getMock();
//        $mock->expects($this->once())->method('checkUsrExist')->willReturn(false);
//        $mock->checkUserExist('provider', 'yourmail');
//    }
}