<?php

namespace App\Test\Browser;

use App\Browser\Guzzle;
use App\Test\ScrapingTestCase;

class BrowserTest extends ScrapingTestCase
{
    public function testGetInstance()
    {
        $normalInstance = Guzzle::getInstance('normal');
        $this->assertInstanceOf('App\Browser\Guzzle', $normalInstance);
        // it should be cached
        $somethingInstance = Guzzle::getInstance('something', ['timeout' => time()]);
        $somethingCache = Guzzle::getInstance('something', ['timeout' => time() + 100]);
        $this->assertEquals($somethingInstance, $somethingCache);
    }

    public function testGetCookieJar()
    {
        $cookie = (new Guzzle())->getCookieJar();
        $this->assertInstanceOf('GuzzleHttp\Cookie\CookieJar', $cookie);

        $config['cookies'] = ['something'];
        $cookie = (new Guzzle($config))->getCookieJar();
        $this->assertEquals('something', reset($cookie));
    }

    public function testAddHeader()
    {
        $guzzle = (new Guzzle());
        $guzzle->getConfig('handler')->push(Guzzle::addHeader('Authorization', 'Basic xxx'), 'prepare_body_something');

        $class = new \ReflectionObject($guzzle->getConfig('handler'));
        $method = $class->getMethod('findByName');
        $method->setAccessible(true);
        $handlerIndex = $method->invokeArgs($guzzle->getConfig('handler'), ['prepare_body_something']);
        $this->assertNotEmpty($handlerIndex);
    }
}
