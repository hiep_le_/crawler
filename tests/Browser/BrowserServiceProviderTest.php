<?php

namespace App\Test\Browser;

use App\Test\ScrapingTestCase;

class BrowserServiceProviderTest extends ScrapingTestCase
{
    public function testRegister()
    {
        $app = $this->getApp();
        $this->assertInstanceOf('App\Browser\Guzzle', $app['browser.guzzle']);
    }
}
