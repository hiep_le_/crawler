<?php

namespace App\Test\Import;
use App\Import\ImportNoteVote;
use App\Scraping\Course\Course360Training\Course360TrainingScraping;
use App\Test\ScrapingTestCase;
use stdClass;


class ImportNoteVoteTest extends ScrapingTestCase
{
    /**
     * @var ImportNoteVote
     */
    protected $importNoteVote;
    protected $scraping;
    protected $app;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->importNoteVote = $this->app['import.notevote'];

        // scraping
        $sourceId1 = rand(0, 100);
        $sourceId2 = rand(0, 100);
        $sourceId3 = rand(0, 100);
        $this->scraping = new stdClass();
        $this->scraping->data['data']['reviews'] = [
            [
                'id' => $sourceId1,
                'author_name' => 'Peter',
                'title' => 'note title 1',
                'note' => 'note body 1',
                'date' => time(),
                'rating' => 5,
            ],
            [
                'id' => $sourceId2,
                'author_name' => 'Mary',
                'title' => 'note title 2',
                'note' => 'note body 2',
                'date' => time(),
                'rating' => 3,
            ],
            [
                'id' => $sourceId3,
                'author_name' => 'Nick',
                'title' => 'note title 3',
                'note' => 'note body 3',
                'date' => time(),
                'rating' => 4,
            ]
        ];
    }

    public function testSupportedType()
    {
        $types = $this->importNoteVote->supportedTypes();
        $this->assertArraySubset(['note', 'vote'], $types);
    }

    public function testGetNoteScraping()
    {
        $sourceId2 = $this->scraping->data['data']['reviews'][1]['id'];
        $date2 = $this->scraping->data['data']['reviews'][1]['date'];
        $review = $this->importNoteVote->getNoteScraping($this->scraping, $sourceId2);

        $this->assertArraySubset([
            'id' => $sourceId2,
            'author_name' => 'Mary',
            'title' => 'note title 2',
            'note' => 'note body 2',
            'date' => $date2,
            'rating' => 3,
        ], $review);
    }

    public function testPush()
    {
        $scrapingData = $this->getMockBuilder(get_class($this->app['scraping.data']))
            ->disableOriginalConstructor()
            ->getMock();

        $go1ScrapingCtrl = $this->getMockBuilder(get_class($this->app['go1.scraping.ctrl']))
            ->disableOriginalConstructor()
            ->setMethods(['getMapping'])
            ->getMock();

        $provider = $this->getMockBuilder(Course360TrainingScraping::class)
            ->disableOriginalConstructor()
            ->setMethods(['hasNoteId'])
            ->getMock();

        // case $provider->hasNoteId() = false
        $importNoteVote = $this->getMockBuilder(get_class($this->app['import.notevote']))
            ->setConstructorArgs([
                $this->app['go1.note.ctrl'],
                $this->app['go1.vote.ctrl'],
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $scrapingData,
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['getData', 'importNoteNoId'])
            ->getMock();

        $item = (object)['course_id' => rand(0, 100)];
        $go1ScrapingCtrl
            ->expects($this->once())
            ->method('getMapping')
            ->willReturn((object) ['remote_id' => rand(0, 100)]);
        $provider->expects($this->once())->method('hasNoteId')->willReturn(false);
        $importNoteVote->expects($this->once())->method('importNoteNoId');
        $importNoteVote->push($provider, $item);

        // case $provider->hasNoteId() = true && data->type = note
        $go1ScrapingCtrl = $this->getMockBuilder(get_class($this->app['go1.scraping.ctrl']))
                                ->disableOriginalConstructor()
                                ->setMethods(['getMapping', 'hasNoteId'])
                                ->getMock();
        $go1ScrapingCtrl
            ->expects($this->atLeast(2))
            ->method('getMapping')
            ->willReturn((object) ['remote_id' => rand(0, 100)]);

        $provider = $this->getMockBuilder(Course360TrainingScraping::class)
                         ->disableOriginalConstructor()
                         ->setMethods(['hasNoteId'])
                         ->getMock();
        $provider->expects($this->once())->method('hasNoteId')->willReturn(true);

        $importNoteVote = $this->getMockBuilder(get_class($this->app['import.notevote']))
            ->setConstructorArgs([
                $this->app['go1.note.ctrl'],
                $this->app['go1.vote.ctrl'],
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $scrapingData,
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['getData', 'getNoteScraping', 'importNoteId'])
            ->getMock();

        $item = (object) ['course_id' => rand(0, 100), 'type' => 'note', 'source_id' => rand(0, 100), 'action' => 'insert'];
        $provider->expects($this->once())->method('hasNoteId')->willReturn(true);
        $importNoteVote->expects($this->once())->method('getNoteScraping');
        $importNoteVote->expects($this->once())->method('importNoteId');
        $importNoteVote->push($provider, $item);

        // case $provider->hasNoteId() = true && data->type = vote
        $provider = $this->getMockBuilder(Course360TrainingScraping::class)
                         ->disableOriginalConstructor()
                         ->setMethods(['hasNoteId'])
                         ->getMock();
        $provider->expects($this->once())->method('hasNoteId')->willReturn(true);
        $importNoteVote = $this->getMockBuilder(get_class($this->app['import.notevote']))
            ->setConstructorArgs([
                $this->app['go1.note.ctrl'],
                $this->app['go1.vote.ctrl'],
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $scrapingData,
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['getData', 'getNoteScraping', 'importVoteId'])
            ->getMock();

        $item = (object) ['course_id' => rand(0, 100), 'type' => 'vote', 'source_id' => rand(0, 100), 'action' => 'insert'];
        $provider->expects($this->once())->method('hasNoteId')->willReturn(true);
        $importNoteVote->expects($this->once())->method('getNoteScraping');
        $importNoteVote->expects($this->once())->method('importVoteId');
        $importNoteVote->push($provider, $item);
    }

    public function testImportNoteNoId()
    {
        $go1ScrapingCtrl = $this->getMockBuilder(get_class($this->app['go1.scraping.ctrl']))
            ->disableOriginalConstructor()
            ->getMock();

        $go1NoteCtrl = $this->getMockBuilder(get_class($this->app['go1.note.ctrl']))
            ->disableOriginalConstructor()
            ->setMethods(['deleteNoteByLoId', 'createNote'])
            ->getMock();

        $go1VoteCtrl = $this->getMockBuilder(get_class($this->app['go1.vote.ctrl']))
            ->disableOriginalConstructor()
            ->setMethods(['deleteVoteByLoId', 'createVote'])
            ->getMock();

        $importNoteVote = $this->getMockBuilder(get_class($this->importNoteVote))
            ->setConstructorArgs([
                $go1NoteCtrl,
                $go1VoteCtrl,
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $this->app['scraping.data'],
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['prepareCreateNote', 'prepareCreateVote'])
            ->getMock();

        // Scraping
        $loId = rand(0, 100);
        $providerName = 'course360training';

        $go1NoteCtrl
            ->expects($this->once())
            ->method('deleteNoteByLoId')
            ->with($loId, $providerName)
            ->willReturn(null);

        $go1VoteCtrl->expects($this->once())
            ->method('deleteVoteByLoId')
            ->with($loId, $providerName)
            ->willReturn(null);

        $importNoteVote->expects($this->atLeast(3))->method('prepareCreateNote');
        $importNoteVote->expects($this->atLeast(3))->method('prepareCreateVote');
        $go1NoteCtrl
            ->expects($this->atLeast(3))
            ->method('createNote');
        $go1VoteCtrl
            ->expects($this->atLeast(3))
            ->method('createVote');

        $importNoteVote->importNoteNoId($this->scraping, $loId, $providerName);
    }

    public function testImportNoteId()
    {
        $go1ScrapingCtrl = $this->getMockBuilder(get_class($this->app['go1.scraping.ctrl']))
            ->disableOriginalConstructor()
            ->getMock();

        $go1NoteCtrl = $this->getMockBuilder(get_class($this->app['go1.note.ctrl']))
            ->disableOriginalConstructor()
            ->setMethods(['createNote', 'updateNote'])
            ->getMock();

        $noteScraping = $this->scraping->data['data']['reviews'][1];
        $remoteId = rand(0, 100);
        $loId = rand(0, 100);
        $providerName = 'course360training';
        $action = 'insert';
        $go1NoteCtrl->expects($this->once())->method('createNote');

        $importNoteVote = $this->getMockBuilder(get_class($this->importNoteVote))
            ->setConstructorArgs([
                $go1NoteCtrl,
                $this->app['go1.vote.ctrl'],
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $this->app['scraping.data'],
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['prepareCreateNote', 'prepareCreateVote'])
            ->getMock();

        $importNoteVote->importNoteId($noteScraping, $remoteId, $loId, $providerName, $action);

        $action = 'update';
        $go1NoteCtrl->expects($this->once())->method('updateNote');
        $importNoteVote->importNoteId($noteScraping, $remoteId, $loId, $providerName, $action);
    }

    public function testImportVoteId()
    {
        $go1ScrapingCtrl = $this->getMockBuilder(get_class($this->app['go1.scraping.ctrl']))
            ->disableOriginalConstructor()
            ->getMock();

        $go1VoteCtrl = $this->getMockBuilder(get_class($this->app['go1.vote.ctrl']))
            ->disableOriginalConstructor()
            ->setMethods(['createVote', 'updateVote'])
            ->getMock();

        $noteScraping = $this->scraping->data['data']['reviews'][1];
        $remoteId = rand(0, 100);
        $loId = rand(0, 100);
        $providerName = 'course360training';
        $action = 'insert';
        $go1VoteCtrl->expects($this->once())->method('createVote');

        $importNoteVote = $this->getMockBuilder(get_class($this->importNoteVote))
            ->setConstructorArgs([
                $this->app['go1.note.ctrl'],
                $go1VoteCtrl,
                $go1ScrapingCtrl,
                $this->app['scraping.ctrl'],
                $this->app['scraping.data'],
                $this->app['config.ctrl'],
                $this->app['logger.ctrl']
            ])
            ->setMethods(['prepareCreateNote', 'prepareCreateVote'])
            ->getMock();

        $importNoteVote->importVoteId($noteScraping, $remoteId, $loId, $providerName, $action);

        $action = 'update';
        $go1VoteCtrl->expects($this->once())->method('updateVote');
        $importNoteVote->importVoteId($noteScraping, $remoteId, $loId, $providerName, $action);
    }

    public function testPrepareCreateNote()
    {
        $noteScraping = $this->scraping->data['data']['reviews'][0];
        $loId = rand(0, 100);
        $note = $this->importNoteVote->prepareCreateNote($noteScraping, $loId);
        $this->assertArraySubset([
            'loId' => $loId,
            'author' => $noteScraping['author_name'],
            'date' => $noteScraping['date'],
            'title' => $noteScraping['title'],
            'content' => $noteScraping['note']
        ], $note);
    }

    public function testPrepareUpdateNote()
    {
        $noteScraping = $this->scraping->data['data']['reviews'][0];
        $note = $this->importNoteVote->prepareUpdateNote($noteScraping);
        $this->assertArraySubset([
            'author' => $noteScraping['author_name'],
            'date' => $noteScraping['date'],
            'title' => $noteScraping['title'],
            'content' => $noteScraping['note']
        ], $note);
    }

    public function testPrepareCreateVote()
    {
        $noteScraping = $this->scraping->data['data']['reviews'][0];
        $loId = rand(0, 100);
        $vote = $this->importNoteVote->prepareCreateVote($noteScraping, $loId);
        $this->assertArraySubset([
            'value' => $noteScraping['rating'],
            'entity_type' => 'lo',
            'profile_id' => 0,
            'entity_id' => $loId
        ], $vote);
    }

    public function testPrepareUpdateVote()
    {
        $noteScraping = $this->scraping->data['data']['reviews'][0];
        $vote = $this->importNoteVote->prepareUpdateVote($noteScraping);
        $this->assertArraySubset([
            'value' => $noteScraping['rating'],
        ], $vote);
    }
}