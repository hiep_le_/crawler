<?php

namespace App\Test\Scraping;

use App\Scraping\ScrapingData;
use App\Test\ScrapingTestCase;

class ScrapingDataTest extends ScrapingTestCase
{
    protected $app;
    /** @var ScrapingData */
    protected $scrapingData;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->scrapingData = $this->app['scraping.data'];
    }

    public function testSaveScrapingData()
    {
        $data = [
            'source_id' => rand(0, 100),
            'title' => 'foo',
            'type' => 'portal',
            'source_url' => 'something'
        ];
        $dataProvideName = 'allconferences';

        $this->scrapingData->saveScrapingData($data, $dataProvideName);
        $scraping = $this->scrapingData->getScrapingDataBySourceId($data['source_id'], $dataProvideName, $data['type']);
        $this->assertNotNull($scraping);
    }

    public function testGetScrapingRecords()
    {
        $data1 = [
            'source_id' => rand(0, 100),
            'title' => 'foo',
            'type' => 'portal',
            'source_url' => 'something',
            'portal_logo' => 'http://content.allconferences.com/images/conference/logos/big/image_103814.jpg',
        ];
        $dataProvideName = 'allconferences';
        $this->scrapingData->saveScrapingData($data1, $dataProvideName);

        $data2 = [
            'source_id' => rand(0, 100),
            'title' => 'foo',
            'type' => 'portal',
            'source_url' => 'something',
            'portal_logo' => 'http://content.allconferences.com/images/conference/logos/big/image_103814.jpg',
        ];
        $dataProvideName = 'allconferences';
        $this->scrapingData->saveScrapingData($data2, $dataProvideName);

        $records = $this->scrapingData->getScrapingRecords(['portal'], $dataProvideName, 2);
        $this->assertCount(2, $records);

        $this->assertEquals((object)[
            'scraping_data_id' => '1',
            'provider' => 'allconferences',
            'type' => 'portal',
            'source_id' => (string) $data1['source_id'],
            'title' => 'foo',
            'source_url' => 'something',
            'data' => [
                'title' => 'foo',
                'type' => 'portal',
                'portal_logo' => 'http://content.allconferences.com/images/conference/logos/big/image_103814.jpg',
            ],
            'queued_import' => '1'
        ], $records[1]);
    }

}
