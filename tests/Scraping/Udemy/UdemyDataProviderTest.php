<?php

namespace App\Test\Scraping\Udemy;

use App\Scraping\Course\Udemy\UdemyScraping;
use App\Scraping\ScrapingDataService;
use App\Test\ScrapingTestCase;

class UdemyScrapingTest extends ScrapingTestCase
{
    private $app;
    /** @var UdemyScraping */
    protected $udemyScraping;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->udemyScraping = $this->app['scraping.udemy'];
    }

    public function testGetName()
    {
        $this->assertEquals('udemy', $this->udemyScraping->getName());
    }

//    /**
//     * @Scraping getCourseListProvider
//     */
//    public function testGetCourseList($dbPage, $currentPage, $nextPage)
//    {
//        $app = $this->getApp();
//
//        // Mock state record
//        $stateRecord = new \stdClass();
//        $stateRecord->data = ['page' => $dbPage];
//
//        // Mock scraping data service
//        $scrapingDataService = $this->getMockBuilder(ScrapingDataService::class)
//                                    ->setConstructorArgs([$app])
//                                    ->getMock();
//        $scrapingDataService->method('getScrapingState')
//                            ->with($this->equalTo('udemy'))
//                            ->willReturn($stateRecord);
//
//        // Stub application container
//        $app->container = [
//            'data.scraping' => $scrapingDataService,
//        ];
//
//        $mock = $this->getMockBuilder(UdemyScraping::class)
//                     ->setConstructorArgs([$this->getApp($app)])
//                     ->setMethods(['getCourses'])
//                     ->getMock();
//
//        $mock->expects($this->once())
//             ->method('getCourses')
//             ->with($this->equalTo($currentPage))
//             ->willReturn(new \ArrayIterator());
//
//        // Test page increment
//        $mock->getList();
//        $this->assertEquals($nextPage, $mock->getStateData()['page']);
//    }
//
//    public function getCourseListProvider()
//    {
//        return [
//            [null, 1, 2],
//            [0, 1, 2],
//            [9, 9, 10],
//        ];
//    }
//
//    /**
//     * @group getCourseGroup
//     */
//    public function testGetCourses()
//    {
//        $this->markTestSkipped();
//        $this->invokeMethod($this->udemyScraping, 'getClientAccess');
//
//        // Get 3 courses for testing
//        $courses = $this->udemyScraping->getCourses(1, 3);
//
//        $this->assertInternalType('array', $courses);
//        $this->assertCount(3, $courses);
//
//        return $courses;
//    }
//
//    /**
//     * @depends testGetCourses
//     * @group   getCourseGroup
//     */
//    public function testGetDataDetail($courses)
//    {
//        $this->markTestSkipped();
//        foreach ($courses as $course) {
//            $courseDetail = $this->udemyScraping->getDataDetail($course);
//            $this->assertInternalType('array', $courseDetail, 'Course detail is not an Array');
//            $this->assertArraySubset([
//                'marketplace' => 1,
//                'published'   => 1,
//                'type'        => 'course',
//            ], $courseDetail);
//            $this->assertArrayHasKey('source_id', $courseDetail, 'Missing course source ID.');
//            $this->assertArrayHasKey('source_url', $courseDetail, 'Missing course source URL.');
//            $this->assertArrayHasKey('title', $courseDetail, 'Missing course title.');
//            $this->assertArrayHasKey('data', $courseDetail, 'Missing course data.');
//            $this->assertArrayHasKey('course_url', $courseDetail['data'], 'Missing course data url.');
//            $this->assertArrayHasKey('course_id', $courseDetail['data'], 'Missing course data id.');
//            $this->assertArrayHasKey('image_url', $courseDetail['data']);
//            $this->assertArrayHasKey('tags', $courseDetail);
//            $this->assertArrayHasKey('language', $courseDetail);
//            $this->assertTrue(isset($courseDetail['data']['allow_enrolment']) && $courseDetail['data']['allow_enrolment'] == 'enquiry');
//        }
//    }
//
//    /**
//     * @depends testGetCourses
//     * @group   getCourseGroup
//     */
//    public function testGetImageUrl($courses)
//    {
//        $this->markTestSkipped();
//        foreach ($courses as $course) {
//            $imageUrl = $this->udemyScraping->getImageUrl($course);
//            $this->assertTrue(filter_var($imageUrl, FILTER_VALIDATE_URL) !== false);
//        }
//    }
}
