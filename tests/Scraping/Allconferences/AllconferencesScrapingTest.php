<?php

namespace App\Test\Scraping\Allconferences;

use App\Browser\Guzzle;
use App\Scraping\Course\Allconferences\AllconferencesScraping;
use App\Scraping\ScrapingFileUpload;
use App\Test\ScrapingTestCase;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;

class AllconferencesScrapingTest extends ScrapingTestCase
{
    protected $app;
    /** @var AllconferencesScraping */
    protected $allconferencesScraping;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->allconferencesScraping = $this->app['scraping.allconferences'];
    }

    public function testGetName()
    {
        $this->assertEquals('allconferences', $this->allconferencesScraping->getName());
    }

    public function testGetCoursePerPage()
    {
        parent::setUp();
        // page return content
        $this->app = $this->getApp();
        $content = file_get_contents(__DIR__ . '/.tmp/conferences.html');
        $this->app->extend('browser.guzzle', function () use ($content) {
            return $this->getClientOnCourses($content);
        });
        $this->allconferencesScraping = $this->app['scraping.allconferences'];
        $courses = $this->allconferencesScraping->getCoursePerPage(2);
        $this->assertCount(20, $courses);
        $this->assertNotEmpty($courses);
        // page not found
        parent::setUp();
        $content = file_get_contents(__DIR__.'/.tmp/conferences_empty.html');
        $this->app = $this->getApp();
        $this->app->extend('browser.guzzle', function () use ($content) {
            return $this->getClientOnCoursesEmpty($content);
        });
        $this->allconferencesScraping = $this->app['scraping.allconferences'];
        $course = $this->allconferencesScraping->getCoursePerPage(100000);
        $this->assertEmpty($course);
    }

    public function testGetList()
    {
        $courses = [];
        for($i = 0; $i < 20; $i++) {
            $courses[] = ['something'];
        }

        $allConferences = $this->getMockBuilder(AllconferencesScraping::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCoursePerPage'])
            ->getMock();
        $allConferences->expects($this->any())
            ->method('getCoursePerPage')
            ->willReturn($courses);

        $results = $allConferences->getList(1);
        $this->assertCount(1, $results);
        $results = $allConferences->getList(30);
        $this->assertCount(30, $results);
    }

    public function testFilterId()
    {
        $id = $this->allconferencesScraping->filterId('conference_103657');
        $this->assertEquals('103657', $id);
    }

    public function testGetPortal()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->app->extend('scraping.file_upload', function () {
            return $this->getScrapingFileUpload();
        });

        $content = file_get_contents(__DIR__ . '/.tmp/conference_detail.html');
        $this->app->extend('browser.guzzle', function () use ($content) {
            return $this->getClientOnCourseDetail($content);
        });

        $this->allconferencesScraping = $this->app['scraping.allconferences'];
        $row['url'] = 'http://webcache.googleusercontent.com/search?q=cache:www.allconferences.com/c/12th-international-project-management-conference-tehran-2017-february-12';
        $row['source_id'] = rand(0, 100);
        $prop = $this->allconferencesScraping->getPortal($row);
        $this->assertArrayHasKey('portal_logo', $prop);
        $this->assertArrayHasKey('title', $prop);
        $this->assertEquals($row['url'], $prop['source_url']);
        $this->assertEquals($row['source_id'], $prop['source_id']);
    }

    public function getScrapingFileUpload() {
        $fileUpload = $this->getMockBuilder(ScrapingFileUpload::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBySourceUrl'])
            ->getMock();
        $fileUpload->expects($this->once())
            ->method('getBySourceUrl')
            ->willReturn(true);
        return $fileUpload;
    }

    public function testGetCourseDetail()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $content = file_get_contents(__DIR__ . '/.tmp/conference_detail.html');
        $this->app->extend('browser.guzzle', function () use ($content) {
            return $this->getClientOnCourseDetail($content);
        });
        $row['url'] = 'http://webcache.googleusercontent.com/search?q=cache:www.allconferences.com/c/pediatric-emergency-medicine-emergent-and-urgent-challenges-sarasota-2016-december-12';
        $this->allconferencesScraping = $this->app['scraping.allconferences'];
        $prop = $this->allconferencesScraping->getCourseDetail($row);
        $this->assertArrayHasKey('name', $prop);
        $this->assertArrayHasKey('image_url', $prop);
        $this->assertArrayHasKey('source_url', $prop);
        $this->assertArrayHasKey('description', $prop);
        $this->assertArrayHasKey('event', $prop);
        $this->assertArrayHasKey('external_url', $prop);
        $this->assertArrayHasKey('tags', $prop);
    }

    public function testGetEventOnCourse() {
        $content = file_get_contents(__DIR__ . '/.tmp/event_on_course.html');
        $crawler = new Crawler();
        $crawler->addContent($content);
        $allConferences = $this->getMockBuilder(AllconferencesScraping::class)
            ->disableOriginalConstructor()
            ->setMethods(['getDateTimeOnCourse', 'getLocationOnCourse'])
            ->getMock();

        $allConferences->expects($this->once())->method('getDateTimeOnCourse')
            ->with('Feb 12, 2017 / 8:00 am - (GMT +3:30 hours)',
                'Feb 13, 2017 / 5:30 pm - (GMT +3:30 hours)');
        $allConferences->expects($this->once())->method('getLocationOnCourse');
        $allConferences->getEventOnCourse($crawler);
    }

    public function testGetDateTimeOnCourse() {
        $begin_time = 'Dec 12, 2016 / 8:00 am - (EST)';
        $end_time = 'Dec 16, 2016 / 12:15 pm - (EST)';
        $dateTime = $this->allconferencesScraping->getDateTimeOnCourse($begin_time, $end_time);
        $this->assertEquals([
            'start' => '2016-12-12T13:00:00+00:00',
            'end' => '2016-12-16T17:15:00+00:00',
        ], $dateTime);
    }

    public function testIsEventPast() {
        $item['event']['start'] = '2016-12-12T13:00:00+00:00';
        $item['event']['end'] = '2019-12-15T17:15:00+00:00';
        $result = $this->allconferencesScraping->isEventPast($item['event']);
        $this->assertFalse($result);

        $item['event']['start'] = '2016-12-12T13:00:00+00:00';
        $item['event']['end'] = '2016-12-15T17:15:00+00:00';
        $result = $this->allconferencesScraping->isEventPast($item['event']);
        $this->assertTrue($result);
    }

    public function testGetDateTime()
    {
        // May 08, 2017 / 9:00 am - (GMT +8:00 hours)
        // Mar 06, 2017 / 8:30 am - (GMT +1:00 hours)
        // Dec 12, 2016 / 8:00 am - (EST)
        $time = 'Mar 06, 2017 / 8:30 am - (GMT +1:00 hours)';
        $dateTime = $this->allconferencesScraping->getDateTime($time);
        $this->assertEquals('2017-03-06T07:30:00+00:00', $dateTime);

        $time = 'Mar 06, 2017 / 8:35 am - (GMT +8:00 hours)';
        $dateTime = $this->allconferencesScraping->getDateTime($time);
        $this->assertEquals('2017-03-06T00:35:00+00:00', $dateTime);

        $time = 'Mar 06, 2017 / 8:35 am - (EST)';
        $dateTime = $this->allconferencesScraping->getDateTime($time);
        $this->assertEquals('2017-03-06T13:35:00+00:00', $dateTime);

        $time = 'Mar 06, 2017';
        $dateTime = $this->allconferencesScraping->getDateTime($time);
        $this->assertEquals('2017-03-06T00:00:00+00:00', $dateTime);
    }

    public function testGetConferenceWebsite() {
        $content = "<button class=\"creme-button eventWebsite\" type=\"button\" onclick=\"javascript:  updateCounter('104371','external_website_counter'); window.open('http://www.conferences-scientific.cz/inpage/conference-bratislava-iacmem-2017/') ;return false;\"><img src=\"/img/view_event_icon.png\" alt=\"\">Conference Web Site</button>";
        $crawler = new Crawler();
        $crawler->addContent($content);
        $link = $this->allconferencesScraping->getConferenceWebsite($crawler->filter('.eventWebsite'));
        $expect = 'http://www.conferences-scientific.cz/inpage/conference-bratislava-iacmem-2017/';
        $this->assertEquals($expect, $link);
    }

    public function testFormatPortalTitle() {
        $portalTitle = "Simple Title";
        $portalTitle = $this->allconferencesScraping->formatPortalTitle($portalTitle);
        $this->assertEquals("simple-title", $portalTitle);

        $portalTitle = "Simple   Title - With    ";
        $portalTitle = $this->allconferencesScraping->formatPortalTitle($portalTitle);
        $this->assertEquals("simple-title-with", $portalTitle);
    }

    public function testGetLocationOnCourse() {
        $content = file_get_contents(__DIR__ . '/.tmp/location_on_course.html');
        $crawler = new Crawler();
        $crawler->addContent($content);
        $location = $this->allconferencesScraping->getLocationOnCourse($crawler);
        $this->assertArrayHasKey('thoroughfare', $location);
        $this->assertArrayHasKey('locality', $location);
        $this->assertArrayHasKey('country', $location);
    }

    public function testGetConferenceWebsiteTitle() {
        $title = 'http://www.ace-conference.org';
        $result = $this->allconferencesScraping->getConferenceWebsiteTitle($title);
        $this->assertEquals('ace-conference.org', $result);

        $title = 'http://ace-conference.org';
        $result = $this->allconferencesScraping->getConferenceWebsiteTitle($title);
        $this->assertEquals('ace-conference.org', $result);

        $title = 'ace-conference.org';
        $result = $this->allconferencesScraping->getConferenceWebsiteTitle($title);
        $this->assertEquals('ace-conference.org', $result);
    }

    public function getClientOnCourseDetail($content) {
        $client = $this->getMockBuilder(Guzzle::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $client->expects($this->once())
            ->method('get')
            ->willReturnCallback(function() use ($content) {
                return new Response(200, [], $content);
            });
        return $client;
    }

    public function getClientOnCourses($content) {
        $client = $this->getMockBuilder(Guzzle::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $client->expects($this->once())
            ->method('get')
            ->willReturnCallback(function() use ($content) {
                return new Response(200, [], $content);
            });
        return $client;
    }

    public function getClientOnCoursesEmpty($content) {
        $client = $this->getMockBuilder(Guzzle::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $client->expects($this->once())
            ->method('get')
            ->willReturnCallback(function() use ($content) {
                return new Response(200, [], $content);
            });
        return $client;
    }

}
