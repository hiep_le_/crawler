<?php

namespace App\Test\Scraping\Lynda;

use App\Scraping\Course\Lynda\LyndaScraping;
use App\Scraping\ScrapingDataService;
use App\Test\ScrapingTestCase;

class LyndaScrapingTest extends ScrapingTestCase
{
    private $app;
    /** @var LyndaScraping */
    protected $lyndaScraping;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->lyndaScraping = $this->app['scraping.lynda'];
    }

    public function testGetName()
    {
        $this->assertEquals('lynda', $this->lyndaScraping->getName());
    }

//    /**
//     * @group getCourseGroup
//     */
//    public function testGetCourseListIterator()
//    {
//        $courseListIterator = $this->lyndaScraping->getCourseListIterator();
//
//        $this->assertInstanceOf(\ArrayIterator::class, $courseListIterator);
//        $this->assertCount(50, $courseListIterator);
//
//        // Filter out courses to reduce test time
//        $returnIterator = new \ArrayIterator();
//        foreach ($courseListIterator as $key => $courseEntry) {
//            if ($key === 2) {
//                break;
//            }
//            $returnIterator->append($courseEntry);
//        }
//
//        return $returnIterator;
//    }
//
//    /**
//     * @depends testGetCourseListIterator
//     * @group   getCourseGroup
//     * @return array Courses HTML content
//     */
//    public function testGetCoursesContent($courseListIterator)
//    {
//        $coursesContent = $this->lyndaScraping->getCoursesContent($courseListIterator);
//
//        $this->assertInternalType('array', $coursesContent);
//        $this->assertEquals(count($courseListIterator), count($coursesContent));
//
//        return $coursesContent;
//    }
//
//    /**
//     * @depends testGetCoursesContent
//     * @group   getCourseGroup
//     * @param string[] $coursesContent Courses HTML content
//     */
//    public function testGetDataDetail($coursesContent)
//    {
//        foreach ($coursesContent as $courseContent) {
//            $courseDetail = $this->lyndaScraping->getDataDetail($courseContent);
//            $this->assertInternalType('array', $courseDetail, 'Course detail is not an Array');
//            $this->assertArraySubset([
//                'marketplace' => 1,
//                'published'   => 1,
//                'type'        => 'course',
//            ], $courseDetail);
//            $this->assertArrayHasKey('source_id', $courseDetail, 'Missing course source ID.');
//            $this->assertArrayHasKey('source_url', $courseDetail, 'Missing course source URL.');
//            $this->assertArrayHasKey('title', $courseDetail, 'Missing course title.');
//            $this->assertArrayHasKey('data', $courseDetail, 'Missing course data.');
//            $this->assertArrayHasKey('course_url', $courseDetail['data'], 'Missing course data url.');
//            $this->assertArrayHasKey('course_id', $courseDetail['data'], 'Missing course data id.');
//        }
//    }
//
//    /**
//     * @Scraping courseDetailProvider
//     * @param $courseData
//     */
//    public function testValidateCourseDetail($courseData)
//    {
//        $this->assertNull($this->invokeMethod($this->lyndaScraping, 'validateCourseDetail', [$courseData]));
//    }
//
//    public function courseDetailProvider()
//    {
//        return [
//            [['source_id' => 1, 'source_url' => 'foo', 'title' => 'bar']],
//            [['source_id' => 1, 'source_url' => 'foo', 'title' => 'bar', 'foo' => 'bar']],
//        ];
//    }
//
//    /**
//     * @Scraping                       courseDetailErrorProvider
//     * @expectedException \Exception
//     * @expectedExceptionMessageRegExp /Course detail missing mandatory properties "[^"]*."/
//     * @param $courseData
//     */
//    public function testValidateCourseDetailHasError($courseData)
//    {
//        $this->invokeMethod($this->lyndaScraping, 'validateCourseDetail', [$courseData]);
//    }
//
//    public function courseDetailErrorProvider()
//    {
//        return [
//            [['source_id' => null, 'source_url' => null, 'title' => null]],
//            [['source_id' => 1, 'source_url' => 'foo', 'title' => '']],
//            [['source_id' => 0, 'source_url' => 'foo', 'title' => '']],
//            [['source_id' => 1, 'source_url' => null, 'title' => '']],
//        ];
//    }
//
//    /**
//     * @Scraping mappingTagProvider
//     */
//    public function testGetMappingTag($tag, $expected)
//    {
//        $this->assertEquals($this->lyndaScraping->getMappingTag($tag), $expected);
//    }
//
//    public function mappingTagProvider()
//    {
//        return [
//            ['foo', ['foo']],
//            ['FOO', ['FOO']],
//            ['Foo', ['Foo']],
//            ['Design', ['Business', 'Technology', 'Design']],
//            ['DESIGN', ['Business', 'Technology', 'Design']],
//            ['design', ['Business', 'Technology', 'Design']],
//            ['Video', ['Technology', 'Business', 'Development', 'Video']],
//            ['video2brain', ['Business', 'Industry Specific', 'video2brain']],
//        ];
//    }
//
//    /**
//     * @Scraping getCourseListProvider
//     */
//    public function testGetCourseList($dbPage, $currentPage, $nextPage)
//    {
//        $app = $this->getApp();
//
//        // Mock state record
//        $stateRecord = new \stdClass();
//        $stateRecord->data = ['page' => $dbPage];
//
//        // Mock scraping data service
//        $scrapingDataService = $this->getMockBuilder(ScrapingDataService::class)
//                                    ->setConstructorArgs([$app])
//                                    ->getMock();
//        $scrapingDataService->method('getScrapingState')
//                            ->with($this->equalTo('lynda'))
//                            ->willReturn($stateRecord);
//
//        // Stub application container
//        $app->container = [
//            'data.scraping' => $scrapingDataService,
//        ];
//
//        $mock = $this->getMockBuilder(LyndaScraping::class)
//                     ->setConstructorArgs([$app])
//                     ->setMethods(['getCourseListIterator', 'getCoursesContent'])
//                     ->getMock();
//
//        $mock->expects($this->once())
//             ->method('getCourseListIterator')
//             ->with($this->equalTo($currentPage))
//             ->willReturn(new \ArrayIterator());
//
//        $mock->expects($this->once())
//             ->method('getCoursesContent')
//             ->willReturn([]);
//
//        // Test page increment
//        $mock->getList();
//        $this->assertEquals($nextPage, $mock->getStateData()['page']);
//    }
//
//    public function getCourseListProvider()
//    {
//        return [
//            [null, 1, 2],
//            [0, 1, 2],
//            [9, 9, 10],
//        ];
//    }
//
//    /**
//     * @Scraping getAuthorAvatarUrlProvider
//     */
//    public function testGetAuthorAvatarUrl($authorId, $expectAvarUrl)
//    {
//        $avarUrl = $this->lyndaScraping->getAuthorAvatarUrl($authorId);
//        $this->assertEquals($expectAvarUrl, $avarUrl);
//    }
//
//    public function getAuthorAvatarUrlProvider()
//    {
//        return [
//            [2932394, 'https://cdn.lynda.com/authors/2932394_200x200_thumb.jpg'],
//            [2932395, false],
//        ];
//    }
}
