<?php

namespace App\Test\Scraping;

use App\Scraping\ScrapingData;
use App\Scraping\ScrapingFileUpload;
use App\Test\ScrapingTestCase;

class ScrapingFileUploadTest extends ScrapingTestCase
{
    protected $app;
    /** @var ScrapingFileUpload */
    protected $fileUpload;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->fileUpload = $this->app['scraping.file_upload'];
    }

    /**
     * Test insert
     * Test getBySourceUrl
     */
    public function testInsert()
    {
        $data = [
            'source_url' => 'http://content.allconferences.com/images/conference/logos/big/image_103657.jpg',
            'remote_url' => 'https://res.cloudinary.com/go1vn/image/upload/v1486479248/fk5gvehuemn7fink9asv.jpg',
            'provider'  => 'allconferences'
        ];

        $this->fileUpload->insert($data);
        $file = $this->fileUpload->getBySourceUrl($data['source_url']);
        $this->assertNotNull($file);
    }

    public function testUpdate()
    {
        $data = [
            'source_url' => 'http://content.allconferences.com/images/conference/logos/big/image_103657.jpg',
            'remote_url' => 'https://res.cloudinary.com/go1vn/image/upload/v1486479248/fk5gvehuemn7fink9asv.jpg',
            'provider'  => 'allconferences'
        ];

        $this->fileUpload->insert($data);
        $file = $this->fileUpload->getBySourceUrl($data['source_url']);
        $remoteUrl = 'something';
        $this->fileUpload->update($file->id, $remoteUrl);
        $file = $this->fileUpload->getBySourceUrl($data['source_url']);
        $this->assertEquals('something', $file->remote_url);
    }
}
