<?php

namespace App\Test\Scraping\Udacity;

use App\Application;
use App\Scraping\Course\Udacity\UdacityScraping;
use App\Scraping\ScrapingDataService;
use App\Test\ScrapingTestCase;

class UdacityScrapingTest extends ScrapingTestCase
{
    private $app;
    /** @var UdacityScraping */
    protected $udacityScraping;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->udacityScraping = $this->app['scraping.udacity'];
    }

    public function testGetName()
    {
        $this->assertEquals('udacity', $this->udacityScraping->getName());
    }

//    /**
//     * @Scraping getCourseListProvider
//     */
//    public function testGetCourseList($dbPage, $nextPage, $callCount)
//    {
//        $app = $this->getApp();
//
//        // Mock state record
//        $stateRecord = new \stdClass();
//        $stateRecord->data = ['page' => $dbPage];
//
//        // Mock scraping data service
//        $scrapingDataService = $this->getMockBuilder(ScrapingDataService::class)
//                                    ->setConstructorArgs([$app])
//                                    ->getMock();
//        $scrapingDataService->method('getScrapingState')
//                            ->with($this->equalTo('udacity'))
//                            ->willReturn($stateRecord);
//
//        // Stub application container
//        $app->container = [
//            'data.scraping' => $scrapingDataService,
//        ];
//
//        $mock = $this->getMockBuilder(UdacityScraping::class)
//                     ->setConstructorArgs([$app])
//                     ->setMethods(['getCourses'])
//                     ->getMock();
//
//        $mock->expects($this->exactly($callCount))
//             ->method('getCourses')
//             ->willReturn([]);
//
//        // Test page increment
//        $courses = $mock->getList();
//        $this->assertEquals($nextPage, $mock->getStateData()['page']);
//
//        return array_slice($courses, 0, 5);
//    }
//
//    public function getCourseListProvider()
//    {
//        return [
//            [null, 2, 1],
//            [0, 2, 1],
//            [9, 9, 0],
//        ];
//    }
//
//    /**
//     * @group getCourseGroup
//     */
//    public function testGetCourses()
//    {
//        $courses = $this->udacityScraping->getCourses(UdacityScraping::COURSE_TYPE_COURSE);
//
//        $this->assertInternalType('array', $courses);
//        $this->assertTrue(count($courses) > 0);
//
//        $degrees = $this->udacityScraping->getCourses(UdacityScraping::COURSE_TYPE_DEGREE);
//
//        $this->assertInternalType('array', $degrees);
//        $this->assertTrue(count($degrees) > 0);
//
//        return array_merge(array_slice($courses, 0, 3), array_slice($degrees, 0, 3));
//    }
//
//    /**
//     * @depends testGetCourses
//     * @group   getCourseGroup
//     */
//    public function testGetDataDetail($courses)
//    {
//        $testedStructure = false;
//        foreach ($courses as $course) {
//            $courseDetail = $this->udacityScraping->getDataDetail($course);
//            if (!$course['available'] || !$course['public_listing']) {
//                $this->assertTrue($courseDetail === Application::SCRAPING_STATUS_CONTINUE);
//                continue;
//            }
//
//            $this->assertInternalType('array', $courseDetail);
//            $this->assertArraySubset([
//                'marketplace' => 1,
//                'published'   => 1,
//                'type'        => 'course',
//            ], $courseDetail);
//            $this->assertArrayHasKey('source_id', $courseDetail, 'Missing course source ID.');
//            $this->assertArrayHasKey('source_url', $courseDetail, 'Missing course source URL.');
//            $this->assertArrayHasKey('title', $courseDetail, 'Missing course title.');
//            $this->assertArrayHasKey('data', $courseDetail, 'Missing course data.');
//            $this->assertArrayHasKey('course_url', $courseDetail['data'], 'Missing course data url.');
//            $this->assertArrayHasKey('course_id', $courseDetail['data'], 'Missing course data id.');
//            $this->assertInternalType('array', $courseDetail['author']);
//            $this->assertTrue(isset($courseDetail['language']) && $courseDetail['language'] == 'en');
//            $this->assertTrue(isset($courseDetail['data']['allow_enrolment']) && in_array($courseDetail['data']['allow_enrolment'], ['subscription', 'enquiry']));
//            $testedStructure = true;
//        }
//
//        // Some cases the course ignored, the data structure can not test
//        $this->assertTrue($testedStructure);
//    }
//
//    /**
//     * @Scraping mappingTagProvider
//     */
//    public function testGetMappingTag($tag, $expected)
//    {
//        $this->assertEquals($this->udacityScraping->getMappingTag($tag), $expected);
//    }
//
//    public function mappingTagProvider()
//    {
//        return [
//            [null, ['Technology']],
//            [[], ['Technology']],
//            ['all', ['Technology']],
//            ['foo', ['foo']],
//            ['FOO', ['FOO']],
//            ['Foo', ['Foo']],
//            ['Android', ['Business', 'Development', 'Android']],
//            ['ANDROID', ['Business', 'Development', 'Android']],
//            ['android', ['Business', 'Development', 'Android']],
//            [['Android', 'Software Engineering'], ['Business', 'Development', 'Android', 'Academic', 'Technology', 'Software Engineering']],
//            [['All', 'Android', 'Software Engineering'], ['Business', 'Development', 'Android', 'Academic', 'Technology', 'Software Engineering']],
//        ];
//    }
}
