<?php

namespace App\Test\Scraping\Course360Training;

use App\Scraping\Course\Course360Training\Course360TrainingScraping;
use App\Test\ScrapingTestCase;
use DateTime;

class Course360TrainingScrapingTest extends ScrapingTestCase
{
    protected $app;
    /** @var Course360TrainingScraping */
    protected $course360TrainingScraping;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
        $this->course360TrainingScraping = $this->app['scraping.course360training'];
    }

    public function testGetName()
    {
        $this->assertEquals('course360training', $this->course360TrainingScraping->getName());
    }

    public function testGetList()
    {
        $courses = $this->course360TrainingScraping->getList(5);
        $this->assertCount(5, $courses);
    }

    public function testGetCourseDetail()
    {
        $row['Remote ID'] = 'Something';
        $row['Tags'] = "tag_a, tag_b";
        // case course has description and image
        $row['Course URL'] = 'http://www.360training.com/business-skills-training/career/building-future/building-your-career-315i';
        $course = $this->course360TrainingScraping->getCourseDetail($row);
        $this->assertArrayHasKey('description', $course);
        $this->assertArrayHasKey('image', $course);
        // course no image
        $row['Course URL'] = 'http://www.360training.com/data-and-analytics/probability-and-statistics/basics-of-statistics-a-comprehensive-study-college-level';
        $course = $this->course360TrainingScraping->getCourseDetail($row);
        $this->assertArrayNotHasKey('image', $course);
        // course no description
        $row['Course URL'] = 'http://www.360training.com/food-beverage-programs/bartending-alcohol-serving-license-certification-online/illinois-alcohol-server-education/illinois-convenience-store-manager-essentials';
        $course = $this->course360TrainingScraping->getCourseDetail($row);
        $this->assertArrayNotHasKey('description', $course);
    }

    public function testGetDataDetail()
    {
        $item = [
            'name' => 'Communication',
            'description' => 'description',
            'source_url' => 'http://www.360training.com/business-skills-training/career/building-future/building-your-career-315i',
            'language' => 'en',
            'id' => rand(0, 100),
            'marketplace' => 1,
            'published' => 1,
            'type' => 'course',
            'price' => 500,
            'image' => 'https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150',
            'reviews_url' => 'something',
        ];

        $course360training = $this->getMockBuilder(get_class($this->app['scraping.course360training']))
            ->disableOriginalConstructor()
            ->setMethods(['getCourseDetail'])
            ->getMock();

        $course360training->expects($this->once())->method('getCourseDetail')->willReturn($item);
        $results = $course360training->getDataDetail('something');

        $this->assertArraySubset([
            'title' => 'Communication',
            'description' => 'description',
            'source_url' => 'http://www.360training.com/business-skills-training/career/building-future/building-your-career-315i',
            'language' => 'en',
            'source_id' => $item['id'],
            'marketplace' => 1,
            'published' => 1,
            'type' => 'course',
            'pricing' => [
                'price' => 500,
                'currency' => 'USD'
            ],
            'data' => [
                'title' => 'Communication',
                'image_url' => 'https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150',
                'course_url' => 'http://www.360training.com/business-skills-training/career/building-future/building-your-career-315i',
                'reviews_url' => 'something',
                'course_id' => $item['id'],
                'allow_enrolment' => 'enquiry',
            ]
        ], $results);
    }

    public function testGetDateTime()
    {
        $string = 'Posted 3 months ago';
        $timestamp = $this->course360TrainingScraping->getDateTime($string);
        $this->assertEquals((new DateTime('-3 months'))->getTimestamp(), $timestamp);
        $string = 'Posted 1 year ago';
        $timestamp = $this->course360TrainingScraping->getDateTime($string);
        $this->assertEquals((new DateTime('-1 year'))->getTimestamp(), $timestamp);
        $string = 'Posted a year and 6 months ago';
        $timestamp = $this->course360TrainingScraping->getDateTime($string);
        $this->assertEquals((new DateTime('-18 months'))->getTimestamp(), $timestamp);
        $string = 'Posted two years and 6 months ago';
        $timestamp = $this->course360TrainingScraping->getDateTime($string);
        $this->assertEquals((new DateTime('-30 months'))->getTimestamp(), $timestamp);
    }
}
