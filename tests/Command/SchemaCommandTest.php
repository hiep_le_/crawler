<?php

namespace App\Test\Command;

use App\Test\ScrapingTestCase;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Tester\CommandTester;

class SchemaCommandTest extends ScrapingTestCase
{
    private $consoleApp;
    private $app;

    public function testExecute()
    {
        $this->app = $this->getApp();
        $this->consoleApp = $this->getConsoleApp();
        $cmd = $this->consoleApp->find('schema');
        $tester = new CommandTester($cmd);
        /** @var Connection $db */
        $db = $this->app['db'];
        $this->assertCount(4, $db->getSchemaManager()->listTableNames());

        // test drop database
        $tester->execute([
            'command' => $cmd->getName(),
            'action' => 'drop',
            '--y' => 1
        ]);
        $this->assertContains('The database schemas has been dropped.', $tester->getDisplay());
    }
}