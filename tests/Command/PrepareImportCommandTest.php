<?php

namespace App\Test\Command;

use App\Test\ScrapingTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PrepareImportCommandTest extends ScrapingTestCase
{
    private $consoleApp;

    public function testExecute()
    {
        $this->consoleApp = $this->getConsoleApp();
        $cmd = $this->consoleApp->find('prepare_import');
        $tester = new CommandTester($cmd);

        $tester->execute([
            'command' => $cmd->getName(),
            'provider' => 'course360training',
            '--type' => 'lo',
            '--limit'  => 1,
        ]);
        $this->assertContains('The prepare import data has been imported successfully', $tester->getDisplay());
    }
}