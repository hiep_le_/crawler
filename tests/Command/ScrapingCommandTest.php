<?php

namespace App\Test\Command;

use App\Command\ScrapingCommand;
use App\Scraping\Course\Course360Training\Course360TrainingScraping;
use App\Scraping\ScrapingController;
use App\Scraping\ScrapingData;
use App\Test\ScrapingTestCase;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Tester\CommandTester;

class ScrapingCommandTest extends ScrapingTestCase
{
    private $app;
    private $consoleApp;

    public function setUp()
    {
        parent::setUp();
        $this->app = $this->getApp();
    }

    public function testExecute()
    {
        $this->consoleApp = $this->getConsoleApp();
        $this->app = $this->getApp();
        $mock = $this->getMockBuilder('App\Command\ScrapingCommand')
          ->setConstructorArgs([$this->app])
          ->setMethods(['getList'])
          ->getMock();

        $this->app->extend("scraping.course360training", function() {
           return $this->getMockScraping();
        });

        $mock->setApplication($this->consoleApp);
        $mock->expects($this->any())->method('getList')->willReturn(null);

        $tester = new CommandTester($mock);
        $tester->execute([
            'command' => $mock->getName(),
            'provider' => 'course360training',
            '--type' => 'portal',
            '--override' => 1,
            '--limit' => 1,
        ]);

        $this->assertContains('Scraping has been completed', $tester->getDisplay());
    }

    public function getMockScraping() {
        $scraping = $this
            ->getMockBuilder(Course360TrainingScraping::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $scraping;
    }

    // @TODO change it local file
//    public function testScrapingData()
//    {
//        $data = [
//            [
//                'source_id' => rand(0, 100),
//                'url' => 'http://webcache.googleusercontent.com/search?q=cache:www.allconferences.com/c/pediatric-emergency-medicine-emergent-and-urgent-challenges-sarasota-2016-december-12',
//                'title' => '12th International Project Management Conference'
//            ]
//        ];
//
//        $progress = $this->getMockBuilder(ProgressBar::class)
//            ->disableOriginalConstructor()
//            ->getMock();
//
//        $cmd = new ScrapingCommand($this->app);
//        $type = 'portal';
//        /** @var ScrapingController $scrapingCtrl */
//        $scrapingCtrl = $this->app['scraping.ctrl'];
//        /** @var ScrapingData $scrapingData */
//        $scrapingData = $this->app['scraping.data'];
//
//        $provider = $scrapingCtrl->getProvider('allconferences');
//        $cmd->scrapingData($data, $type, true, $provider, $progress);
//        $scrapingData = $scrapingData->getScrapingDataBySourceId($data[0]['source_id'], $provider->getName(), $type);
//        $this->assertNotNull($scrapingData);
//    }
}