<?php

return call_user_func(function () {
    $prefix = 'SCRAPING_DB';
    putenv('SCRAPING_DB_NAME=scraping_prod');
    putenv('SCRAPING_DB_USERNAME=gc_prod');
    putenv('SCRAPING_DB_PASSWORD=gc_prod#2016');
    putenv('SERVICE_URL_PATTERN=http://api.mygo1.com/v3/SERVICE-service');

    $conf = require __DIR__ . '/config.default.php';
    $conf['note_firebase_url'] = 'https://notes-c4d57.firebaseio.com/note-dev-service';
    $conf['import.options'] = [
        'providers' => [
            'account'   => [
                'instance' => 'http://accounts.gocatalyze.com/'
            ],
            'wikihow'           => [
                'instance' => 'learn.gocatalyze.quyen.vn',
                'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6IjIuMCIsImV4cCI6MTUwMzA0NTEzOSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJtYWlsIjoidXNlci4xQGxlYXJuLmdvY2F0YWx5emUucXV5ZW4udm4iLCJuYW1lIjoiICIsInJvbGVzIjpbImFkbWluaXN0cmF0b3IiXX19fQ.irTTnBs6WFGDM8yNbnwmY6veuAmNXIukGDYLBqaafkU',
                'mail'     => 'user.1@learn.gocatalyze.quyen.vn',
            ],
            'course360training' => [
                'instance' => '360training.mygo1.com',
                'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg1MTc1Mjk0LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMTM1OCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzAwOCwibWFpbCI6IjM2MHRyYWluaW5nQHRvaWxhLm5ldCIsIm5hbWUiOiJmb28gYmFyIiwicm9sZXMiOlsiQWRtaW4gb24gI0FjY291bnRzIl0sImFjY291bnRzIjpbeyJpZCI6MzEzNTksImluc3RhbmNlIjoiMzYwdHJhaW5pbmcubXlnbzEuY29tIiwicHJvZmlsZV9pZCI6MSwicm9sZXMiOlsiU3R1ZGVudCIsImFkbWluaXN0cmF0b3IiXX1dfX19.SDCoVLqjYeEGg1_sP7nG9ZahF8ZlsbWWEDzwaseptuw',
                'mail'     => '360training@toila.net',
            ],
            'udemy' => [
                'instance' => 'udemy.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg2MzQ0OTg5LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMzc0MCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzgzOCwibWFpbCI6InVkZW15QGdvMS5jb20iLCJuYW1lIjoiVWRlbXkgR08xIiwiYWNjb3VudHMiOlt7ImlkIjozMzc0MSwiaW5zdGFuY2UiOiJ1ZGVteS5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.pYbMggILBRa0GyeGJZ1_W1aS_LgSqoiUk25W5IEc71M',
                'mail' => 'udemy@go1.com'
            ],
            'opensesame' => [
                'instance' => 'opensesame.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNy4yLjEuMCIsImV4cCI6MTQ4OTUyNzk2Niwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJpZCI6NjU1MzcsImluc3RhbmNlIjoiYWNjb3VudHMuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoyMTU5MCwibWFpbCI6InVzZXIuMUBvcGVuc2VzYW1lLm15Z28xLmNvbSIsIm5hbWUiOiJ1c2VyLjEgb3BlbnNlc2FtZS5teWdvMS5jb20iLCJhY2NvdW50cyI6W3siaWQiOjU4NjIwLCJpbnN0YW5jZSI6Im9wZW5zZXNhbWUubXlnbzEuY29tIiwibmFtZSI6IiAiLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9yIl19XX19fQ.pcRTOM7vm0w-NEGxeC1WCPc6g757hKyJ-Y5qMWEL-ws',
                'mail' => 'user.1@opensesame.mygo1.com'
            ],
            'allconferences' => [
                'instance' => 'allconferences.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNy4xLjIuMCIsImV4cCI6MTQ4ODcxNzMyMSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJpZCI6MzUyMDcsImluc3RhbmNlIjoiYWNjb3VudHMtZGV2LmdvY2F0YWx5emUuY29tIiwicHJvZmlsZV9pZCI6MTQyOTcsIm1haWwiOiJhbGxjb25mZXJlbmNlc0BnbzEuY29tIiwibmFtZSI6ImZvbyBiYXIiLCJyb2xlcyI6WyJBZG1pbiBvbiAjQWNjb3VudHMiXSwiYWNjb3VudHMiOlt7ImlkIjozNTIwOCwiaW5zdGFuY2UiOiJhbGxjb25mZXJlbmNlcy5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.SDx9JTW5fwdazHrFoYJHs-OKkJ35bkO17OiMuOpK9kY',
                'mail' => 'allconferences@go1.com'
            ]
        ],
    ];
    return $conf;
});
