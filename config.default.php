<?php

use go1\util\DB;
use go1\util\Service;

return call_user_func(function () {
    return [
        'env' => $env = getenv('ENV') ?: 'dev',
        'debug'                   => 'production' !== $env,
        'jwt.private_key'         => 'gc_dev#2016', # Production #user doesn't need this configuration, #api will send it. Just need for dev environment.
        'jwt.expire_time'         => getenv('JWT_EXPIRATION') ?: '+ 1 month',
        'logOptions'              => ['name' => 'scraping'],
        'clientOptions' => [],
        'cacheOptions'            =>
            (getenv('CACHE_BACKEND') && 'memcached' === getenv('CACHE_BACKEND'))
                ? ['backend' => 'memcached', 'host' => getenv('CACHE_HOST'), 'port' => getenv('CACHE_PORT')]
                : ['backend' => 'filesystem', 'directory' => __DIR__ . '/cache'],
        'dbOptions' => [
            'default' => DB::connectionOptions('scraping')
        ],
        'youtube.options' => [
            'key' => getenv('YOUTUBE_API_KEY'),
        ],
        'monolog.options' => [
            'monolog.logfile' => __DIR__ . '/var/logs/something.log',
        ],
        'note_firebase_url' => getenv('FIREBASE_URL') ?: 'https://notes-c4d57.firebaseio.com/note-dev-service',
        'import.options' => [
            'providers' => [
                'account'   => [
                    'instance' => 'http://accounts-dev.gocatalyze.com/'
                ],
                'wikihow'           => [
                    'instance' => 'learn.gocatalyze.quyen.vn',
                    'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6IjIuMCIsImV4cCI6MTUwMzA0NTEzOSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJtYWlsIjoidXNlci4xQGxlYXJuLmdvY2F0YWx5emUucXV5ZW4udm4iLCJuYW1lIjoiICIsInJvbGVzIjpbImFkbWluaXN0cmF0b3IiXX19fQ.irTTnBs6WFGDM8yNbnwmY6veuAmNXIukGDYLBqaafkU',
                    'mail'     => 'user.1@learn.gocatalyze.quyen.vn',
                ],
                'course360training' => [
                    'instance' => '360training.mygo1.com',
                    'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg1MTc1Mjk0LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMTM1OCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzAwOCwibWFpbCI6IjM2MHRyYWluaW5nQHRvaWxhLm5ldCIsIm5hbWUiOiJmb28gYmFyIiwicm9sZXMiOlsiQWRtaW4gb24gI0FjY291bnRzIl0sImFjY291bnRzIjpbeyJpZCI6MzEzNTksImluc3RhbmNlIjoiMzYwdHJhaW5pbmcubXlnbzEuY29tIiwicHJvZmlsZV9pZCI6MSwicm9sZXMiOlsiU3R1ZGVudCIsImFkbWluaXN0cmF0b3IiXX1dfX19.SDCoVLqjYeEGg1_sP7nG9ZahF8ZlsbWWEDzwaseptuw',
                    'mail'     => '360training@toila.net',
                ],
                'udemy' => [
                    'instance' => 'udemy.mygo1.com',
                    'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg2MzQ0OTg5LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMzc0MCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzgzOCwibWFpbCI6InVkZW15QGdvMS5jb20iLCJuYW1lIjoiVWRlbXkgR08xIiwiYWNjb3VudHMiOlt7ImlkIjozMzc0MSwiaW5zdGFuY2UiOiJ1ZGVteS5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.pYbMggILBRa0GyeGJZ1_W1aS_LgSqoiUk25W5IEc71M',
                    'mail' => 'udemy@go1.com'
                ],
                'opensesame' => [
                    'instance' => 'opensesame.mygo1.com',
                    'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg2MzQ2NjEyLCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMzc0NiwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzgzOSwibWFpbCI6Im9wZW5zZXNhbWVAdG9pbGEubmV0IiwibmFtZSI6Ik9wZW5zZXNhbWUgR28xIiwiYWNjb3VudHMiOlt7ImlkIjozMzc0NywiaW5zdGFuY2UiOiJvcGVuc2VzYW1lLm15Z28xLmNvbSIsInByb2ZpbGVfaWQiOjEsInJvbGVzIjpbIlN0dWRlbnQiLCJhZG1pbmlzdHJhdG9yIl19XX19fQ.cJHo-V70a2hvK-W7FTzK2-JH4X6JD5UKaX2KvDMRI70',
                    'mail' => 'opensesame@toila.net'
                ],
                'allconferences' => [
                    'instance' => 'allconferences.mygo1.com',
                    'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNy4xLjIuMCIsImV4cCI6MTQ4ODcxNzMyMSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJpZCI6MzUyMDcsImluc3RhbmNlIjoiYWNjb3VudHMtZGV2LmdvY2F0YWx5emUuY29tIiwicHJvZmlsZV9pZCI6MTQyOTcsIm1haWwiOiJhbGxjb25mZXJlbmNlc0BnbzEuY29tIiwibmFtZSI6ImZvbyBiYXIiLCJyb2xlcyI6WyJBZG1pbiBvbiAjQWNjb3VudHMiXSwiYWNjb3VudHMiOlt7ImlkIjozNTIwOCwiaW5zdGFuY2UiOiJhbGxjb25mZXJlbmNlcy5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.SDx9JTW5fwdazHrFoYJHs-OKkJ35bkO17OiMuOpK9kY',
                    'mail' => 'allconferences@go1.com'
                ]
            ],
        ],
        'providers' => [
            'account'   => [
                'instance' => 'http://accounts-dev.gocatalyze.com/'
            ],
            'wikihow'           => [
                'instance' => 'learn.gocatalyze.quyen.vn',
                'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6IjIuMCIsImV4cCI6MTUwMzA0NTEzOSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJtYWlsIjoidXNlci4xQGxlYXJuLmdvY2F0YWx5emUucXV5ZW4udm4iLCJuYW1lIjoiICIsInJvbGVzIjpbImFkbWluaXN0cmF0b3IiXX19fQ.irTTnBs6WFGDM8yNbnwmY6veuAmNXIukGDYLBqaafkU',
                'mail'     => 'user.1@learn.gocatalyze.quyen.vn',
            ],
            'course360training' => [
                'instance' => '360training.mygo1.com',
                'jwt'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg1MTc1Mjk0LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMTM1OCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzAwOCwibWFpbCI6IjM2MHRyYWluaW5nQHRvaWxhLm5ldCIsIm5hbWUiOiJmb28gYmFyIiwicm9sZXMiOlsiQWRtaW4gb24gI0FjY291bnRzIl0sImFjY291bnRzIjpbeyJpZCI6MzEzNTksImluc3RhbmNlIjoiMzYwdHJhaW5pbmcubXlnbzEuY29tIiwicHJvZmlsZV9pZCI6MSwicm9sZXMiOlsiU3R1ZGVudCIsImFkbWluaXN0cmF0b3IiXX1dfX19.SDCoVLqjYeEGg1_sP7nG9ZahF8ZlsbWWEDzwaseptuw',
                'mail'     => '360training@toila.net',
            ],
            'udemy' => [
                'instance' => 'udemy.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg2MzQ0OTg5LCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMzc0MCwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzgzOCwibWFpbCI6InVkZW15QGdvMS5jb20iLCJuYW1lIjoiVWRlbXkgR08xIiwiYWNjb3VudHMiOlt7ImlkIjozMzc0MSwiaW5zdGFuY2UiOiJ1ZGVteS5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.pYbMggILBRa0GyeGJZ1_W1aS_LgSqoiUk25W5IEc71M',
                'mail' => 'udemy@go1.com'
            ],
            'opensesame' => [
                'instance' => 'opensesame.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNi4xMi4zIiwiZXhwIjoxNDg2MzQ2NjEyLCJvYmplY3QiOnsidHlwZSI6InVzZXIiLCJjb250ZW50Ijp7ImlkIjozMzc0NiwiaW5zdGFuY2UiOiJhY2NvdW50cy1kZXYuZ29jYXRhbHl6ZS5jb20iLCJwcm9maWxlX2lkIjoxMzgzOSwibWFpbCI6Im9wZW5zZXNhbWVAdG9pbGEubmV0IiwibmFtZSI6Ik9wZW5zZXNhbWUgR28xIiwiYWNjb3VudHMiOlt7ImlkIjozMzc0NywiaW5zdGFuY2UiOiJvcGVuc2VzYW1lLm15Z28xLmNvbSIsInByb2ZpbGVfaWQiOjEsInJvbGVzIjpbIlN0dWRlbnQiLCJhZG1pbmlzdHJhdG9yIl19XX19fQ.cJHo-V70a2hvK-W7FTzK2-JH4X6JD5UKaX2KvDMRI70',
                'mail' => 'opensesame@toila.net'
            ],
            'allconferences' => [
                'instance' => 'allconferences.mygo1.com',
                'jwt' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJnbzEudXNlciIsInZlciI6InYxNy4xLjIuMCIsImV4cCI6MTQ4ODcxNzMyMSwib2JqZWN0Ijp7InR5cGUiOiJ1c2VyIiwiY29udGVudCI6eyJpZCI6MzUyMDcsImluc3RhbmNlIjoiYWNjb3VudHMtZGV2LmdvY2F0YWx5emUuY29tIiwicHJvZmlsZV9pZCI6MTQyOTcsIm1haWwiOiJhbGxjb25mZXJlbmNlc0BnbzEuY29tIiwibmFtZSI6ImZvbyBiYXIiLCJyb2xlcyI6WyJBZG1pbiBvbiAjQWNjb3VudHMiXSwiYWNjb3VudHMiOlt7ImlkIjozNTIwOCwiaW5zdGFuY2UiOiJhbGxjb25mZXJlbmNlcy5teWdvMS5jb20iLCJwcm9maWxlX2lkIjoxLCJyb2xlcyI6WyJTdHVkZW50IiwiYWRtaW5pc3RyYXRvciJdfV19fX0.SDx9JTW5fwdazHrFoYJHs-OKkJ35bkO17OiMuOpK9kY',
                'mail' => 'allconferences@go1.com'
            ]
        ],
        'mail.options'    => [
            'jwt'      => getenv('jwt'),
            'endpoint' => getenv('mail_service') ?: 'https://api.mygo1.com/v3/mail-service/',
        ],
        'maintainers'     => ['phuong.bui@go1.com.au'],
    ] + Service::urls(['user', 'lo', 'portal', 'onboard', 'vote', 'note', 'cloudinary', 'scraping'], $env, getenv('SERVICE_URL_PATTERN'));
});

// Create a file config and overrider config.default.php
// eg: config.dev.php
//return call_user_func(function () {
//    $prefix = 'SCRAPING_DB';
//    putenv('SCRAPING_DB_HOST=db');
//    putenv('SCRAPING_DB_MASTER=db');
//    putenv('SCRAPING_DB__SLAVE=db');
//    putenv('SCRAPING_DB_NAME=phuongb_lti');
//    putenv('SCRAPING_DB_USERNAME=root');
//    putenv('SCRAPING_DB_PASSWORD=dvs-dv8');
//    putenv('SERVICE_URL_PATTERN=http://api-dev.mygo1.com/v3/SERVICE-service');
//    return require __DIR__ . '/config.default.php';
//});