FROM busybox
COPY . /app
RUN rm -rf /app/.git/
WORKDIR /app
VOLUME ["/app"]