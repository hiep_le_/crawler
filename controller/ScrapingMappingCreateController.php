<?php

namespace go1\Scraping\controller;

use Assert\LazyAssertionException;
use go1\Scraping\domain\scraping\ScrapingMappingRepository;
use go1\util\AccessChecker;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Exception;

class ScrapingMappingCreateController
{
    private $repository;
    private $accessChecker;

    public function __construct(
        ScrapingMappingRepository $repository,
        AccessChecker $accessChecker
    ) {
        $this->repository = $repository;
        $this->accessChecker = $accessChecker;
    }

    public function create(Request $req)
    {
        $provider = $req->request->get('provider', null);
        $sourceId = $req->request->get('source_id', null);
        $remoteId = $req->request->get('remote_id', null);
        $type = $req->request->get('type', 'unknown');
        $this->validate($req);
        try {
            \Assert\lazy()
                ->that($provider, 'provider')->notNull()->string()
                ->that($sourceId, 'source_id')->notNull()->string()
                ->that($remoteId, 'remote_id')->notNull()->string()
                ->that($type, 'type')->notNull()->string()
                ->verifyNow();
        } catch (LazyAssertionException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400);
        }

        try {
            $data = [
                'provider' => $provider,
                'source_id' => $sourceId,
                'remote_id' => $remoteId,
                'type' => $type
            ];
            $scrapingMappingId = $this->repository->insert($data);
        } catch (Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
        return new JsonResponse(['scraping_mapping_id' => $scrapingMappingId], 201);
    }

    public function validate($req)
    {
        $user = $this->accessChecker->validUser($req);
        if ($this->accessChecker->isAccountsAdmin($req)) {
            return true;
        }
        return new JsonResponse(['message' => 'Only portal admin or accounts admin can use the features'], 400);
    }
}