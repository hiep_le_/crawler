<?php

namespace go1\Scraping\controller;

use App\SchemaDefinition;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\TableExistsException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ScrapingMappingSchemaController
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function install()
    {
        $man = $this->connection->getSchemaManager();
        $oldSchema = $man->createSchema();
        $schema = clone $oldSchema;
        if (!$man->tablesExist(['scraping_mapping'])) {
            $definition = new SchemaDefinition($schema);
            $definition->tableScrapingMapping();
        }
        $sqls = $oldSchema->getMigrateToSql($schema, $this->connection->getDatabasePlatform());
        foreach ($sqls as $sql) {
            try {
                $this->connection->executeQuery($sql);
            }
            catch (TableExistsException $e) {
            }
        }

        return new JsonResponse(null, 204);
    }
}
