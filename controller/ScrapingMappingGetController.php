<?php

namespace go1\Scraping\controller;

use Assert\LazyAssertionException;
use go1\Scraping\domain\scraping\ScrapingMappingRepository;
use go1\util\AccessChecker;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Exception;

class ScrapingMappingGetController
{
    private $repository;
    private $accessChecker;

    public function __construct(ScrapingMappingRepository $repository) {
        $this->repository = $repository;
    }

    public function get(Request $req)
    {
        $provider = $req->query->get('provider', null);
        $sourceId = $req->query->get('source_id', null);
        $type = $req->query->get('type', null);


        try {
            \Assert\lazy()
                ->that($provider, 'provider')->notNull()->string()
                ->that($sourceId, 'source_id')->notNull()->string()
                ->that($type, 'type')->notNull()->string()
                ->verifyNow();
        } catch (LazyAssertionException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400);
        }

        try {
            $mapping = $this->repository->get($provider, $sourceId, $type);
        } catch (Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
        return new JsonResponse($mapping, 201);
    }

}